# Guide Administrateur·rice [v4.3]
https://mobiliscope.quarto.pub/guide_admin/guide_admin.html


# Environnement
apache 2 et php 7.3.29
pas de base de données, les fichiers lus sont stockés dans un bucket minio


# Installation
`npm install`
`composer install`

# Installer minio

## installation mac
`https://min.io/download#/macos`
`brew upgrade`
`brew install minio/stable/minio`

`mkdir ~/Data/mobiliscope`

`sudo MINIO_ROOT_USER=minioadmin MINIO_ROOT_PASSWORD=minioadmin minio server ~/Data/mobiliscope  --console-address '127.0.0.1:60001' --address '127.0.0.1:60000'`


Se loguer sur la console minio
http://127.0.01:60001/login
avec minioadmin / minioadmin

Créer un bucket nommé mobiliscope et lui donner l’accès public

## Installation Windows
https://min.io/download#/windows
télécharger le MINIO SERVER
créer un dossier C:\Users\Documents\database\mobiliscope
déplacer le minio.exe dans le dossier C:\Users\Documents\database\
ouvrir un invite de commande :
cd C:\Users\Documents\database
minio.exe server C:\Users\Documents\database\mobiliscope --console-address '192.168.0.1:60001' --address '192.168.0.1:60000'
NOTE : il est possible que la bécane en local ne puisse pas ouvrir le service minio sur ces ports → changer les ports jusqu’à trouver un port libre. Attention il faut alors changer les ports dans la config /minio-settings.php
Parfois il est préférable d’utiliser l’adresse interne 127.0.0.1 au lieu de 192.168.0.1 (c’est équivalent)
Pour ajouter des répertoires et fichiers  au minio, il faut absolument passer par l’interface et ne pas ajouter directement les folders dans le répertoire C:\Users\Documents\database\mobiliscope (cela casse tout).


# Développement

## Lancer minio
`sudo MINIO_ROOT_USER=minioadmin MINIO_ROOT_PASSWORD=minioadmin minio server ~/Data/mobiliscope  --console-address '127.0.0.1:60001' --address '127.0.0.1:60000'`


## Lancer la compilation du javascript et css
`nvm use`
`npm run build:dev`

# Build pour la production
`npm run build:prod`

