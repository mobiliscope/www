   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'about' ? 'current-page' : ''; ?>">About</a>
      <div class="left-menu-niv2">
        <a href="/en/info/about/summary"><div class="item">In summary</div></a>
        <a href="/en/info/about/team"><div class="item">Team</div></a>
        <a href="/en/info/about/partners"><div class="item">Partners</div></a>
        <a href="/en/info/about/publications"><div class="item">Papers & co.</div></a>
     </div>
   </div>

   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'methods' ? 'current-page' : ''; ?>">Methods</a>
     <div class="left-menu-niv2">
      <a href="/en/info/methods/data"><div class="item">Transforming trip dataset</div></a>
      <a href="/en/info/methods/indicators"><div class="item">Describing ambient population</div></a>
      <a href="/en/info/methods/geovizualisation"><div class="item">Geovizualisation</div></a>
      <a href="/en/info/methods/evolution"><div class="item">Developments and updates</div></a>
    </div>
   </div>

    <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'open' ? 'current-page' : ''; ?>">Open science</a>
     <div class="left-menu-niv2">
        <a  href="/en/info/open/license"><div class="item">Open-source code and documentation</div></a>
        <a href="/en/info/open/opendata"><div class="item">Open data</div></a>
        <a href="/en/info/open/reuses"><div class="item">Reproductible papers</div></a>
        <a href="/en/info/open/cite"><div class="item">How to cite Mobiliscope?</div></a>
    </div>
   </div>

   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'multitask-tool' ? 'current-page' : ''; ?>">Fields of application</a>
     <div class="left-menu-niv2">
       <a href="/en/info/multitask-tool/scientific-tool"><div class="item">Science & Research</div></a>
        <a href="/en/info/multitask-tool/planning-tool"><div class="item">Planning</div></a>
       <a href="/en/info/multitask-tool/pedagogical-tool"><div class="item">Teaching</div></a>
    </div>
   </div>

     <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'toolbox' ? 'current-page' : ''; ?>">Toolbox</a>
     <div class="left-menu-niv2">
      <a href="/en/info/toolbox/zoning"><div class="item">Filter by institutional zonings <small>(France)</small></div></a>
    </div>
    </div>

