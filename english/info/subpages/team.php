<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Team</h2>
	</section>


        <section>
		<h3>Members</h3>

        <p>
    		<b><a href = "https://lisst.univ-tlse2.fr/home/julie-vallee#/" target="_blank" >Julie Vallée</a></b> is researcher in geography at the French National Center for Scientific Research (CNRS). She is a member of the LISST laboratory (Toulouse) and associated with the Géographie-cités laboratory (Paris). Julie is the scientific coordinator of the Mobiliscope.</br>
    		She is conducting research focusing on spatial mechanisms of social inequalities in urban areas. From people's daily mobility, she aims to explore neighbourhood effects (notably on health inequalities) and to analyse the 'daycourse' of place.</br>
    		
    	</p>

    	<p>
    		<b><a  target="_blank" href = "http://crh.ehess.fr/index.php?9040">Aurélie Douet</a></b> is an engineer in cartography and geomatics.</br> She led the development of Mobiliscope for more than 4 years. In particular, she extended the platform to French cities other than the Paris region and integrated Latin American cities. More recently, she reorganised the data processing protocols and the software architecture. Since December 2023, Aurélie has been a research engineer at the Centre de Recherches Historiques (EHESS/CNRS) and continues to follow the progress of the project with interest.
    	</p>


        <p>
    		<b><a href = "https://www.ined.fr/en/research/researchers/Le+Roux+Guillaume" target="_blank">Guillaume Le Roux</a></b> is researcher at the French Institute for Demographic Studies (INED).</br>
    		His research focuses on the relationships between social mobility, spatial mobility and recomposition of territories under urban influence. Guillaume has critically contributed to the exploration of origin-destination survey database (EGT 2010, Paris region) in order to analyse social segregation around the clock.
		</p>

        <p>
    		<b><a target="_blank" href = "https://geographie-cites.cnrs.fr/en/members/hadrien-commenges/">Hadrien Commenges</a></b> is an associate professor at the University Paris 1 Panthéon-Sorbonne. </br>
    		His research focuses on quantitative methods applied to urban geography, mainly in the field of urban mobility and transportation.
    	</p>

    	</section>

		<section>
		<h3>Previous members</h3>

		<p>
    		<b>Constance Lecomte</b> developed in 2017 the first two versions of the Mobiliscope dedicated to Île-de-France.</br>
    		She then remained heavily involved in the Mobiliscope project while working at Observatoire des Territoires in the Agence nationale de la cohésion des territoires (ANCT).
    	</p>

    	 <p>
			<b>Elisa Villard</b> joined the team for several months in 2019. She was in charge of including Canadian cities in the Mobiliscope (following her five-month internship in Montreal).
		</br>

		</p>
		</section>


		<section>
		<h3>Students</h3>

		<p>
		Several students have completed their master's theses with the Mobiliscope team.

		<ul>
			<li><b>Robin Demé</b> (Master 2 Démographie - Expertise en Sciences de la Population - Université Paris 1 Panthéon-Sorbonne). 2023.</li>

			<li><b>Baptiste Larrouy</b> (Centrale Nantes School / Bachelor in Digital City - Geomatics and Urbanism). 2021.</li>
			
			<li><b>Élisa Villard</b> (Centrale Nantes School / Bachelor in Digital City - Geomatics and Urbanism). 2019.</li>
			
			<li><b>Simon Oertlin</b> (Université Paris 1 Panthéon-Sorbonne / Master in Geography). 2019.</li>
			
			<li><b>Maximilien Riot</b> (Université de Nice / Master in Geomatics). 2019. </li>
		</ul>
		</p>
		</section>

		<section>
        <h3>Others contributors</h3>

		<p>
			<ul>

				<li>Renaud Bessières (<a target="_blank" href="https://qodop.com/" title="qodop">qodop</a>) : web development</li> 
				<li>Mathieu Leclaire (Géographie-cités/ISC-PIF) : IT reorganisation (hosting on HumaNum, data storage on MinIO, gitlab repositories, etc.)</li>
				<li>Barbara Christian and Julie Pelata (Cerema) : help with French origin-destination surveys (EMC²)</li>
				<li>Cédric Poirier : improving the interface</li>
				<li>Saber Marrouchi and Yonathan Parmentier (Géographie-cités) : IT support</li>
				<li>Florent Demoraes (Université de Rennes/ANR Modural) : critical feedback on the use of Latin American surveys and help with the Bogotá base map</li>
				<li>Marc Sainte-Croix and Blandine Maison (Agence d’urbanisme, région nîmoise et alésienne) : participation in the integration of Nîmes region</li>
				<li>Sébastien Haule (Géographie-cités) : communications support</li>
				<li>Robin Cura and Mattia Bunel (Géographie-cités) : geovisualisation advice</li>

			</ul>
		</p>
		</section>


		<section>
         <h3>Join us!</h3>
        <p>
    		Contact us (mobiliscope@cnrs.fr) if you want to join the team!
        </p>
        </section>

		<p>
		  <button class="style-button mb50"><a href="/en/info/about/partners">Discover our partners</a></button>
		</p>


</div>
