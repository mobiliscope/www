<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">

	<section>
		<h2>Filter by institutional zonings (France)</h2>
        <center>
            <h4> Get to know the French Mobiliscope areas that are part of public action programmes:</br>
            <i>Actions Coeur de Ville (ACV)</i>, <i>Petites Villes de Demain (PVD)</i> et <i>Quartiers Prioritaires en Politique de la Ville (QPV)</i>.</h4>
        </center>
	</section>

    </br>

    <section>
        <h3>Action Coeur de Ville (ACV)</b></h3>
        
        <p> 
            Mobiliscope covers <b>88</b> of the 235 <b>municipalities</b> ('communes' in French) in the <i><a href = "https://agence-cohesion-territoires.gouv.fr/action-coeur-de-ville-42" target="_blank">Action Coeur de Ville</a></i> programme - <small>list as of November 2021</small>.
        </p>

        <p>
        These 88 ACV municipalities are listed in the table below.
        <br>
        <small>
            <ul>
            <li>The table can be sorted, filtered and downloaded in csv format.</li>
            <li>By clicking on the name of a municipality, you will be redirected to the associated geovisualisation.</li>
            <li>The <i>Correspondence type</i> category indicates whether the municipality is divided into several Mobiliscope districts (‘ Multi-districts municipality’), corresponds to a single district ('Single-district municipality’) or is included in a larger district made up of several municipalities (‘Sub-district municipality’). A few rare ‘Particular case’ are linked to merged municipalities or discontinuous coverages.</li>
            </ul>
        </small>
        </p>
        
            <br>

            <?php include('table/acv_table.html'); ?> 
        </p>
    
    <p class="ss-style-dots" ></p>

    </section>


	<section>
		<h3>Petites Villes de Demain (PVD)</h3>

        <p> 
             Mobiliscope covers <b>502</b> of the 1643 municipalities ('communes' in French) in the <i><a href = "https://agence-cohesion-territoires.gouv.fr/petites-villes-de-demain-45" target="_blank">Petites Villes de Demain</a></i> programme - <small>list as of November 2021</small>.
        </p>

        <p>
            These 502 PVD municipalities are listed in the table below.
        <br>
        <small>
            <ul>
            <li>The table can be sorted, filtered and downloaded in csv format.</li>
            <li>By clicking on the name of a municipality, you will be redirected to the associated geovisualisation.</li>
            <li>The <i>Correspondence type</i> category indicates whether the municipality is divided into several Mobiliscope districts (‘ Multi-districts municipality’), corresponds to a single district ('Single-district municipality’) or is included in a larger district made up of several municipalities (‘Sub-district municipality’). A few rare ‘Particular case’ are linked to merged municipalities or discontinuous coverages.</li>
            </ul>
        </small>
            
            <br>

            <?php include('table/pvd_table.html'); ?>
        </p>

    <p class="ss-style-dots" ></p>

    </section>

	<section>
		<h3>Poverty Areas ('Quartiers Prioritaires en Politique de la Ville' - QPV)</h3>

        <p>
             <b>686</b> of the 2572 Mobiliscope French <i>districts</i> include <b>Poverty Areas </b> : 871 of the 1514 Poverty Areas in France are covered by the Mobiliscope.
        </p>

        <p>
            These 686 Mobiliscope districts with QPVs are listed in the table below.
            <br>
            <small>
            <ul>
            <li>The table can be sorted, filtered and downloaded in csv format.</li>
            <li>By clicking on the name of a district, you will be redirected to the associated geovisualisation.</li>
            <li>The <i>Cover rate</i> category corresponds to the proportion of the surface area of the Mobiliscope district that is covered by one or more QPVs.</li>
            <li>The QPVs taken into account are issued from the <a href = "https://sig.ville.gouv.fr/page/198/les-quartiers-prioritaires-de-la-politique-de-la-ville-2015" target="_blank"> <b>2015</b> QPV geography</a> (and not from the new QPV geography <a href = "https://sig.ville.gouv.fr/page/224/les-quartiers-prioritaires-de-la-politique-de-la-ville-2024" target="_blank">updated in <b>2024</b></a>).</li>
            </ul>
            </small>

            <br>

            <?php include('table/qpv_table.html'); ?> 
        </p>

        <p> 
            Other functions are available in Mobiliscope to explore the daily rhythms of the 'Poverty Areas'. You can :
            <ul>
                <li>display the boundaries of the QPVs (as of 2015) on the map  <img src="/dist/assets/layers2.png" width="25px" height= "25px"/></li>
                <li>measure (in numbers and %) the ambient population according to their place of residence in or outside a 'Poverty Area'</li>
            </ul>
        </p>

    <p class="ss-style-dots" ></p>

    </section>

    <section>
        <h3>Methodology and open data</h3>

        <p>
        <b>49 French cities/regions</b> are included in the current version of Mobiliscope. These 49 French cities/regions are divided into 2,572 districts covering a total of more than 10,000 municipalities ('communes') where 60% of the country's population lives.
        <br>
        
        <i>Note: 20 French districts (and 79 municipalities) are part of more than one survey/geovizualisations.</i>
        </p>

        <h4>Step 1.</h4> 

        <p>
        There are four types of correspondence between municipalities -> districts:
            <ul>
                <li>A municipality is divided into several Mobiliscope districts (‘ Multi-districts municipality’). This is often the case for (large) cities.</li>
                <li>A municipality corresponds to a single district ('Single-district municipality’).</li>
                <li>A municipality is included in a larger district made up of several municipalities (‘Sub-district municipality’). This  situation is mainly found in peripheral and rural areas.</li>
                <li>A few ‘particular cases’ (2%) are linked to merged municipalities or discontinuous coverages.</li>
            </ul>
        </p>

        <p>
            The <b>csv files</b> with correspondence between <i>municipalities -> districts</i> (and also <i>districts->municipalities</i>) can be downloaded <a href="/data-settings/commune_secteur.zip" title="commune_secteur" download="commune_secteur.zip"><b>here</b></a>, along with documentation in French (description of the method and dictionary of variables). 
        </p>

        <h4>Step 2.</h4> 

        <p>
            A cross-reference is made with the municipalities in the <i>Action Coeur de Ville (ACV)</i> and <i>Petites Villes de Demain (PVD)</i> programmes (as well as with the Urban Areas (ZAU) zoning, the Urban Attraction Areas (AAV) zoning and the density grids). 
        </p>

        <p>
            The <b>csv files</b> listing the <b>minicipalities</b> (and also the Mobiliscope <b>districts</b>) in the various zoning areas can be downloaded <a href="/data-settings/commune_secteur_zonages.zip" title="commune_secteur_zonages" download="commune_secteur_zonages.zip"><b>ici</b></a>, along with documentation in French (description of the data sets used for the zoning areas, description of the method and dictionary of variables).  
        </p>

    </br>
    </br>
    </br>
    </br>

    </section>

</div>


<script type="text/javascript">
$(document).ready(function () {

	// DataTable PVD
	$('#pvdtable thead tr')
        .clone(true)
        .addClass('filterspvd')
        .appendTo('#pvdtable thead');

    $('#pvdtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'pvd_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,1])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filterspvd th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    console.log(title);
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filterspvd th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([2,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filterspvd th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#pvdtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });

    

    // DataTable ACV
    $('#acvtable thead tr')
        .clone(true)
        .addClass('filtersacv')
        .appendTo('#acvtable thead');

    $('#acvtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'acv_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,1])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersacv th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filtersacv th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([2,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersacv th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#acvtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });


	// DataTable QPV
    $('#qpvtable thead tr')
        .clone(true)
        .addClass('filtersqpv')
        .appendTo('#qpvtable thead');

    $('#qpvtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'qpv_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,2])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersqpv th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filtersqpv th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([1,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersqpv th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#qpvtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });
	
});

</script>
