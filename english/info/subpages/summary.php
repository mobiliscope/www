<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
    <h2>In summary</h2>
  </section>

  </br>
  <section>
  <p>
    Mobiliscope is a geovisualization platform that allows you to explore the ambient population within cities over a 24 hour period. With the Mobiliscope, we discover how cities, neighbourhood social composition and segregation change around the clock.<br>
  </p>

   <p>
    <b>58 city regions</b> are included in the actual version of the Mobiliscope: 49 <b>French</b> cities, 6 <b>Canadian</b> cities and 3 <b>Latin American</b> cities.</br>
  </p>

  <p>
    Behind the Mobiliscope, there is a <a href="/en/info/about/team">team</a> of geographers and geomaticians working in Géographie-cités laboratory (Paris, France) and as well as some supporting <a href="/en/info/about/partners">partners</a>.</br>
  </p>

  <p>
    To underline how cities and social segregation gain to be observed from a 'daycourse' perspective, we have also introduced and described the Mobiliscope in various <a href="/en/info/publications">papers and communications</a>.  
  </p>

  <p>
    To cite the <b>current version</b> of Mobiliscope&nbsp;:</b>
      <ul>Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C, Villard E (2024). Mobiliscope, an open platform to explore cities and social mix around the clock [v4.3]. www.mobiliscope.cnrs.fr. doi:10.5281/zenodo.11111161</ul>
    </p>

  </section>


  <section>
  <h3>How it works ?</h3>

    <p>
    The <a href="/en/info/methods/data">data</a> comes from recent 'origin-destination surveys' with 968,000 respondents and 2.9 millions of trips. 
    </p>

    <p>
    Present population can be explored over the 24h period of a typical "weekday" (Monday-Friday) according to some personal, household and place <a href="/en/info/methods/indicators">indicators</a>.</p>

    <p>
    One central map and two graphs are the key elements of the <a href="/en/info/methods/geovizualisation">geovizualisation</a> around the clock.
    </p>

    <p>
    The Mobiliscope is constantly <a href="/en/info/methods/evolution">evolving</a>. New cities and functionalities are regularly added.
    </p>

    <button class="style-button mb50"><a href="/en/geoviz/montreal/">Discover the tool for <b>Montreal</b> region</a></button>
    </section>


    <section>
    <h3>Open science</h3>

    <p>
      Working in line with the <b>FAIR principles</b>, we do our best to ensure that our source code as well as the data which are displayed in the Mobiliscope are easy to <b>F</b>ind, <b>A</b>ccessible, <b>I</b>nteroperable and <b>R</b>eusable. 
    </p>

    <p>
      Code developed to process the initial transportation data and to create the geovisualization interfaceto are <a href="/en/info/open/license">open source</a>. An <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">administrator's guide</a> (in French) also documents the codes used and how the web application works. Data displayed in the tool is <a href="/en/info/open/opendata">open-data</a>. They are reused in <a href="/en/info/open/reuses">reproducible articles</a>.
      </p>
      </br>
      </br>
    </section>


    <section>
    <h3>A multitask tool</h3>

    <p>
      Daily mobility data is increasing but remain difficult to handle to explore cities and their daily rhythms. Mobiliscope has been developed to help large audiences to discover how cities change around the clock. It is intended to be used as a <a href="/en/info/multitask-tool/planning-tool">planning tool</a> for policymakers, as a <a href="/en/info/multitask-tool/scientific-tool">scientific tool</a> for researchers, or as a <a href="/en/info/multitask-tool/pedagogical-tool">pedagogical tool</a> for teachers.
    </p>

      <p>
      For those interested in the daytime rhythms of the areas covered by public action programmes, a new tool provides direct links to the French territories concerned by these zonings. 
      </p>

      <button class="style-button mb50"><a href="/en/info/toolbox/zoning">Discover the <b>Filter by institutional zonings <small>(France)</small></b>.</a></button>

    </section>

    <section>
    <center>
        <figure class="inline" >
          <img src="/dist/assets/prix.jpg" alt="prix" width="350"/>
          <figcaption>Trophy of the <b>Open Science of Research Data prize</b>.</br><i>Awarded in 2022 by the French Ministry of Higher Education and Research</i></figcaption>
        </figure>
      </center>
   
    </section>

    </br>
    </br> 
    </br>
    </br> 


</div>


