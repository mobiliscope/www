<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Open-source code and documentation</h2>

	</br>
	<p>
		In the spirit of open science and the FAIR principles, our codes are <b>open-source</b>.
	</p>

	</section>

	<section>
	<h3>Data-preprocessing</h3>

		<p>
		Aggregated and weighted data (which cannot be used to re-identify the respondents) are generated using two R scripts:
		<ul>
			<li>the first one transforms trip dataset from surveys into hourly location dataset</li>
   			<li>the second produces all the files needed to geovisualise the ambient populations by hour and by district, for each indicator.</li>
   		</ul>
   		</p>

   		<p>
		The scripts for pre-processing Mobiliscope data are available in <a href = "https://gitlab.huma-num.fr/mobiliscope/data-process" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>data-process</b></a>.
		</p>

		<p>
		The data produced is stored in the <i>MinIO</i> storage service, in the form of geojson files for spatial data and csv files for attribute data without geometry. Much of this data is available in <a href="/en/info/open/opendata">open data</a>. 
		</p>
				
	</section>
	
	<section>
	<h3>Software</h3>

			<p>
				The Mobiliscope has been developed using HTML, CSS (framework <a href = "https://minicss.org/" target = "_blank"> mini.css</a>), javascript and PHP. The geovisualization has been coded in javascript and relies on <a  href = "https://d3js.org/" target="_blank" >D3.js</a> - developed by Mike Bostock - and <a href="http://leafletjs.com" title="A JS library for interactive maps" target="_blank">Leaflet</a> library. OpenStreetMap layers and maps drawn with D3.js are displayed with <a href="https://github.com/teralytics/Leaflet.D3SvgOverlay" target="_blank">Leaflet.D3SvgOverlay</a>.
			</p>

			<p>
				The source code for the Mobiliscope web pages is available in <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b></a> repository. It is from this source code that the Mobiliscope is deployed on the HumaNum server. 
			</p>
	</section>


	<section>
	<h3>Licence and citation</h3>
		<p>
				 Programs are under copyleft <b><a href="https://spdx.org/licenses/AGPL-3.0-or-later.html" target="_blank">AGPL licence</a></b>
				With AGPL license, users can use and modify programs as long as :
				<ul>
					<li>they redistribute the modified programs under AGPL license</li>

					<li>they cite Mobiliscope</br>
			 			➔ Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C,  Villard E (2024). Mobiliscope, an open platform to explore cities and social mix around the clock [v4.3]. www.mobiliscope.cnrs.fr doi:10.5281/zenodo.11111161.</li>
			 		</ul>	
			</p>
	</section>


	<section>
	<h3>Documentation</h3>

		<p>
			A new <b>Administrator Guide</b> has been written (in French). It describes the codes used, summarises how the services work and explains how to maintain and update the tool.</br>
			The guide is freely available at <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">https://mobiliscope.quarto.pub/guide_admin/guide_admin.html</a>.
		</p>

	</section>

	<section>
	<h3>Archiving old versions</h3>
	
		<p>
		Prior to version v4.3, R code were archived in <a href = "https://github.com/Geographie-cites/mobiliscope" target="_blank">github.com/Geographie-cites/mobiliscope</a>. This archive is no longer used after v4.3.  
		</p>

 	</section>

	</br>
	</br>
	</br>
	</br>

</div>


