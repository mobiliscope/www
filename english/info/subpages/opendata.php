<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
	<h2>Open data</h2>

</br>
	<p>
		The development of Mobiliscope is part of an open science approach. We do our utmost to ensure that the <b>data</b> complies with the FAIR principles: <b>F</b>indable, <b>A</b>ccessible, <b>I</b>nteroperable and <b>R</b>eusable.
	</br>
	</p>


	<section>
	<h3>Data</h3>

		<p>
			District hourly <b>data</b> (format .csv and .geojson) can be <b>downloaded</b> from the Mobiliscope platform (as well as the data dictionary). With these files, we have also attached a pdf document summarizing the terms of the ODbL license and the sources to be indicated. 
		</p>
		</section>

		<section>
		<h4>Download city by city via the tool</h4>
		
		<p>
			To download the data, <b>click the button</b> <img src="/dist/assets/download.svg" width="20px" height= "20px"/>
			<ul>
				<li> either above the central map to get hourly location data for every district of the selected city region (and according to the selected indicator and the representation mode)</li>
				<li> or to the side of the lower chart to get hourly values of segregation index (Duncan or Moran) for the whole region and according to the selected indicator.</li>
			</ul>
		</p>

		</section>

		<section>
		<h4>Downloading the whole data set</h4>

		<p>
			A folder containing <b>all the data</b> offered for download in the current version is also available in a </b> <a href="https://doi.org/10.5281/zenodo.11111161" target="_blank">Zenodo record</a>. They are made available under an open licence (OdBL).
		</p>

		<p>
			Using the data from Zenodo (and the software code from <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b></a> repository), users can run Mobiliscope locally on their computer: all the data will be displayed except for flows of "non-resident" people according to their districts of residence.
			</br>
			<i>For more information on this local installation, see the <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html#sec-instalocal" target = "_blank">dedicated section of the Administrator's Guide</a>.</i>
		</p>

		<p>
		 Data from previous Mobiliscope versions is also archived:
		 <ul>
		 	<li>Data from version <a href="https://zenodo.org/records/7822016" target="_blank">v4.2 [All data]</a> (licence ODbL)</li>
			<li>Data from version <a href="https://zenodo.org/records/7738571" target="_blank">v4.1 [Extract of French Data]</a> (licence ODbL)</li> 
			<li>Data from version <a href="https://zenodo.org/records/4900655" target="_blank">v4.0 [Extract of French Data]</a> (licence ODbL)</li> 
		</ul>

		</p>

		</section>

		<section>
		<h4>Other data available</h4>

		<p>
		For France, some other datasets used in the tool <i><a href="/en/info/toolbox/zoning">Filter by institutional zonings (France)</a></i> are also available for download.
		</p>
		

	</section>
	<section>
	<h3>Licence and citation</h3>
		<p>
		These open data are under <b><a href="https://spdx.org/licenses/ODbL-1.0.html" target="_blank">ODbL license</a></b>. Users are free to re-use and adapt these data as long as:

		<ul>
			<li>they redistribute a version of their adapted database under the ODbL licence</li>
			
			<li>they cite Mobiliscope </br>
			 ➔ Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C, Villard E (2024). Mobiliscope, an open platform to explore cities and social mix around the clock [v4.3]. www.mobiliscope.cnrs.fr doi:10.5281/zenodo.11111161</li>

			<li>they mention the original sources of the data (the 'Origin-Destination' surveys)</br>
			➔ the sources are indicated on the corresponding city map and in the licence terms.</li>
		</ul>
			 
		</br>
		</p>

	</section>

	 </br>
	 </br>	
	 </br>
	 </br>	


</div>
