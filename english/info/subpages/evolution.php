<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
<section>
<h2>Developments and updates</h2>
</section>

</br>

<section>
 	<h3>What's new in the latest version [v4.3]?</h3>
 	
        <p>
        <i>June 2024</i>
	
 	<ul>
 	
 	<li>Creation of a <b><a href="/en/info/toolbox/zoning" target="_blank">tool to filter</a></b> French Mobiliscope areas included in institutional zonings.</li>

 	<li>In the geovisualisation of French city regions, possibility to display <b>background maps</b> of public statistics zoning (2010 Urban Area Zoning; 2020 City Attraction Areas) and public action (Action Coeur de Ville and Petites Villes de Demain programmes) in addition to the perimeters of the Poverty Areas.</li>
   
   	<li>Hosting services on <b>HumaNum</b> Digital Humanities Platform.</li>
    
    	<li>Use of a high-performance <b>data storage service</b> (MinIO).</li>
    
    	<li>Reorganisation of data pre-processing code and software code in dedicated <b><a href = "https://gitlab.huma-num.fr/mobiliscope" target="_blank">gitlab</a></b> public repositories.</li>
    
    	<li>Publication of a new <b><a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">Administrator Guide</b></a> (in French).</li>
    
    	<li>Possibility for users to <b>run Mobiliscope locally on their own machines</b> by cloning the software code from <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b></a>, downloading the data via the <b><a href="https://doi.org/10.5281/zenodo.11111161" target="_blank">Zenodo</a></b> repository and following the instructions in the <b><a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html#sec-instalocal" target="_blank">Administrator Guide</b></a>.</li>
	
	</ul>
        </p>

  	</section>

  	<section>
	<h3>Updates</h3>

		<section>
 		<h4>Updating with recent data</h4>

 		<p>
		One of the challenges for the Mobiliscope team is to use the most recent data to update the geovisualisations of the ambient populations.
		</p>

		<p>
		The geovisualisation of the Valenciennes region (France) has been updated with data from the 2019 EMC² survey: <a href="/en/geoviz/valenciennes/" target='_blank'>Valenciennes région (<b>2019</b>)</a>. However, the geovisualisation initially built using 2011 data remains accessible: <a href="/en/geoviz/valenciennes2011/" target='_blank'>Valenciennes région (<b>2011</b>)</a>.
		</p>

		<p>
		The geovisualisation of the Paris region (France) proposed by default in Mobiliscope uses data from the 2010 Global Transport Survey (EGT): <a href="/en/geoviz/idf/" target='_blank'>Paris region (<b>2010</b>)</a>.
		</br>
		More recent data from the 2018-20 Global Transport Survey (taken from the Progedo catalogue) has been analysed and used as the basis for another geovisualisation: <a href="/en/geoviz/idf2020/" target='_blank'>Paris region (<b>2020</b>)</a>. However, the small sample of people surveyed per district in this recent survey severely limits the quality of estimates of the populations present per district and per hour (see <a href="https://gitlab.huma-num.fr/mobiliscope/data-process/blob/main/traitements_annexes/val_stat_egt2020/val_stat_egt2020.pdf" target='_blank'>additional analyses</a> - in French). For Paris region, it is therefore strongly recommended to continue using the geovisualisation based on <a href="/en/geoviz/idf/" target='_blank'><b>2010</b></a> data.
		</p>

		<p>Other updates are also being studied for cities with more recent data on population travel: <b>Toulouse</b> (2023 <i>vs.</i> 2013), <b>Nice</b> (2023 <i>vs.</i> 2009), <b>Angers</b> (2022 <i>vs.</i> 2012), <b>Caen</b> (2022 <i>vs.</i> 2011), <b>Bordeaux</b> (2021 <i>vs.</i> 2009), <b>Grenoble</b> (2020 <i>vs.</i> 2010), <b>Marseille</b> (2020 <i>vs.</i> 2009), <b>Montréal</b> (2018 <i>vs.</i> 2013), <b>Bogotá</b> (2023 <i>vs.</i> 2019)...
		</p>

		</section>

		<section>
 		<h4>Addition of new city regions</h4>
	
		<p>
		In France, new cities with origin-destination surveys surveys, such as <b>Toulon</b> (2022) and <b>Chambéry</b> (2022), could be added to the platform.
		</p>

		<p>
		Cities in other countries could also be added if accurate and representative surveys of people's daily trips are available:
			<ul>
			<li><b>Stockholm</b> (<i>Stockholm Travel Survey, 2015</i>) in Sweden.</li>
		
			<li><b>Amsterdam</b>, <b>Rotterdam</b>, <b>Utrecht</b>, <b>La Haye</b> (<i>Dutch National travel survey - Onderweg in Nederland ODiN</i>) in the Netherlands.</li>
		
			<li><b>Manchester</b> (<i>Greater Manchester Travel Diary Survey</i>) et <b>Londres</b> (<i>London Travel Demand Survey</i>) in Great Britain.</li>
			</ul>
		</p>

		</br>

		<p>
		<b>If you are interested in helping to update the data or adding new cities to Mobiliscope, please contact us!</br>
		<a href="mailto:mobiliscope@cnrs.fr">mobiliscope[at]cnrs.fr</a></b>.
		</p>

		</section>


	<section>
	<h3>Version history</h3>

	<p>
	Mobiliscope is a constantly evolving tool since the first version went online in 2017.
	</p>

	<h4><b>v1</b> & <b>v2</b> [2017]</h4> 
				
		<p>
			The first two versions concerned only the <b>Paris Region</b>, with an interface in <b>English</b>.

			<ul>
				<li><b>v1</b><i> [May 2017]</i></br>
				The first version (only in English) allows to explore the ambient population over 24 hours of the day according to three indicators (total population, level of education and socioprofessional status of the household) with two modes of representation (choropleth maps and proportional symbol maps). The studied population is over 16 years of age. A central map displays the ambient population per districts in the whole city region. A first chart shows the segregation indices (Duncan and Moran) for the whole region, a second chart shows the ambient in the district selected in the map.</li>
				</br>
				<li><b>v2</b><i> [September 2017]</i></br>
				In the second version, six new indicators are included (sex, age, income, occupational status, residential areas and activity). Flows (from residential districts) are added.</li>
			</ul>
		</p>
		
	</section>


	<section>
	<h4><b>v3</b> [2019-2020]</h4> 
		
		<p>
			A <b>bilingual interface</b> (French/English) is proposed. A new indicator for the last mode of transportation is added. New city regions from France but also from Canada are added to increase geographical coverage.  

			<ul>
				<li><b>v3.1.</b><i> [March 2019]</i></br>
				<b>21 French cities</b> are added. The age of the studied population is now set at 16 years and older. A new indicator for the last mode of transportation is added.
				</li>
				</br>

				<li><b>v3.2.</b><i> [December 2019]</i></br>
				With one more French city region (<b>Nîmes</b>).
				</li>
				</br>

				<li><b>v3.3.</b><i> [April 2020]</i></br>
				<b>6 Canadian cities</b> are included. In Canadian cities, indicators are slightly less numerous (sex, age, income, occupational status, activity carried out and last mode of transport used) and the age threshold is slightly lower (set at 15 years and older).
				</li>
			</ul>
		</p>



		</section>

		
		<section>
		<h4><b>v4</b> [2021-...]</h4>
			
		<p>
			The fourth version of the tool undergoes a <b>major overhaul of its graphic and cartographic interface</b>. New cities and functionalities are added. </br>
			District hourly <b>data</b> can be <b>downloaded</b> from the Mobiliscope platform. These open-data are under <b><a href="https://spdx.org/licenses/ODbL-1.0.html" target="_blank">ODbL license</a></b> (cf. <a href="/fr/info/open/license">ici</a>).
 			
		<ul>
 			<li>
 				<b>v4.0</b><i> [April 2021]</i>

				<ul style="margin-top: 0px">
					<li>Addition of <b>26 new French cities/regions</b> such as Rouen, Metz, Besançon, Brest, Le Havre, Poitiers and Tours but also the territories of Martinique and La Réunion.</li>
					
					<li><b>Open-data</b>. Hourly data of <b>French cities</b> can be downloaded from the Mobiliscope platform.</li>
				
					<li>Integration of 2 new indicators: (1) residents/non-residents of the area; (2) living (yes/no) in a 'Poverty Areas' (<i>i.e.</i>i> Quartiers Priortaires en Politique de la Ville - QPV). <i>For France only</i>.</li>

					<li>Addition of a new mode of representation (ambient population density - people per km²) is now available for the 'Whole population' indicator.</li>
				
					<li><i>OpenStreetMap</i> tiles can be displayed to explore cities more easily.</li>

					<li>Change of threshold values. The threshold previously applied to the maps and charts of the non-resident population (flow mode) has been changed. The threshold of 12 respondents below which no information was displayed (number of non-residents and flows between districts of presence and residence) has been removed. It has been replaced by a threshold of 6 (crude) respondents concerning only flows between districts of presence and residence.</li>

					<li>Additional changes: data for Valenciennes region have been updated from the EMC² 2019 survey ; 18 peripheral districts for the Bordeaux region (2009) have been added ; corrections were made on estimates of the ambient population in Canadian cities; segregation indices for the indicators 'activities' and 'mode of transport' have been deleted.</li>

				</ul>

			</li>

			</br>

 			<li>
 				<b>v4.1</b><i> [April 2022]</i>

 				<ul style="margin-top: 0px">

					<li>Addition of <b>3 new Latin American cities/regions</b>: Bogotá (Colombia), Santiago (Chile) and São Paulo (Brazil), with the addition of specific indicators and associated layers (<i>e.g.</i> indicator on informal work, TransMilenio network in Bogotá)</li>

					<li><b>Open-data</b>. Hourly data of <b>Canadian and Latin America cities</b> (as well as of French cities) can be downloaded from the Mobiliscope platform.</li>

					<li>Development of the Spanish interface to offer a <b>trilingual interface</b> (French, English, Spanish).</li>

					<li>Integration of an indicator regarding the household composition of respondents for all cities/regions.</li> 

					<li>Display of satellite and/or aerial images to help the user find his way around the cities.</li>

					<li>Readjustment of the computation of ambient population across hours. This readjustment induces a shift of one hour in hourly data compared to the data displayed in the previous versions of the tool: ambient population located in a district at a time <i>h</i> was previously counted at <i>h-1</i>.</li>
		 
					<li>Specification of URLs corresponding to displayed geovisualisations. For example, the proportion of women in Bogotá at 8am in the La Candelaria district can now be displayed directly with this <a target="_blank"  href = "/en/geoviz/bogota?m1=2&m2=2&m3=1&m4=part&t=8&s=UTAM94">URL</a>. Possibility to share specific geovisualisation using the 'Share view' button <img src="/dist/assets/share.svg" width="20px" height= "20px"/>. </li>

					<li>Improvement of the search tool by municipality/city name <img src="/dist/assets/search-black.svg" width="20px" height= "20px"/> :  district associated with the searched municipality/city is now automatically selected in the geovisualization (selection of the district in the map and the top chart).</li>

					<li>Modification of the last main mode of transport for Canadian cities: in case of multimodal trips, the last main mode of transport is now defined following this priority order: 1) Collective transportation; 2) Individual motor vehicle and 3) Soft mobility. This priority order is now similar to the one adopted for French and Latin American cities</li>
			
				</ul>
			</li>
		</br>

			<li>
 				<b>v4.2</b><i> [April 2023]</i>
 				
 				<ul style="margin-top: 0px">
 				
 					<li>Extensive <b>reorganisation of the software architecture</b> (web code refactoring) in order to guarantee stability, efficiency and durability of the platform and to facilitate future developments.</li>

 					<li><b>Refactoring of the R code</b> used for data-processing</b>. Data have also been <b>reorganised and simplified</b>.</li>

 					<li>For the choropleth maps, modification of the discretization methods now carried out "on the fly" city by city thanks to the <a href = "https://github.com/simogeo/geostats" target="_blank" >geostats.js</a> library. New class boundaries may consequently differ from those of v4.1, in particular for the indicators "Sex", "Area/Residence" and "Activity" whose class boundaries were previously similar for all cities.</li>
 							
 					<li>Introduction of stricter rules for the removal of double counts (<i>i.e.</i> respondents counted twice in the same district at a given hour) in order to correct some hourly estimates. These corrections mainly affect Canadian cities, and are very marginally in French and Latin American cities.</li>
 							
 					<li>Simplification of map and chart titles.</li>

 					<li>Creation of a <b><a href="https://doi.org/10.5281/zenodo.7822016" target="_blank">Zenodo record</a></b> gathering <b>all the data</b> proposed in open-data in the tool.</li> 

 				</ul>
 				
			</li>
	 	</p>		
		</section>
		
		
</br>
</br>
</br>
</br>
</br>
</br>	
</div>



