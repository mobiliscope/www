<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Teaching</h2>
	</section>


	<p>
    	Students of various ages can use the Mobiliscope to discover the cities and the daily mobility of their inhabitants.

		<ul>
			<li> In <b>primary schools</b>, Mobiliscope has proved to be a fun medium to let students explore the different types of trips (pendular, punctual and regular) and urban morphologyo. To make the experience more playful students were asked to answer a small quiz using tablets in which they can explore their cities and their neighbourhoods.</li>
			
			<li> In <b>secondary schools</b> and <b>universities</b>, Mobiliscope is also used in connection with urban studies (mobility, segregation, mechanisms of social inequality...) or cartographic/geovisualisation methods.</li>

		</ul>
	</p>


	<h4>
	<figure class="inline">
	<img src="/dist/assets/ecole2.jpg" alt="ecole2" width="220"/>
	<img src="/dist/assets/ecole3.jpg" alt="ecole1" width="220"/>
	<img src="/dist/assets/ecole4.jpg" alt="ecole2" width="220"/>
	</figure>
	</h4>

	</section>


	<section>
    <h3>Share your teaching experiences</h3>
	
	<p>	
		Don't hesitate to send us a message to share your teaching experiences with Mobiliscope.<br>
		We are very interested in your feedback. We can also help you set up specific actions around your neighbourhood, your town... 
	</p>
	
    </section>

    <br>
	 <br>
	 <br>


</div>
