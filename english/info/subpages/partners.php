<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
		<h2>Partners</h2>
	</section>

	<section>
	<p>
		Mobiliscope is being developed in two French public research laboratories associated with the <b>CNRS</b>: the <b>Géographie-cités</b> laboratory in Paris (since 2017) and the <b>LISST</b> laboratory in Toulouse (since 2024).

		<a href = "https://geographie-cites.cnrs.fr/en/" target="_blank" >
		<figure class="inline">
		  <img src="/dist/assets/geocites.png" alt="geocites" width="280"/>
		</figure>
		</a>

		<a href = "https://www.cnrs.fr/en" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/cnrs.png" alt="cnrs" width="90"/>
		</figure>
		</a>

		<a href = "https://lisst.univ-tlse2.fr/home" target="_blank" >
		<figure class="inline">
		  <img src="/dist/assets/logo-lisst.png" alt="lisst" width="215"/>
		</figure>
		</a>
	</p>

	<p>
		The other two main partners of Mobiliscope are Ined and ANCT.

		<a href = "https://www.ined.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/ined.png" alt="ined" width="170"/>
		</figure>
		</a>

		<a href = "https://agence-cohesion-territoires.gouv.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/anct.png" alt="anct" width="265"/>
		</figure>
		</a>

		</br>
	</p>

	<p>
		Mobiliscope benefits from the support of other institutional public partners in terms of access to data, IT infrastructures and funding.

		<a href = "http://www.progedo-adisp.fr/" target="_blank">
		<figure class="inline">
		 <img src="/dist/assets/progedo.png" alt="progedo" width="130"/>
		</figure>
		</a>

		<a href = "https://www.cerema.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/cerema.png" alt="cerema" width="255"/>
		</figure>
		</a>

		<a href = "https://www.huma-num.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/huma-num.png" alt="huma-num" width="95"/>
		</figure>
		</a>

		<a href = "https://www.transports.gouv.qc.ca/" target="_blank">
		<figure class="inline">
			<img src="/dist/assets/mintransportsqc.png" alt="mintransportsqc" width="170"/>
		</figure>
		</a>

		<a href = "http://labex-dynamite.com/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/labex.png" alt="labex" width="80"/>
		</figure>
		</a>

		<a href = "https://www.spherelab.org/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/labsphere.png" alt="spherelab" width="145"/>
		</figure>
		</a>

	</p>
	</section>

	<p>
		<button class="style-button mb50"><a href="/en/info/about/publications">Discover our papers and communications</a></button>
	</p>

</div>
