﻿<?php
include($_SERVER['DOCUMENT_ROOT'].'/data-settings/city.php');
//phpinfo();
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include($_SERVER['DOCUMENT_ROOT'].'/minio-settings.php');

$language = "en";
$page = '';
$subpage = '';
$pageMeta = '';
$sectionName = '';
$pagePath = '';
$pageCrumbs = "<a href='/".$language."'>Home</a>";


$curCity = (!empty($_GET['city']) && in_array($_GET['city'], array_keys($city)))? $_GET['city']: 'albi';


if( $bucket == 'mobiliscope' ) {
    $path =  $path . '/' . $curCity;
} else
    $path =  $hash;

if(!empty($_GET['hash']) ){
    $filters = getObject($s3Client, $bucket, $path,'filters.json');
    $filters_array = json_decode($filters, true);
    $curCity = $filters_array['request']['study'];
}

 // List of pages and their titles
    $pages = [
        'about'=>'About',
        'methods'=>'Methods',
        'open'=>'Open science',
        'multitask-tool'=>'Fields of application',
        'toolbox'=>'Toolbox',
    ];

    $subpages = [
        'about'=>[
            'summary'=>'In summary',
            'team'=>'Team',
            'partners'=>'Partners',
            'publications'=>'Publications & co',
        ],

        'methods'=>[
            'data'=>'Transforming trip dataset',
            'indicators'=>'Describing ambient population',
            'geovizualisation'=>'Geovizualisation',
            'evolution'=>'Developments and updates'
        ],

        'open'=>[
            'license'=>'Open-source code and documentation', 
            'opendata'=>'Open data',  
            'reuses'=>'Reproductible papers',
            'cite'=>'How to cite Mobiliscope?',

        ],

        'multitask-tool'=>[
            'scientific-tool'=>'Science & Research',
            'planning-tool'=>'Planning',
			'pedagogical-tool'=>'Teaching',
        ],

        'toolbox'=>[          
            'zoning'=>'Filter by institutional zonings <small>(France)</small>',
        ]
    ];


    // this description will be used by Google as the text under the title of the page
    // TITLE it should be max 70 char with keywords
    // META it should be max 230 char with human readable text
    $pageSEO = [
        //'about'=>['title'=>'Mobiliscope – To explore daytime and nighttime population in cities !','meta'=>'Cities are not static over the 24h period...Neighborhoods, social mix and urban segregation change according to people daily mobility.'],
        'summary'=>['title'=>'Mobiliscope - In summary','meta'=>'Cities are not static over the 24h period... Neighborhoods, social mix and urban segregation change according to people\'s daily mobility and their everyday geography.'],
        'team'=>['title'=>'Mobiliscope team','meta'=>'A web mapping platform supervised by Julie Vallée and developed by Aurélie Douet and Constance Lecomte. Also with Guillaume Le Roux, Hadrien Commenges and Elisa Villard.'],
        'partners'=>['title'=>'Mobiliscope partners','meta'=>'Developed in Géographie-cités lab (Paris, France), the spatio-temporal interface is supported by public partners:  CNRS, ANCT, Cerema and Labex DynamiTe.'],
        'publications'=>['title'=>'Mobiliscope – Papers and communications','meta'=>'Various audience (national or local planners, scientific world, education community etc.) may be interested in relation with social and urban geography, daily mobility, spatial justice, segregation, time geography, neighborhood effects, health geography.'],
       
        //'methods'=>['title'=>'Mobiliscope – some methodological information','meta'=>'Daily time matters! In the Mobiliscope, city maps and graphs change over the 24h period to explore dynamically copresence between rich and poor, women and men, active and retired people etc.'],
        'data'=>['title'=>'Mobiliscope – Transforming trip dataset','meta'=>'Using large public origin-destination surveys, the Mobiliscope expands traditional urban studies based on residential census and home-work commuting.'],
        'indicators'=>['title'=>'Mobiliscope –  Describing ambient population','meta'=>'Let’s explore hourly ambient population in cities according to their demographic (sex, age) and social (education level, socioprofessional status) profile. Transportation mode is also available. '],
        'geovizualisation'=>['title'=>'Mobiliscope - Geovizualisation methods','meta'=>'Spatio-temporal interface has been developed with d3.js library. Choose a city region in France and explore change from animated maps and hourly segregation and spatial autocorrelation indices (Duncan and Moran).'],
         'evolution'=>['title'=>'Mobiliscope - Developments and updates','meta'=>'A open platform constantly evolving since it was first launched in 2017. New cities and features are regularly added.'],
       
        //'news'=>['title'=>'Mobiliscope - News','meta'=>'To promote temporal approach in urban inequalities for various audience (national or local planners, scientific world, education community etc.).'],
        //'events'=>['title'=>'Mobiliscope - Events','meta'=>'Regular participation in large french audience events (Fête de la science; Nuit de la Géographie ; Festival International de Géographie (FIG) ; Salon Innovativ (SHS) ; ThéoQuant ; SAGEO…).'],


        //'open'=>
        'license'=>['title' => 'Mobiliscope – Open-source and documentation', 'meta'=> 'In the spirit of open science and FAIR principles. Open-data under ODbL licence. Developed with open source technologies (javascript, D3.js) and accesible under free and copyleft licence (AGPL) through a Github public repository.'],
        'opendata'=>['title' => 'Mobiliscope - Open-data','meta'=>'Open-data, downloadable from the platform or via a zenodo repository'],
        'reuses'=>['title' => 'Mobiliscope - Reproductible papers','meta'=>'Fully reproducible papers from Mobiliscope data'],
        'cite'=>['title' => 'How to cite Mobiliscope?','meta'=>'Mobiliscope citation model and associated doi'],

         // 'multitask-tool'=>['title'=>'Le Mobiliscope – a multitask tool','meta'=>'An interactive tool available for all those interested in urban and social inequalities (urban planners, scientific community, teachers, students etc.).'],
        
        'scientific-tool'=>['title'=>'Mobiliscope – For Science & Research','meta'=>'To add a daycourse of place approach in scientific literature related to urban inequalities, neighborhood or place effects, and spatial justice.'],
        'planning-tool'=>['title'=>'Mobiliscope – For Planning','meta'=>'Where? ? When? Who ? How ? To do what ? These questions are frequent in urban planning. Policymakers can find some answers in the Mobiliscope using interactive platfom.'],
        'pedagogical-tool'=>['title'=>'Mobiliscope - For Teaching','meta'=>'In elementary, middle or high schools let the students discover mobilities in urban areas and have fun.'],
        

        //'toolbox'=>
        'zoning'=>['title'=>'Mobiliscope - Filter by institutional zonings','meta'=>'Knowing the French Mobiliscope integrated into the zoning of public action'],
       
       

    ];




    if($section == "info"){
        $page = (!empty($_GET['page']) && in_array($_GET['page'], array_keys($pages) ) ) ? $_GET['page']: 'about';
        $subpage = (!empty($_GET['subpage']) && in_array($_GET['subpage'], array_keys($subpages[$page]) ) ) ? $_GET['subpage']: '';
        $sectionName = "Info";
        $pagePath = "/".$section."/".$page."/".$subpage;
        $pageName = $subpages[$page][$subpage];
        $pageCrumbs .= "<i class='fas fa-chevron-right'></i>
                <span class=\"cur-bc-page\">".$pages[$page]."</span>
                <i class='fas fa-chevron-right'></i>
                <h1 class=\"cur-bc-subpage\">".$subpages[$page][$subpage]."</h1>";
    }

    if($section == "geoviz"){
        $page = $curCity;
        $sectionName = "Maps";
        $pagePath = "/".$section."/".$page;
        $pageName = !empty($pages[$page]) ? $pages[$page] : $city[$page]['longName'][$language];
        $pageCrumbs = "";
    }

    if($section == "home"){
        $pagePath =  "/";
        $pageName = 'Cities around the clock';
        $pageCrumbs = "<i class='fas fa-chevron-right'></i><a href='/".$language."'>Home</a>";
    }

    if($section == "mobiliquery"){
        $pagePath =  "/";
        $pageName = 'Mobiliquest';
        $pageCrumbs = "<i class='fas fa-chevron-right'></i><a href='/".$language."'>Mobiliquest</a>";
    }



    $pageSEODefaut = [
        'title'=>'Mobiliscope - ' . $pageName . '',
        'meta'=> $pageName . ' - Mobiliscope, a free and open-source web mapping platform for the interactive exploration of neighborhood social composition and segregation around the clock in urban areas !'
    ];

    $titleSEO = ( !empty($subpage) && !empty($pageSEO[$subpage]) ) ? $pageSEO[$subpage]['title'] : ( !empty($pageSEO[$page]) ? $pageSEO[$page]['title'] : $pageSEODefaut['title'] ) ;
    $metaSEO  = ( !empty($subpage) && !empty($pageSEO[$subpage]) ) ? $pageSEO[$subpage]['meta']  : ( !empty($pageSEO[$page]) ? $pageSEO[$page]['meta'] :  $pageSEODefaut['meta'] );
    $pageTitle = !empty( $titleSEO ) ?  $titleSEO : $pageSEODefaut['title'];
    $pageMeta = !empty( $metaSEO ) ? $metaSEO : $pageSEODefaut['meta'];

    $curPage =  [
        'pagePath' => "/".$language.$pagePath,
        'pagePathNoLang' => $pagePath,
        'pageName' => $pageName,
        'pageCrumbs' => $pageCrumbs,
        'pageTitle' => $pageTitle,
        'pageMeta' => $pageMeta,
    ];


    //var_dump($curPage);
?>
