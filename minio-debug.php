<?php
/*
    Un fichier texte .env dans ../.env permet de changer l'environnement sans reconstruire le docker

    Les variables d'environnement sont stockées dans le fichier ../settings.json

    La variable d'environnement $_ENV['environnement'] est définie dans ce fichier et permet de se brancher sur n'importe quel environnement :
    //define('ENV', "local");
    //define('ENV', "test");
    //define('ENV', "production");

    Le code est versionné avec une version majeure
    Les données sur le minio sont versionnées avec une version mineure

    L'ensemble de ces 2 versions forment un chemin d'accès à la donnée dans minio

    La version du code majeure fonctionne avec toutes les versions de données mineures
    Le chemin d'accès au bucket est /mobiliscope/version_majeure/version_mineure

    Exemple pour brancher la version 2 du code avec le version 01022023 :
    majorVersion = '2'; (peut être vide pour la production)
    minorVersion = '01022023';
    et la donnée doit être dans le bucket : /mobiliscope/2/01022023/

    pour se brancher sur la dernière donnée de la version 3 du code, : /mobiliscope/3/latest/

    pour se brancher sur un extraction mobiliquest  : /mobiliquest/id_request

    Documentation du SDK https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-stream-wrapper.html
*/


/* LOG */
echo 'Begin log to minio-debug.log';
$log_file = "./minio-debug.log";
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('log_errors', TRUE);
ini_set('error_log', $log_file);
ini_set('memory_limit', '1024M');
/* END LOG */

error_log("\n -------------------Begin log to minio-debug.log \n\n", 3, $log_file); // LOG

require 'vendor/autoload.php';

$envFile = __DIR__ . '/../.env';

if(file_exists($envFile)) {
    $lines = file($envFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach ($lines as $line) {

        if(!empty($line) && strpos(trim($line), '=') > 0){
            $values = explode('=', $line, 2);
            $curEnv = trim($values[1]);
        }
    }
}

error_log("\n curEnv = ".$curEnv, 3, $log_file); // LOG

$_ENV['environnement'] = ( (!empty($curEnv) && in_array($curEnv, ['local','test','production']) ) ? $curEnv : 'production');

error_log("\n _ENV = " . print_r($_ENV, true), 3, $log_file); // LOG

$settings = [];

try {
    $minioSettings = json_decode(file_get_contents(__DIR__ . '/../settings.json'), true, $depth=512, JSON_THROW_ON_ERROR);
    error_log("\n minioSettings" . print_r($minioSettings, true), 3, $log_file); // LOG
} catch(Exception $e) {
    echo "minio settings not found !";
    error_log("\n minio settings not found !", 3, $log_file); // LOG
    error_log(print_r($e, true)); // LOG
}

//var_dump($minioSettings);

$settings = $minioSettings[$_ENV['environnement']];

error_log("\n settings = " . print_r($settings, true), 3, $log_file); // LOG

$s3Client = new Aws\S3\S3Client([
  'version' => 'latest',
  'region'  => 'us-east-1',
  'endpoint' => $settings["minioEndPoint"],
  'use_path_style_endpoint' => true,
  'credentials' => [
    'key'    => $settings["minioAccessKey"],
    'secret' => $settings["minioSecretKey"],
  ],
]);

//var_dump($s3Client, true);

//BEGIN LOG
//try{
//    $s3ClientDebug = print_r($s3Client, true);
//} catch(Exception $e){
//    var_dump($e);
//}
// END LOG

//error_log("\n s3Client = " . $s3ClientDebug, 3, $log_file); // LOG

$s3Client->registerStreamWrapper();

$hash = !empty($_GET['hash']) ? $_GET['hash'] : '';
$context = !empty($_GET['context']) ? $_GET['context'] : 'mobiliscope';

// default settings :
$bucket = 'mobiliscope';
$path =  (!empty($settings["majorVersion"]) ? ($settings["majorVersion"] . '/') : '' ) . $settings["minorVersion"] ;


if( $context == 'mobiliquest' ) {
    $bucket = 'mobiliquest';
    $path = $hash;
}

error_log("\n path = " . $path, 3, $log_file); // LOG
error_log("\n context = " . $context, 3, $log_file); // LOG


$the_menu_json = getObject($s3Client, $bucket, $path, 'albi/menu.json');

error_log("\n the_menu_json = " . $the_menu_json, 3, $log_file); // LOG

listObjects($s3Client, $bucket);




function getObject($s3Client, $bucket, $path, $key) {
    $result =  '';
    $log_file = "./minio-debug.log";

    $url = 's3://' . $bucket . '/' . $path . '/' . $key;
    //var_dump($url);

    error_log("\n getObject url= " . $url, 3, $log_file); // LOG

    try {
        //Retrieve object from bucket.
        if(file_exists($url)){
            $object = $s3Client->getObject([
                'Bucket' => $bucket,
                'Key' => $path . '/' . $key,
            ]);
            //print_r($object);
            error_log("\n getObject-object = " . $object, 3, $log_file); // LOG
            $result = $object['Body']->getContents();
            error_log("\n getObject-result = " . $result, 3, $log_file); // LOG
        } else {
            error_log("\n Le fichier demandé n\'existe pas = " . $url, 3, $log_file); // LOG
        }
        return $result;

    } catch(AwsException $e) {
        print_r('Error : ' . $e->getAwsErrorMessage());
    }
}

function listObjects($s3Client, $bucket) {
    $log_file = "./minio-debug.log";
    error_log("\n all_files = ", 3, $log_file); // LOG
    try {
        //Listing all objects
        $objects = $s3Client->listObjects([
            'Bucket' => $bucket,
        ]);

        if(!empty($objects['Contents'])){
            foreach ($objects['Contents'] as $object) {
                //var_dump($object);
                //echo $object['Key'] . "<br>";
                error_log("\n" . $object['Key'], 3, $log_file); // LOG

            }
        } else {
            //echo 'no objects :/';
            error_log("\n No objects", 3, $log_file); // LOG
        }

    } catch(AwsException $e) {
        print_r('Error: ' . $e->getAwsErrorMessage());
    }

}


echo '<br><br>End log to minio-debug.log';
error_log("\n ***********************End log to minio-debug.log \n\n", 3, $log_file); // LOG

