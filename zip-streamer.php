<?php
// Format d'appel
// http://dev.mobiliscope.parisgeo.cnrs.fr/zip-streamer.php?lang=fr&city=bogota&file=stacked/sex_prop_stacked.csv&geo=1
// old url https://mobiliscope.parisgeo.cnrs.fr/zip-streamer.php?files=./data/albi/stacked/sex_prop_stacked.csv,./data/albi/geo/secteurs.geojson,./data/readme.md,./data/odbl/odbl_fr.pdf

require 'vendor/autoload.php';
// we set $bucket and $path
include($_SERVER['DOCUMENT_ROOT'].'/minio-settings.php');

$file = (!empty($_GET['file'])) ? $_GET['file'] : '';
$city = (!empty($_GET['city'])) ? $_GET['city'] : '';
$lang = (!empty($_GET['lang'])) ? $_GET['lang'] : 'fr';
$layers = (!empty($_GET['layers'])) ? $_GET['layers'] : '0';

if(empty($file) || empty($city)) {
    echo 'Error wrong parameters : /zip-streamer.php?lang=[LANG]&city=[CITYNAME]&file=[FILE]&layers=[BOOL]. <br>Exemple : /zip-streamer.php?lang=fr&city=bogota&files=stacked/sex_prop_stacked.csv&layers=1';
    exit;
}

$tmp = explode('/',$file);

$fileName = $tmp[sizeof($tmp)-1];

// rename zip folder
if (strstr($file, 'choro')) {
    $name = str_replace('_choro_stacked.csv', '_pct', $fileName);
} else if (strstr($file, 'prop')) {
    $name = str_replace('_prop_stacked.csv', '_nb', $fileName);
} else if (strstr($file, 'flow')) {
    $name = str_replace('_flow_stacked.csv', '_nbNoResid', $fileName);
} else if (strstr($file, 'segreg')) {
    $name = str_replace('.csv', '', $fileName);
}

// Nom du zip en sortie
$zipfile = 'data_'. $city . '_' . $name .'.zip';

// enable output of HTTP headers
$options = new ZipStream\Option\Archive();
$options->setSendHttpHeaders(true);

// create a new zipstream object
$zip = new ZipStream\ZipStream($zipfile, $options);

// the data from minio
$filePath = $path . '/' . $city;
$data  = getObject($s3Client, $bucket, $filePath, $file);

// Add csv file 
$zip->addFile($city . '_'. $name . '.csv', $data);

// add secteur file
if( $layers == 1 ){
    $secteur = $city . '/layers';
    $data  = getObject($s3Client, $bucket, $path, $secteur.'/secteurs.geojson');
    $zip->addFile( $city . '_secteurs.geojson', $data);
}

// add licence file
$licence = './data-settings/odbl/odbl_' . $lang . '.pdf';
$data = file_get_contents($licence);
$zip->addFile('odbl_'. $lang . '.pdf', $data);


// add dico file
$dictionnary = './data-settings/readme.md';
$data = file_get_contents($dictionnary);
$zip->addFile('readme.md', $data);

// finish the zip stream
$zip->finish();

?>
