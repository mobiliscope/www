console.log('map-printer begins');

// langage
var currentPathname = window.location.pathname;
var version = currentPathname.slice(1,3);


function minisculeED(enq) {
  if(enq.includes(" ")){
    var str = enq.split(" ");
    var word1 = str[0].toLowerCase();
    var word2 = str[1].toLowerCase();
    return newStr = word1 + "-" + word2 ;

  } else{

      return enq.toLowerCase() ;
  }

}


function format(nbr) {
  var nombre = ''+nbr;
  var retour = '';
  var count = 0;
  var separator = { "fr" : " ", "en" : ",", "es" : " ", }; // separator according to version (language)
  console.log(separator);
  for(var i=nombre.length-1 ; i>=0 ; i--)
  {
    if(count!=0 && count % 3 == 0)
        retour = nombre[i]+ separator[version]+retour ;
    else
        retour = nombre[i]+retour ;
    count++;
  }
  return retour;
}


//console.log(translation['index']['enquetes'][version]);

function printMap1(){

  var width = 1066,
  height = 830;

  var projection =  d3.geo.mercator()
  .center([1.85, 46.3])
  .scale(3000)
  .translate([width / 2, height / 2]);

  var path = d3.geo.path()
  .projection(projection);

  var svg = d3.select("#map-container1").append("svg")
  .attr("id", "svg")
  .attr("width", width/1.2)
  .attr("height", height/1.2)
  .classed("svg-container", true)
  .attr("viewBox", "180 0 700 780")
  .classed("svg-content-responsive", true);

  //Pattern (hachures)
  var pattern = svg.append("defs")
    .append("pattern")
      .attr({ id:"hash", width:"5", height:"5", patternUnits:"userSpaceOnUse", patternTransform:"rotate(45)"})
    .append("rect")
      .attr({ width:"2", height:"10", transform:"translate(0,0)", fill:"#e29424" });

  var europe = svg.append("g");
  var cadre = svg.append("g");
  var fdGuy = svg.append("g");
  var dom = svg.append("g");
  var region = svg.append("g");
  var ed = svg.append("g");
  var edPtBis = svg.append("g");
  var edPt = svg.append("g");
  var villes = svg.append("g");

  var infoBulle = d3.select("#map-container1").append("div")
  .attr("class", "infoBulle")
  .style("opacity", 0);
  //position infobulle selon navigateur
  var ua = navigator.userAgent.toLowerCase();
  if (ua.includes('chrome')) {
    myparam = 270;
  } else {
    myparam = 0 ;
  };

  // var source = d3.select("#map-container1").append("div")
  //    .html("<h6>Source : CEREMA</h6>");

  europe.selectAll("path")
  .data(europeJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#edecec")
  .style("stroke", "white")
  .style("stroke-width", 0.2);

  cadre.selectAll("path")
  .data(cadreJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#dff2ff")
  .style("stroke", "white")
  .style("stroke-width", 1);

  fdGuy.selectAll("path")
  .data(fdGuyJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#edecec")
  .style("stroke", "white")
  .style("stroke-width", 0.2);

  region.selectAll("path")
  .data(regJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#bcc5d5")
  .style("stroke", "white")
  .style("stroke-width", 0.4);

  dom.selectAll("path")
  .data(domJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#bcc5d5")
  .style("stroke", "white")
  .style("stroke-width", 0.4);

  ed.selectAll("path")
  .data(edJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "url(#hash)")
  //.style("fill", "#e29424")
  .style("fill-opacity", 1);

  edPtBis.selectAll(".centroid")
  .data(edptJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 8 + "px")
  .style("fill", "none")
  .style("stroke", "#e29424")
  .style("stroke-width", 4);


  edPt.selectAll(".centroid")
  .data(edptJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 8 + "px")
  .style("fill", "#e29424")
  .style("stroke", "white")
  .style("stroke-width", 2)
  .on("mouseover", function(d) {
    d3.select(this).attr("r", 10 + "px");
    d3.select(this).style({"cursor" : "pointer"}) ;
    infoBulle.transition()
    .duration(100)
    .style("opacity", .8);
    infoBulle.html("<span style = 'font-family: OpenSans, sans-serif; font-size: 1.5em; line-height: 1em;'>"
    + d.properties.NAME_ED + "</span><br/>" + "(" + d.properties.DATE_ED + ")<br/>"
    + format(d.properties.nbrespondent) + " " +  translation['index']['enquetes'][version])
    .style("top", (d3.event.clientY - 30) + "px")
    .style("position", "fixed")
    .style("background-color", "white")
    .style("display", "block")
    .style("width", "auto")
    .style("left", (d3.event.clientX + 30) + "px");
  })
  .on("mouseout", function(d) {
    d3.select(this).attr("r", 8 + "px");
    infoBulle.transition()
    .duration(500)
    .style("opacity", 0 );
  })
  .on("click", function(d){
    var enq = d.properties.enquete;
    document.location.href = "/"+ version +"/geoviz/" + minisculeED(enq);
  });

  villes.selectAll(".centroid")
  .data(villeseurJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 2 + "px")
  .style("stroke", "grey")
  .style("stroke-width", 4);

  villes.selectAll("g")
  .data(villeseurJson.features)
  .enter()
  .append("text")
  .attr("class", "labels")
  .attr("dy", -6)
  .style("font-size", 14 + "px")
  .text(function(d) { return d.properties["NAME_" + version.toUpperCase()] ? d.properties["NAME_" + version.toUpperCase()] : d.properties.NAME_FR ; })

  var node = villes.selectAll("g.node")
  .data(villeseurJson.features)
  .enter()
  .append("g")
  .attr("class", "node");

  node.append("text")
  .attr("class", "labels")
  .attr("dy", -6)
  .style("font-size", 18 + "px")
  .attr("fill", "grey")
  .text(function(d) { return d.properties["NAME_" + version.toUpperCase()] ? d.properties["NAME_" + version.toUpperCase()] : d.properties.NAME_FR ; })
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; });

  $('#loader').hide();

} // end printMap1



function printMap2(){

  var width = 1060,
  height = 730;

  var projection =  d3.geo.mercator()
  .center([-71.85, 45])
  .scale(3500)
  .translate([width / 1.8, height / 1.5]);

  var path = d3.geo.path()
  .projection(projection);

  var svg = d3.select("#map-container2").append("svg")
  .attr("id", "svg")
  .attr("width", width/1.2)
  .attr("height", height/1.2)
  .classed("svg-container", true)
  .attr("viewBox", "100 0 800 780")
  .classed("svg-content-responsive", true);

  var usa = svg.append("g");
  var canada = svg.append("g");
  var eod = svg.append("g");
  var eodPtBis = svg.append("g");
  var eodPt = svg.append("g");
  var villes = svg.append("g");

  var infoBulle2 = d3.select("#map-container2").append("div")
  .attr("class", "infoBulle2")
  .style("opacity", 0);

  //position infobulle selon navigateur
  var ua = navigator.userAgent.toLowerCase();
  if (ua.includes('chrome')) {
    myparam2 = 850;
  } else {
    myparam2 = 0 ;
  };

  // var source = d3.select("#map-container2").append("div")
  //    .html("<h6>Source : Statistics Canada</h6>");

  usa.selectAll("path")
  .data(usaJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#edecec")
  .style("stroke", "white")
  .style("stroke-width", 0.4);

  canada.selectAll("path")
  .data(canadaJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#bcc5d5")
  .style("stroke", "white")
  .style("stroke-width", 0.4);

  eod.selectAll("path")
  .data(eodJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "url(#hash)")
  .style("fill-opacity", 1);

  eodPtBis.selectAll(".centroid")
  .data(eodptJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 8 + "px")
  .style("fill", "none")
  .style("stroke", "#e29424")
  .style("stroke-width", 4);

  eodPt.selectAll(".centroid")
  .data(eodptJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 8 + "px")
  .style("fill", "#e29424")
  .style("stroke", "white")
  .style("stroke-width", 2)
  .on("mouseover", function(d) {

    d3.select(this).attr("r", 10 + "px");
    d3.select(this).style({"cursor" : "pointer"}) ;
    infoBulle2.transition()
    .duration(100)
    .style("opacity", .8);
    infoBulle2.html("<span style = 'font-family: OpenSans, sans-serif; font-size: 1.5em; line-height: 1em;'>"
    + d.properties.NAME_ED + "</span><br/>" + "(" + d.properties.DATE_ED + ")<br/>"
    + format(d.properties.nbrespondent) + " " +  translation['index']['enquetes'][version])
    .style("top", (d3.event.clientY - 30) + "px")
    .style("position", "fixed")
    .style("background-color", "white")
    .style("display", "block")
    .style("width", "auto")
    .style("left", (d3.event.clientX + 30) + "px");
  })
  .on("mouseout", function(d) {
    d3.select(this).attr("r", 8 + "px");
    infoBulle2.transition()
    .duration(500)
    .style("opacity", 0 );
  })
  .on("click", function(d){
    var enq = d.properties.enquete;
    document.location.href = "/"+ version +"/geoviz/" + minisculeED(enq);
  });

  villes.selectAll(".centroid")
  .data(villesusaJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 2 + "px")
  .style("stroke", "grey")
  .style("stroke-width", 4);

  villes.selectAll("g")
  .data(villesusaJson.features)
  .enter()
  .append("text")
  .attr("class", "labels")
  .attr("dy", -6)
  .style("font-size", 14 + "px")
  .text(function(d) { return d.properties.enquete ; }) ;

  var node = villes.selectAll("g.node")
  .data(villesusaJson.features)
  .enter()
  .append("g")
  .attr("class", "node")

  node.append("text")
  .attr("class", "labels")
  .attr("dy", -6)
  .style("font-size", 18 + "px")
  .attr("fill", "grey")
  .text(function(d) { return d.properties.enquete ; })
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; });

  $('#loader').hide();

} // end printMap2


function printMap3(){

  var width = 1060,
  height = 730;

  var projection =  d3.geo.mercator()
  .center([-56, -14])
  .scale(900)
  .translate([width / 2, height / 2]);

  var path = d3.geo.path()
  .projection(projection);

  var svg = d3.select("#map-container3").append("svg")
  .attr("id", "svg")
  .attr("width", width/1.2)
  .attr("height", height/1.2)
  .classed("svg-container", true)
  .attr("viewBox", "100 0 800 780")
  .classed("svg-content-responsive", true);

  var ameL = svg.append("g");
  var cochbr = svg.append("g");
  var eod = svg.append("g");
  var eodPtBis = svg.append("g");
  var eodPt = svg.append("g");
  var ctryname = svg.append("g");
  var villes = svg.append("g");

  var infoBulle3 = d3.select("#map-container3").append("div")
  .attr("class", "infoBulle3")
  .style("opacity", 0);

  //position infobulle selon navigateur
  var ua = navigator.userAgent.toLowerCase();
  if (ua.includes('chrome')) {
    myparam2 = 850;
  } else {
    myparam2 = 0 ;
  };

  ameL.selectAll("path")
  .data(fdAmeLJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#edecec")
  .style("stroke", "white")
  .style("stroke-width", 0.4);

  cochbr.selectAll("path")
  .data(fdcochbrJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "#bcc5d5")
  .style("stroke", "white")
  .style("stroke-width", 0.4);

  eod.selectAll("path")
  .data(eodAmeLJson.features)
  .enter()
  .append("path")
  .attr("d", path)
  .style("fill", "url(#hash)")
  //.style("fill", "#e29424")
  .style("fill-opacity", 1);

  eodPtBis.selectAll(".centroid")
  .data(eodptAmeLJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 8 + "px")
  .style("fill", "none")
  .style("stroke", "#e29424")
  .style("stroke-width", 4);

  eodPt.selectAll(".centroid")
  .data(eodptAmeLJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 8 + "px")
  .style("fill", "#e29424")
  .style("stroke", "white")
  .style("stroke-width", 2)
  .on("mouseover", function(d) {

    d3.select(this).attr("r", 10 + "px");
    d3.select(this).style({"cursor" : "pointer"}) ;
    infoBulle3.transition()
    .duration(100)
    .style("opacity", .8);
    infoBulle3.html("<span style = 'font-family: OpenSans, sans-serif; font-size: 1.5em; line-height: 1em;'>"
    + d.properties.NAME_ED + "</span><br/>" + "(" + d.properties.DATE_ED + ")<br/>"
    + format(d.properties.nbrespondent) + " " +  translation['index']['enquetes'][version])
    .style("top", (d3.event.clientY - 30) + "px")
    .style("position", "fixed")
    .style("background-color", "white")
    .style("display", "block")
    .style("width", "auto")
    .style("left", (d3.event.clientX + 30) + "px");
  })
  .on("mouseout", function(d) {
    d3.select(this).attr("r", 8 + "px");
    infoBulle3.transition()
    .duration(500)
    .style("opacity", 0 );
  })
  .on("click", function(d){
    var enq = d.properties.enquete;
    document.location.href = "/"+ version +"/geoviz/" + minisculeED(enq);
  });

  var nodeb = ctryname.selectAll("g.node")
  .data(ctrynameJson.features)
  .enter()
  .append("g")
  .attr("class", "node");

  nodeb.append("text")
  .attr("class", "labels")
  .attr("dy", 15)
  .attr("dx", -12)
  .style("font-size", 20 + "px")
  .style("font-weight", "bold")
  .attr("fill", "#dfe4ed")
  .text(function(d) { return d.properties["NAME_" + version.toUpperCase()] ? d.properties["NAME_" + version.toUpperCase()] : d.properties.NAME_FR ; })
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; });

  villes.selectAll(".centroid")
  .data(villesAmeLJson.features)
  .enter()
  .append("circle")
  .attr("class", "centroid")
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
  .attr("r", 2 + "px")
  .style("stroke", "grey")
  .style("stroke-width", 4);

  villes.selectAll("g")
  .data(villesAmeLJson.features)
  .enter()
  .append("text")
  .attr("class", "labels")
  .attr("dy", -6)
  .style("font-size", 14 + "px")
  .text(function(d) { return d.properties["NAME_" + version.toUpperCase()] ? d.properties["NAME_" + version.toUpperCase()] : d.properties.NAME ; })

  var node = villes.selectAll("g.node")
  .data(villesAmeLJson.features)
  .enter()
  .append("g")
  .attr("class", "node")

  node.append("text")
  .attr("class", "labels")
  .attr("dy", -6)
  .style("font-size", 18 + "px")
  .attr("fill", "grey")
  .text(function(d) { return d.properties["NAME_" + version.toUpperCase()] ? d.properties["NAME_" + version.toUpperCase()] : d.properties.NAME ; })
  //.text(function(d) { return d.properties["NAME_" + version.toUpperCase()] ; })
  .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; });

  $('#loader').hide();

} // end printMap3

