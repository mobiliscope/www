<?php
/*
    Un fichier texte .env dans ../.env permet de changer l'environnement sans reconstruire le docker

    Les variables d'environnement sont stockées dans le fichier ../settings.json

    La variable d'environnement $_ENV['environnement'] est définie dans ce fichier et permet de se brancher sur n'importe quel environnement :
    //define('ENV', "local");
    //define('ENV', "test");
    //define('ENV', "production");

    Le code est versionné avec une version majeure
    Les données sur le minio sont versionnées avec une version mineure

    L'ensemble de ces 2 versions forment un chemin d'accès à la donnée dans minio

    La version du code majeure fonctionne avec toutes les versions de données mineures
    Le chemin d'accès au bucket est /mobiliscope/version_majeure/version_mineure

    Exemple pour brancher la version 2 du code avec le version 01022023 :
    majorVersion = '2'; (peut être vide pour la production)
    minorVersion = '01022023';
    et la donnée doit être dans le bucket : /mobiliscope/2/01022023/

    pour se brancher sur la dernière donnée de la version 3 du code, : /mobiliscope/3/latest/

    pour se brancher sur un extraction mobiliquest  : /mobiliquest/id_request

    Documentation du SDK https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-stream-wrapper.html
*/


require 'vendor/autoload.php';

$envFile = __DIR__ . '/../.env';

if(file_exists($envFile)) {
    $lines = file($envFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    foreach ($lines as $line) {
        if(!empty($line) && strpos(trim($line), '=') > 0){
            $values = explode('=', $line, 2);
            $curEnv = trim($values[1]);
        }
    }
} else { // if there is no .env file, we check the DNS
    if( $_SERVER['SERVER_NAME'] == "dev.mobiliscope.parisgeo.cnrs.fr" ){
        $curEnv = 'local';
    }

    if( $_SERVER['SERVER_NAME'] == "test.mobiliscope.cnrs.fr" ){
        $curEnv = 'test';
    }

    if( $_SERVER['SERVER_NAME'] == "mobiliscope.cnrs.fr" ){
        $curEnv = 'production';
    }

}


//var_dump($curEnv);

$_ENV['environnement'] = ( (!empty($curEnv) && in_array($curEnv, ['local','test','production']) ) ? $curEnv : 'production');

$settings = [];

try {
    $minioSettings = json_decode(file_get_contents(__DIR__ . '/../settings.json'), true, $depth=512, JSON_THROW_ON_ERROR);
} catch(Exception $e) {
    echo "minio settings not found !";
}
//var_dump($minioSettings);

$settings = $minioSettings[$_ENV['environnement']];

$s3Client = new Aws\S3\S3Client([
  'version' => 'latest',
  'region'  => 'us-east-1',
  'endpoint' => $settings["minioEndPoint"],
  'use_path_style_endpoint' => true,
  'credentials' => [
    'key'    => $settings["minioAccessKey"],
    'secret' => $settings["minioSecretKey"],
  ],
]);


$s3Client->registerStreamWrapper();

$hash = !empty($_GET['hash']) ? $_GET['hash'] : '';
$context = !empty($_GET['context']) ? $_GET['context'] : 'mobiliscope';

// default settings :
$bucket = 'mobiliscope';
$path =  (!empty($settings["majorVersion"]) ? ($settings["majorVersion"] . '/') : '' ) . $settings["minorVersion"] ;

if( $context == 'mobiliquest' ) {
    $bucket = 'mobiliquest';
    $path = $hash;
}

function getObject($s3Client, $bucket, $path, $key) {
    $result =  '';

    $url = 's3://' . $bucket . '/' . $path . '/' . $key;
    //var_dump($url);

    try {
        //Retrieve object from bucket.
        if(file_exists($url)){
            $object = $s3Client->getObject([
                'Bucket' => $bucket,
                'Key' => $path . '/' . $key,
            ]);
            //print_r($object);
            $result = $object['Body']->getContents();
        } else {
            echo 'Le fichier demandé n\'existe pas : ' . $url ;exit;
        }
        return $result;

    } catch(AwsException $e) {
        print_r('Error : ' . $e->getAwsErrorMessage());
    }
}




