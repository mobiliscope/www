

   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'about' ? 'current-page' : ''; ?>">À propos</a>
      <div class="left-menu-niv2">
        <a href="/fr/info/about/summary"><div class="item">En quelques mots...</div></a>
        <a href="/fr/info/about/team"><div class="item">Équipe</div></a>
        <a href="/fr/info/about/partners"><div class="item">Partenaires</div></a>
        <a href="/fr/info/about/publications"><div class="item">Publications & co</div></a>
     </div>
   </div>

   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'methods' ? 'current-page' : ''; ?>">Méthodes</a>
     <div class="left-menu-niv2">
        <a href="/fr/info/methods/data"><div class="item">Transformation des déplacements</div></a>
        <a href="/fr/info/methods/indicators"><div class="item">Caractérisation de la population présente</div></a>
        <a href="/fr/info/methods/geovizualisation"><div class="item">Géovisualisation (carto)graphique</div></a>
        <a href="/fr/info/methods/evolution"><div class="item">Evolutions et actualisations</div></a>
    </div>
   </div>

     <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'open' ? 'current-page' : ''; ?>">Science ouverte</a>
     <div class="left-menu-niv2">
        <a href="/fr/info/open/license"><div class="item">Code ouvert et documenté</div></a>
        <a href="/fr/info/open/opendata"><div class="item">Données libres et réutilisables</div></a>
        <a href="/fr/info/open/reuses"><div class="item">Articles reproductibles</div></a>
        <a href="/fr/info/open/cite"><div class="item">Comment citer le Mobiliscope?</div></a>
    </div>
   </div>

    <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'multitask-tool' ? 'current-page' : ''; ?>">Champs d'applications</a>
     <div class="left-menu-niv2">
        <a href="/fr/info/multitask-tool/scientific-tool"><div class="item">Recherche</div></a>
        <a href="/fr/info/multitask-tool/planning-tool"><div class="item">Aménagement</div></a>
        <a href="/fr/info/multitask-tool/pedagogical-tool"><div class="item">Enseignement</div></a>
    </div>
   </div>

   
     <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'toolbox' ? 'current-page' : ''; ?>">Boite à outils</a>
     <div class="left-menu-niv2">
      <a href="/fr/info/toolbox/zoning"><div class="item">Filtrer selon les zonages institutionnels <small>(France)</small></div></a>
    <!--<a href="/fr/info/toolbox/educ"><div class="item">Découvrir les supports ludo-pédagogiques</div></a>-->
    <!--<a href="/fr/info/toolbox/mobiliquest"><div class="item">Faire des requêtes avec le <b><i>Mobiliquest</i></b></div></a>-->
    <!-- PREVOIR UNE DEMO PAS A PAS ??? -->
    </div>
    </div>


<!--      <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'carbon' ? 'current-page' : ''; ?>">CARBONSCOPE
     </a>
    <div class="left-menu-niv2">
        <a href="/fr/info/carbon/x-pourquoi"><div class="item">Pourquoi ?</div></a>
        <a href="/fr/info/carbon/x-comment"><div class="item">Comment ?</div></a>
        <a href="/fr/info/carbon/x-go"><div class="item">GO</div></a>
    </div>
    </div> -->
