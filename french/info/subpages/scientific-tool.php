<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Un outil pour la recherche</h2>
	</section>

	<section>
		<p>
		Le Mobiliscope se situe à l'intersection de plusieurs champs de recherche, dans le prolongement des travaux de la <i>time-geography</i>.
		</p>

		<p>
			Dans les travaux scientifiques sur la <b>ségrégation</b> sociale ou les <b>effets de quartier</b>, la mobilité quotidienne des populations demeure sous-étudiée. Les divisions socio-spatiales des villes et les logiques d'entre-soi sont essentiellement analysées selon les lieux des résidence. Pourtant, les déplacements quotidiens peuvent (i) conduire les populations à être exposées à d'autres quartiers que leurs seuls quartiers de résidence et (ii) occasionnent des modifications dans la composition sociale des quartiers et dans les mesures de la ségrégation sociale des villes (<a href = "/pdf/2017_SegregationAroundTheClock.pdf"target="_blank">Le Roux et al., 2017</a>). Ces dynamiques quotidiennes gagnent à être considérées lorsqu'on analyse les logiques spatiales relatives à la production et à la reproduction des inégalités sociales (éducation, emploi, santé etc.).
		</p>

		<p> 
			Les rythmes quotidiens de la ville peuvent ainsi être analysés en fonction de la position socio-économiques des individus, mais également en fonction de leur genre. Des analyses issus du Mobiliscope ont ainsi mis en valeur l'<b>occupation genrée</b> de la ville au cours des 24 heures de la journée. En ce sens, le Mobiliscope permet de projeter dans l'espace et dans le temps les contraintes auxquelles les femmes et les hommes sont inégalement assujettis (<a href = "https://asmn.univ-nantes.fr/index.php?id=465"target="_blank">Vallée, 2020</a>).
		</p>

		<p>		
			Ces thématiques de recherches ont donné lieu à des articles, dont certains sont entièrement <a href="/fr/info/open/reuses">reproductibles</a> à partir des données du Mobiliscope.
		</p>
		

		<p>
			Les <a href="/fr/info/open/opendata">données issues du Mobiliscope</a> peuvent ainsi alimenter les refléxions sur l'efficacité des <b>politiques territorialement sélectives</b> traditionnellement centrées sur une approche statique et résidentielle. Quand les "quartiers prioritaires" sont pensés et définis selon le profil social de la population résidente, ce sont en effet un certain nombre des mécanismes de production et de reproduction des inégalités qui se trouvent ignorés (<a href = "https://shs.hal.science/halshs-03801117/document"target="_blank">Vallée, 2022</a>).

			<center>
				<figure class="inline">
			  	<img src="/dist/assets/aquariumfr.png" alt="aquariumfr" width="500"/>
			  	<figcaption>"La définition des quartiers concentrant les populations prioritaires au fil des heures"</br>in <a href = "/pdf/2017_TargetAreas.pdf" target="_blank">Challenges in targeting areas for public action. Target areas at the right place and at the right time,</a></figcaption>
			</figure>
			</center>


			Le Mobiliscope peut également enrichir les <b>modèles de simulation</b> (SMA) dans lesquels des données spatio-temporelles sur les déplacements ou les présences des populations gagnent à être intégrées. Doter les agents de comportements plus réalistes dans l'espace et dans le temps constitue un atout pour simuler les dynamiques à l’œuvre aussi bien au fil des heures que des années.
		</p>


	</section>

	<p>
		<button class="style-button mb50"><a href="/fr/info/multitask-tool/pedagogical-tool">Découvrez la portée pédagogique de l'outil</a></button>
	</p>



</div>
