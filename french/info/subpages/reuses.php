<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
	<h2>Articles reproductibles</h2>
	</br>

	<p>
		Le Mobiliscope a donné lieu à plusieurs articles scientifiques (voir <a href="/fr/info/about/publications">liste</a>). La mise à disposition des données du Mobiliscope en open data donne aussi la possibilité de publier des articles scientifiques  entièrement reproductibles.
	</p>
	</section>

	<section>
		<h3>Intersectional approach of everyday geography</h3>
	</section>

		<h4>Résumé</h4>
		<section>
			<p>
			Les variations heure par heure de la répartition spatiale des classes sociales, des groupes d’âges et des femmes et des hommes au sein des villes demeurent peu analysées et croisées dans la littérature sur la ségrégation, principalement centrée sur les lieux de résidence des classes sociales.
			</p>

			<p>
			Dans l’article <b>Intersectional approach of everyday geography</b> publié dans la revue <i>Environment and Planning B: Urban Analytics and City Science</i> (preprint sur <a href = "https://arxiv.org/abs/2106.15492v2" target="_blank">arXiv</a> et <a href = "https://hal.science/hal-04109508" target="_blank">HAL</a></li>), Julie Vallée et Maxime Lenormand utilisent les données du Mobiliscope de 2 572 secteurs géographiques répartis dans 49 villes françaises et leur périphérie (10 000 communes) pour étudier les variations horaires de la population présente, en distinguant pour chaque secteur des groupes de populations en fonction du genre, de l’âge et du niveau de diplôme des individus.
			</p>

			<p>
			Une première classification permet de faire ressortir cinq profils de secteurs (deux profils « attractifs en journée », deux profils « attractifs la nuit » et un profil « stable ») avec une distribution très inégale de ces profils de secteurs selon les groupes de population considérés. D’un point de vue géographique, l’analyse met aussi en évidence une distribution inégale de ces cinq profils selon le gradient d’urbanisation des secteurs (analyse intra-urbaine) et une distribution relativement similaire entre les 49 villes considérées (analyse inter-urbaine).
			</p>

			<figure class="inline">
		  	<img src="/dist/assets/fig-epb.png" alt="Intersectional approach of everyday geography" width="750"/>
			</figure>

			<p>
			Pour prolonger cette étude avec une perspective intersectionnelle, les auteurs ont ensuite mesuré pour chacun des secteurs le décalage (‘mismatch’) entre les profils horaires des différents groupes de populations en définissant comme « dominants » les groupes connus pour concentrer le pouvoir et le capital, à savoir les hommes, les personnes d’âge intermédiaire (35-64 ans) et les personnes avec un haut niveau de diplôme (Bac + 3). Ils montrent que ce sont surtout dans les secteurs avec une forte augmentation ou une forte diminution de la population totale présente au cours de la journée que se combinent de grandes divergences dans les profils horaires à la fois selon le genre, l’âge et le niveau de diplôme. Ils soulignent également que ce sont ces mêmes secteurs au sein desquels les profils horaires des « dominants » sont plus synchrones que ceux des « non dominants » (femmes, personnes âgées et personnes avec un faible niveau de diplôme). Cette lecture géographique et intersectionnelle des variations horaires de la population présente jette un nouvel éclairage sur les territoires où les pairs sont en coprésence au cours des 24 heures de la journée et sont donc potentiellement mieux placés pour interagir et défendre leurs intérêts communs.
			</p>
		</section>

		<section>
		<h4>Pour aller plus loin</h4>
			<p>
			<ul>
				<li>Télécharger les <b>données du Mobiliscope</b> - v4.1 (Extract of French Data)&nbsp;: <a href = "http://www.doi.org/10.5281/zenodo.7738571" target="_blank">dépôt zenodo</a> (licence ODbL)</li>
			
				<li>Accéder aux <b>codes R</b>&nbsp;: <a href = "https://gitlab.huma-num.fr/mobiliscope/intersectionality" target="_blank">dépôt gitlab</a> (licence GPLv3)</li>

				<li>Visualiser les <b>résultats</b>&nbsp;: <a href = "https://shiny.umr-tetis.fr/Intersectionality" target="_blank">plateforme shiny</a></li>

			</ul>
			</p>
		</section>
		
		
		<section>
		<h4>Référence</h4>

			<p>
			Vallée J, Lenormand M (2024). <a href = "https://dx.doi.org/10.1177/23998083231174025" target="_blank">Intersectional approach of everyday geography</a>, <i>Environment and Planning B: Urban Analytics and City Science</i>, 51(2), 347-365. doi:10.1177/23998083231174025. Preprint <a href = "https://arxiv.org/abs/2106.15492v2" target="_blank">arXiv</a> & <a href = "https://hal.science/hal-04109508" target="_blank">HAL</a>
			</p>
		</section>


	</br>
	<section>
		<i><h3>D'autres papiers reproductibles seront bientôt publiés!</h3></i>

	</section>


	 </br>
	 </br>	
	 </br>
	 </br>	


</div>
