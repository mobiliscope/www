<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
	<h2>Données libres et réutilisables</h2>

	</br>
	<p>
		Le développement du Mobiliscope s’inscrit dans une démarche de science ouverte. Nous faisons notre maximum pour que les <b>données</b> respectent les <b>principes FAIR</b>&nbsp;: «&nbsp;<b>F</b>aciles à trouver, <b>A</b>ccessibles, <b>I</b>nteropérables et <b>R</b>éutilisables&nbsp;».</br>
	</p>

	</section>

	<section>
	<h3>Données</h3>

		<p>
			Les données de <b>population présente par heure et par secteur</b>, ainsi que les <b>indices de ségrégation</b> associés, sont librement proposées au <b>téléchargement</b> (format .csv), accompagnées d'un <b>dictionnaire</b>, d'un <b>fichier géographique</b> (format .geojson) et d'un fichier résumant les <b>termes de la licence ODbL</b> et précisant les <b>sources initiales des données</b>. 
		</p>
		</section>

		<section>
		<h4>Téléchargement ville par ville via l'outil</h4>
		
		<p>
			Pour télécharger les données d'<b>une ville</b>, consultez la géovisualisation de la ville en question et cliquez sur le bouton <img src="/dist/assets/download.svg" width="20px" height= "20px"/> 
		<ul>
			<li> soit au dessus de la carte centrale pour obtenir les valeurs agrégées par secteur selon l'indicateur et le mode de représentation sélectionné</li>
			<li> soit à côté du graphique du bas pour obtenir les valeurs des indices de ségrégation pour l'ensemble de la région selon l'indicateur sélectionné et l'indice de ségrégation choisi.</li>
		</ul>
		</p>

		</section>

		<section>
		<h4>Téléchargement du jeu entier de données</h4>

		<p>
			<b>Toutes les données</b> utilisées dans la version actuelle de l'outil [v4.3] sont regroupées dans un même dossier et stockées dans un <b><a href="https://doi.org/10.5281/zenodo.11111161" target="_blank">dépot Zenodo</a></b>. Elles sont mises à disposition sous licence libre OBbL.
		</p>

		<p>
			A partir de ces données issues de Zenodo (et du code logiciel du dépôt <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b>)</a>, l'utilisateur peut faire tourner le Mobiliscope en local sur sa machine : toutes les données seront visualisables sauf les données sur les flux entre les secteur de résidence et de présence.</br>
			<i>Pour plus d'information sur cette installation en local, voir la <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html#sec-instalocal" target = "_blank">section dédiée</a> du guide Administrateur·rice.</i>
			
		</p>

		<p>
		Les données des versions antérieures de l'outil sont également archivées :
		<ul>
			<li>Données de la version <a href="https://zenodo.org/records/7822016" target="_blank">v4.2 [All data]</a> (licence ODbL)</li>
			<li>Données de la version <a href="https://zenodo.org/records/7738571" target="_blank">v4.1 [Extract of French Data]</a> (licence ODbL)</li> 
			<li>Données de la version <a href="https://zenodo.org/records/4900655" target="_blank">v4.0 [Extract of French Data]</a> (licence ODbL)</li>
		</ul> 
		</p>

		</section>

		<section>
		<h4>Autres données mises à disposition</h4>

		<p>
		 Pour la France, sont aussi proposés au télechargement:
		 <ul>
			<li>les <b>fichiers csv</b> de correspondance entre <b>communes et secteurs</b> accompagnés d'une documentation</b>&nbsp;: <a href="/data-settings/commune_secteur.zip" title="commune_secteur" download="commune_secteur.zip"><b>ici</b></a></li>
		 	<li>les <b>fichiers csv</b> de l'inscription des communes et des secteurs dans les différents <b>zonages de la statistique publique ou de l'action publique</b> (Action Coeur de Ville - ACV, Petites Villes de Demain - PVD, Zonage en Aires Urbaines -ZAU, Zonage des Aires d'Attraction des Villes - AAV, Grilles de densités communales) accompagnés d'une <b>documentation</b> : <a href="/data-settings/commune_secteur_zonages.zip" title="commune_secteur_zonages" download="commune_secteur_zonages.zip"><b>ici</b></a></li>
        </ul>
        <br>
		
		<i>Ces données sont utilisées dans l'outil <a href="/fr/info/toolbox/zoning">Filtrer selon les zonages institutionnels</a></b> français (ACV, PVD et QPV).</i>
		
		</p>


	</section>
	<section>
	<h3>License et modèle de citation</h3>
		<p>
		Les données du Mobiliscope sont mises à disposition sous <b>licence libre <a href = "https://spdx.org/licenses/ODbL-1.0.html" target="_blank">ODbL</a></b>.
		</p>
		<p>
		Elles sont donc librement réutilisables et modifiables sous réserve :
		<ul>
			<li>qu'elles demeurent sous licence ODbL</li>
			
			<li>que le Mobiliscope soit cité</br>
			 ➔ Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C, Villard E (2024). Mobiliscope, un outil libre pour explorer les villes et leur mixité sociale au cours des 24 heures de la journée [v4.3]. www.mobiliscope.cnrs.fr doi:10.5281/zenodo.11111161</li>
			
			<li>que les sources initiales des données (les enquêtes sur les déplacements quotidiens) soient mentionnées</br>
			➔ les sources sont indiquées sur la carte de la ville correspondante et dans les termes de licence de réutilisation.</li>
		</ul>
		
		</br>


	</p>

	</section>

	 </br>
	 </br>	
	 </br>
	 </br>	


</div>
