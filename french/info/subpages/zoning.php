<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">

	<section>
		<h2>Filtrer selon les zonages institutionnels (France)</h2>
        <center>
            <h4>Connaitre les territoires français du Mobiliscope intégrés aux programmes de l'action publique&nbsp;:</br>
            <i>Actions Coeur de Ville (ACV)</i>, <i>Petites Villes de Demain (PVD)</i> et <i>Quartiers Prioritaires en Politique de la Ville (QPV)</i>.</h4>
        </center>
	</section>

    </br>

    <section>
        <h3>Action Coeur de Ville (ACV)</b></h3>
        
        <p> 
            Le Mobiliscope couvre <b>88</b> des 235 <b>communes</b> du programme <i><a href = "https://agence-cohesion-territoires.gouv.fr/action-coeur-de-ville-42" target="_blank">Action Coeur de Ville</a></i> - <small>liste de novembre 2021.</small>
        </p>

        <p>
            Ces 88 communes ACV sont listées dans le tableau ci-dessous.
            <br>
            <small>
                <ul>
                    <li>Le tableau peut être trié, filtré et téléchargé au format csv.</li>
                    <li>En cliquant sur le nom de commune, vous serez redirigé vers la geovisualisation associée.</li>
                    <li>La catégorie <i>Type de correspondance</i> indique si la commune est découpée en plusieurs secteurs du Mobiliscope ("Commune multi-secteurs"), correspond à un seul secteur ("Commune mono-secteur") ou est incluse dans un secteur composé de plusieurs communes ("Commune infra-secteur"). Quelques rares "Cas particuliers" sont liés à des fusions de communes ou à des emprises communales discontinues.</li>
                </ul>
            </small>
        
            <br>

            <?php include('table/acv_table.html'); ?> 
        </p>
    
    <p class="ss-style-dots" ></p>

    </section>


	<section>
		<h3>Petites Villes de Demain (PVD)</h3>

        <p> 
            Le Mobiliscope couvre <b>502</b> des 1643 <b>communes</b> du programme <i><a href = "https://agence-cohesion-territoires.gouv.fr/petites-villes-de-demain-45" target="_blank">Petites Villes de Demain</a></i> - <small>liste d'avril 2023.</small>
        </p>

        <p>
            Ces 502 communes PVD sont listées dans le tableau ci-dessous.
            <br>
            <small>
                <ul>
                    <li>Le tableau peut être trié, filtré et téléchargé au format csv.</li>
                    <li>En cliquant sur le nom de commune, vous serez redirigé vers la geovisualisation associée.</li>
                    <li>La catégorie <i>Type de correspondance</i> indique si la commune est découpée en plusieurs secteurs du Mobiliscope ("Commune multi-secteurs"), correspond à un seul secteur ("Commune mono-secteur") ou est incluse dans un secteur composé de plusieurs communes ("Commune infra-secteur"). Quelques rares "Cas particuliers" sont liés à des fusions de communes ou à des emprises communales discontinues.</li>
                </ul>
            </small>
            
            <br>

            <?php include('table/pvd_table.html'); ?>
        </p>

    <p class="ss-style-dots" ></p>

    </section>

	<section>
		<h3>Quartiers Prioritaires en Politique de la Ville (QPV)</h3>

        <p>
            <b>686</b> des 2572 <b>secteurs</b> du Mobiliscope comprennent des <b>Quartiers Prioritaires en Politique de la Ville</b> (QPV) : ce sont ainsi 871 des 1514 QPV existants en France qui sont couverts par le Mobiliscope.
        </p>

        <p>
            Ces 686 secteurs avec QPV sont listés dans le tableau ci-dessous.
            <br>
            <small>
                <ul>
                    <li>Le tableau peut être trié, filtré et téléchargé au format csv.</li>
                    <li>En cliquant sur le nom du secteur, vous serez redirigé vers la geovisualisation associée.</li>
                    <li>La catégorie <i>Taux de converture</i> correspond à la part de la surface du secteur du Mobiliscope qui est couverte par un/des QPV.</li>
                    <li>Les QPV pris en compte sont issus de la <a href = "https://sig.ville.gouv.fr/page/198/les-quartiers-prioritaires-de-la-politique-de-la-ville-2015" target="_blank"> géographie prioritaire de <b>2015</b></a> (et non de la nouvelle géographie prioritaire <a href = "https://sig.ville.gouv.fr/page/224/les-quartiers-prioritaires-de-la-politique-de-la-ville-2024" target="_blank">actualisée en <b>2024</b></a>).</li>
                </ul>
            </small>
            
            <br>

            <?php include('table/qpv_table.html'); ?> 
        </p>

        <p> 
            D'autres fonctionnalités sont disponibles dans le Mobiliscope pour explorer les rythmes quotidiens des Quartiers Prioritaires en Politique de la Ville. Il est possible :
            <ul>
                <li>d'afficher les limites des QPV (de 2015) sur la carte  <img src="/dist/assets/layers2.png" width="25px" height= "25px"/></li>
                <li>de qualifier (en nombre et en %) la population présente selon son lieu de résidence dans ou en dehors d'un QPV</li>
            </ul>
        </p>

    <p class="ss-style-dots" ></p>

    </section>

    <section>
        <h3>Méthodologie et jeux de données en open-data</h3>

        <p>
          <b>49 villes/régions</b> françaises sont accessibles dans la version actuelle du Mobiliscope. Ces 49 villes/régions françaises sont découpées en <b>2572 secteurs</b> qui couvrent au total plus de 10&nbsp;000 communes au sein desquelles réside <b>60% de la population</b> du pays.</br>
          <i> Note : 20 secteurs français (et 79 communes) sont intégrés à plusieurs enquêtes/géovisualisations.</i>
        </p>

        <h4>Etape 1.</h4> 

        <p>
            On identifie quatre types de correspondance entre communes -> secteurs : 
            <ul>
                <li>Une commune est découpée en plusieurs secteurs ("commune multi-secteurs"). C'est souvent le cas des (grandes) villes.</li>
                <li>Une commune correspond à un seul secteur ("commune mono-secteur"). </li>
                <li>Une commune est incluse dans un secteur plus large composé de plusieurs communes ("commune infra-secteur"). Ce cas de figure se retrouve principalement dans les espaces périphériques et ruraux.</li>
                <li>Les rares "cas particuliers" (2%) sont par exemple liés à des fusions de communes ou à des emprises communales discontinues.</li>
            </ul>
        </p>

        <p>
            Les <b>fichiers csv</b> de correspondance <i>commune->secteur</i> (et aussi <i>secteur->commune</i>) du Mobiliscope sont téléchargeables 
            <a href="/data-settings/commune_secteur.zip" title="commune_secteur" download="commune_secteur.zip"><b>ici</b></a> accompagnés d'une <b>documentation</b> (descriptif de la méthode et dictionnaire des variables).
        </p>

        <h4>Etape 2.</h4> 

        <p>
            Un croisement est effectué avec les communes des programmes <i>Action Coeur de Ville (ACV)</i> et <i>Petites Villes de Demain (PVD)</i> (ainsi qu'avec le zonage en Aires Urbaines (ZAU), le zonage des Aires d'Attraction des Villes (AAV) et les grilles de densités communales). 
        </p>

        <p>
            Les <b>fichiers csv</b> de l'inscription des <b>communes</b> (et aussi des <b>secteurs</b> du Mobiliscope) dans les différents zonages sont téléchargeables <a href="/data-settings/commune_secteur_zonages.zip" title="commune_secteur_zonages" download="commune_secteur_zonages.zip"><b>ici</b></a> accompagnés d'une <b>documentation</b> (descriptif des jeux de données utilisés pour les zonages, descriptif de la méthode et dictionnaire des variables).
        </p>

    </br>
    </br>
    </br>
    </br>

    </section>

</div>


<script type="text/javascript">
$(document).ready(function () {

	// DataTable PVD
	$('#pvdtable thead tr')
        .clone(true)
        .addClass('filterspvd')
        .appendTo('#pvdtable thead');

    $('#pvdtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'pvd_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,1])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filterspvd th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    console.log(title);
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filterspvd th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([2,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filterspvd th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#pvdtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });

    

    // DataTable ACV
    $('#acvtable thead tr')
        .clone(true)
        .addClass('filtersacv')
        .appendTo('#acvtable thead');

    $('#acvtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'acv_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,1])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersacv th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filtersacv th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([2,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersacv th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#acvtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });


	// DataTable QPV
    $('#qpvtable thead tr')
        .clone(true)
        .addClass('filtersqpv')
        .appendTo('#qpvtable thead');

    $('#qpvtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'qpv_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,2])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersqpv th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filtersqpv th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([1,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersqpv th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#qpvtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });
	
});

</script>
