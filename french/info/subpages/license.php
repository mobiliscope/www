<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Code ouvert et documenté</h2>

	</br>
	<p>
		Dans une démarche de science ouverte, nous rendons disponibles les <b>codes</b> utilisés dans le cadre du projet Mobiliscope. 
	</p>
	</section>


	<section>
	<h3>Code R de traitement de la donnée</h3>

			<p>
				Les données agrégées et pondérées (qui ne permettent pas la réidentification des personnnes enquêtées) sont générées grâce à deux scripts R&nbsp;: 
				<ul>
					<li>le premier transforme les données de déplacements issues des enquêtes en données de présences horaires</li> 
					<li>le second produit l’ensemble des fichiers nécessaires à la géovisualisation des populations présentes par heure et par secteur, pour chaque indicateur</li>
				</ul>
			</p>

			<p>
				Les scripts de pré-traitement des données liées au Mobiliscope sont dans le dépôt <a href = "https://gitlab.huma-num.fr/mobiliscope/data-process" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>data-process</b></a>.
			</p>

			<p>
				Les données produites sont stockées dans le service de stockage <i>MinIO</i>, sous forme de fichiers geojson pour les données spatiales et de fichiers csv pour les données attributaires sans géométrie. Une grande partie de ces données sont <a href="/fr/info/open/opendata">librement accessibles et réutilisables</a>.
			</p>
				
	</section>
	
	<section>
	<h3>Code logiciel</h3>

			<p>
				Le Mobiliscope est développé en HTML, CSS (framework <a href = "https://minicss.org/" target = "_blank"> mini.css</a>), javascript et PHP. La géovisualisation a été codée en javascript et s’appuie, pour l'interactivité des cartes et graphiques, sur les libraires <a  href = "https://d3js.org/" target="_blank" >D3.js</a> - développé par Mike Bostock - et <a href="http://leafletjs.com" title="A JS library for interactive maps" target="_blank">Leaflet</a>. La superposition des tuiles OSM et des cartes dessinées avec D3.js a été rendue possible par l'utilisation du plugin <a href="https://github.com/teralytics/Leaflet.D3SvgOverlay" target="_blank">Leaflet.D3SvgOverlay</a>.
			</p>

			<p>
				Le code source des pages web du Mobiliscope est disponible dans le dépôt <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b></a>. C’est à partir de ce code source qu’est déployé le Mobiliscope sur le serveur Huma-Num.
			</p>

				
	</section>

	<section>
	<h3>License et modèle de citation</h3>
		<p>
		Ces codes sont mis à disposition sous <b>licence libre <a href = "https://spdx.org/licenses/AGPL-3.0-or-later.html" target="_blank">AGPL</a></b>. Cette licence AGPL autorise toute redistribution ou modification du code sous réserve  :
		<ul>
			<li>qu'il demeure sous licence AGPL</li>

			<li>que le Mobiliscope soit cité</br>
			 ➔ Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C,  Villard E (2024). Mobiliscope, un outil libre pour explorer les villes et leur mixité sociale au cours des 24 heures de la journée [v4.3]. www.mobiliscope.cnrs.fr doi:10.5281/zenodo.11111161.</li>
		</ul>	
		</p>
	
	</section>

	<section>
	<h3>Documentation</h3>

		<p>
			Un nouveau <b>Guide Administrateur·rice</b> a été rédigé. Il décrit les codes utilisés, fait la synthèse des du fonctionnement des services et précise comment maintenir l'outil et faire des mises à jour.</br>
			Ce guide est librement consultable <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">https://mobiliscope.quarto.pub/guide_admin/guide_admin.html</a>.
		</p>

	</section>

	<section>
	<h3>Archivage des anciennes versions du code</h3>
	<p>
		Avant la version v4.3, le code R de traitement de la donnée et le code logiciel étaient archivés dans <a href = "https://github.com/Geographie-cites/mobiliscope" target="_blank">github.com/Geographie-cites/mobiliscope</a>. Cette archive n'est plus utilisée à partir de la v4.3.
	</p>

 	</section>

	</br>
	</br>
	</br>
	</br>

</div>
