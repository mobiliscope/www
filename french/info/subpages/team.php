<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Équipe</h2>
	</section>

	<section>
	<h3>Membres</h3>


    <p>
		<b><a href = "https://lisst.univ-tlse2.fr/accueil/julie-vallee#/" target="_blank" >Julie Vallée</a></b> est directrice de recherche en géographie au Centre National de la recherche Scientifique (CNRS). Elle est membre du laboratoire LISST (Toulouse) et associée au laboratoire Géographie-cités (Paris).</br> 
		Julie est la responsable du Mobiliscope. </br> 
		Ses travaux concernent les mobilités quotidiennes des populations, les phénomènes de ségrégations et les effets de lieu. Avec le Mobiliscope, Julie explore et met en lumière les rythmes quotidiens des espaces et les inégalités sociales associées.</br>
		
	</p>

	<p>
		<b><a  target="_blank" href = "http://crh.ehess.fr/index.php?9040">Aurélie Douet</a></b> est géomaticienne / cartographe.</br>
		Elle a été en charge  des développements du Mobiliscope pendant plus de 4 ans. On lui doit notamment l'extension de l'outil (initialement centré sur l'Île-de-France) aux autres villes françaises, l'intégration des villes d'Amérique latine et plus récemment la réorganisation de l'architecture logicielle et des protocoles de traitement des données. Depuis décembre 2023 Aurélie est ingénieure d'études au Centre de Recherches Historiques (EHESS/CNRS) et continue de suivre avec intérêt les avancements du projet. 
	</p>

	<p>
		<b><a href = "https://www.ined.fr/fr/recherche/chercheurs/Le+Roux+Guillaume" target="_blank">Guillaume Le Roux</a></b> est chargé de recherche à l'Institut National d'Etudes Démographiques (Ined) dans l'unité Logement, Inégalités Spatiales et Trajectoires.</br> 
		Ses recherches portent sur les relations entre mobilités sociales, mobilités spatiales et recompositions des territoires sous influence urbaine. Guillaume a contribué en 2016 à l'exploration critique de la base de données de l'enquête globale transport (EGT) de l'Île-de-France afin de produire une analyse de la ségrégation sociale de la région francilienne aux différentes heures de la journée. Depuis 2021, il s'investit dans  l'intégration et l'analyse des rythmes quotidiens des villes d'Amérique latine.
	</p>


	<p>
		<b><a target="_blank" href = "https://geographie-cites.cnrs.fr/membres/hadrien-commenges/">Hadrien Commenges</a></b> est maître de conférences en géographie à l'Université Paris 1 Panthéon-Sorbonne et membre du laboratoire Géographie-cités.</br>
		Ses recherches s'inscrivent dans une approche quantitative de la géographie urbaine, en particulier dans le champ de la mobilité urbaine et des transports.
	</p>

	</section>

	<section>
	<h4>Anciennes membres</h4>

    <p>
		<b>Constance Lecomte</b> a développé en 2017 les deux premières versions du Mobiliscope centrées sur l'Île-de-France.</br>
		Elle est ensuite restée fortement impliquée dans le projet lorsqu'elle travaillait à l’Observatoire des Territoires au sein de l'Agence nationale de la cohésion des territoires (ANCT).
	</p>

    
	<p>
		<b>Elisa Villard</b> a fait partie de l'équipe pendant plusieurs mois en 2019. Elle s'est occupée d'inclure les villes canadiennes dans le Mobiliscope (dans la continuité de son stage de fin d'études à Montréal).</br>
	</p>

	</section>

	<section>
	<h3>Étudiant·e·s</h3>

	<p>
	Plusieurs étudiant·e·s ont réalisé des stages en lien avec le Mobiliscope.

	<p>
	<b>Robin Demé</b> (Master 2 Démographie - Expertise en Sciences de la Population - Université Paris 1 Panthéon-Sorbonne). 2023. <i>Analyser l’évolution de la population résidente francilienne sous l’angle de la fréquentation quotidienne. Etude de cas de l’Ile-de-France entre 2008 et 2018</i>. Stage co-encadré par Guillaume Le Roux (INED) et Célio Sierra-Paycha (CRIDUP).
	</p>
	
	<p>
	<b>Baptiste Larrouy</b> (École Centrale Nantes - filière Villes numériques). 2021. <i>Étude de la diversité des consommations d’énergies des territoires français suivant leurs fréquentations et caractéristiques démographiques, géographiques et urbaines</i>. Stage co-encadré par Mathieu Durand-Daubin (EDF) et Julie Vallée.
	</p>
		
	<p>	
	<b>Élisa Villard</b> (École Centrale Nantes - filière Villes numériques). 2019. <i>Extension du Mobiliscope aux villes canadiennes</i>. Stage encadré par Julie Vallée et effectué à Montréal en partenariat avec le laboratoire SPHERELab.
	</p>

	<p>
	<b>Simon Oertlin</b> (Master 2 Geoprisme - Université Paris 1 Panthéon-Sorbonne). 2019. <i>Les rythmes quotidiens de la ségrégation sociale dans 22 grandes villes françaises</i>. Stage encadré par Julie Vallée et effectué à Paris au laboratoire Géographie-cités.
	</p>

	<p>
	<b>Maximilien Riot</b> (Master 2 GeoPRAD - Université de Nice). 2019. <i>L’offre de soins heure par heure dans la région Occitanie</i>. Stage encadré par Julie Vallée et effectué à Toulouse en partenariat avec l'Institut Fédératif d'Études et de Recherches Interdisciplinaires Santé Société (IFERISS) et l’Agence Régionale de la Santé (ARS) d’Occitanie. 
	</p>

	</section>

	<section>
    <h3>Autres contributeurs et contributrices</h3>

    <p>
	<ul>
		<li>Renaud Bessières (<a target="_blank" href="https://qodop.com/" title="qodop">qodop</a>) : conseil en développement web</li> 
		<li>Mathieu Leclaire (Géographie-cités/ISC-PIF) : aide pour la réorganisation informatique (hébergement sur HumaNum, stockage des données sur MinIO, dépôts gitlab etc.)</li>
		<li>Barbara Christian et Julie Pelata (Cerema) : aide sur les données des Enquêtes EMC²</li>
		<li>Cédric Poirier : amélioration de l'interface</li>
		<li>Saber Marrouchi et Yonathan Parmentier (Géographie-cités) : soutien informatique</li>
		<li>Florent Demoraes (Université de Rennes/ANR Modural) : retours critiques sur l'exploitation des enquêtes latino-américaines et aide sur le fond de carte Bogotá</li>
		<li>Marc Sainte-Croix et Blandine Maison (Agence d’urbanisme, région nîmoise et alésienne) : participation à l'intégration de Nîmes</li>
		<li>Sébastien Haule (Géographie-cités) : aide en infographie et communication</li>
		<li>Robin Cura et Mattia Bunel (Géographie-cités) : conseils en géovisualisation</li>
	</ul>
	</p>
	</section>

	<section>
     <h3>Rejoignez-nous !</h3>
    <p>
		Contactez-nous si vous souhaitez rejoindre l'équipe (dans le cadre de vos activités ou de vos études).
    </p>
	</section>

	<p>
	<button class="style-button mb50"><a href="/fr/info/about/partners">Découvrez nos partenaires</a></button>
	</p>


</div>
