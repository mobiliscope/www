<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Publications & co</h2>
	</section>

	<section>
	<h3>A la Une !</h3>


		<p>
		<i>[Présentation]</i> Le Mobiliscope, un outil libre pour quantifier et qualifier la population présente dans les territoires au cours des 24 heures de la journée : Regard critique sur les opportunités offertes par le Mobiliscope en comparaison des données téléphoniques et sur les contraintes d’accès et de diffusion des données de présence. <i>Commission Territoires du Conseil national de l’information statistique (Cnis)</i>. Session <i><a href = "https://www.cnis.fr/evenements/territoires-2024-1re-reunion/" target="_blank">Estimer la population présente sur le territoire</a></i>, Paris, 6 juin 2024. <a href = "https://hal.science/hal-04603446" target="_blank">hal-04603446</a>. <i>Accès au <a href = "/pdf/2024_Mobiliscope_Cnis.pdf" target="_blank">diaporama</a></i>
		</p>

		<p>
		<i>[Article]</i> Vallée J, Lenormand M (2024). <a href = "https://dx.doi.org/10.1177/23998083231174025" target="_blank">Intersectional approach of everyday geography</a>, <i>Environment and Planning B: Urban Analytics and City Science</i>, 51(2), pp.346-365. Preprint <a href = "https://arxiv.org/abs/2106.15492v2" target="_blank">arXiv</a> & <a href = "https://hal.science/hal-04109508" target="_blank">HAL</a>
		</p>


		<p>
		<i>[Présentation]</i> Mobiliscope : cartographier, mesurer et analyser les déplacements quotidiens et leurs impacts. <i>Webinaire "La science à distance" organisé par Le Club CNRS Entreprises</i>. 30 janvier 2024. <i><a href = "https://www.dailymotion.com/video/k51t1yKu0NB8plzY5HW" target="_blank">Replay</a></i>
		</p>


		<p class="ss-style-dots" ></p>

	<h3>Revue de presse</h3>

				<h4>Communiqués de presse</h4>

				<p>

				<ul>

				<li>Version v4.1&nbsp;: communiqué en <a href = "/pdf/2022_Mobiliscope_v4.1_FR.pdf" target="_blank">français</a>, <a href = "/pdf/2022_Mobiliscope_v4.1_EN.pdf" target="_blank">anglais</a> et <a href = "/pdf/2022_Mobiliscope_v4.1_ES.pdf" target="_blank">espagnol</a>, avril 2022.</li>

				<li>Version v4.0&nbsp;: communiqué en <a href = "/pdf/2021_CP_CNRS_Mobiliscope_web.pdf" target="_blank">français</a> et <a href = "/pdf/2021_PR_CNRS_Mobiliscope_web.pdf" target="_blank">anglais</a>, avril 2021.</li>

				</ul>

				</p>
			</section>

			<section>
				<h4>Radio</h4>

				<p>

				<ul>

				<li>RFI - <i>Nouvelles Technologies</i>. <a href = "https://www.rfi.fr/fr/podcasts/nouvelles-technologies/20210410-le-mobiliscope-outil-de-g%C3%A9ovisualisation-de-la-mixit%C3%A9-sociale-dans-les-villes" target="_blank">Le Mobiliscope, outil de géovisualisation de la mixité sociale dans les villes</a>, par Dominique Desaunay, 10 avril 2021.</li>

				<li>France Culture - <i>La méthode scientifique</i>. <a href = "https://www.radiofrance.fr/franceculture/podcasts/le-journal-des-sciences/mobiliscope-cartographier-heure-par-heure-la-composition-sociale-d-un-quartier-4003725" target="_blank">Le journal des sciences&nbsp;: le Mobiliscope</a>, par Natacha Triou, 8 avril 2021.</li>

				<li>France Culture - <i>Nos Géographies</i>. <a href = "https://www.franceculture.fr/emissions/nos-geographies/autour-de-lhabitat-des-lieux-et-des-liens" target="_blank">Autour de l’habitat, des lieux et des liens</a>, animée par Dominique Rousset. 10 septembre 2020. </li>

				</ul>

				</p>
			</section>


			<section>
				<h4>Journaux et magazines</h4>

				<p>
				<ul>

				<li><i>La gazette des communes</i>. <a href = "https://www.lagazettedescommunes.com/738947/le-mobiliscope-un-outil-pour-suivre-le-rythme-du-territoire/" target="_blank"> Le Mobiliscope, un outil pour suivre le rythme des territoires</a>, par Alexandre Léchenet et Laura Fernandez Rodriguez, avril 2021.</li>

				<li>Magazine <i>Sciences et vie</i>. <a href = "/pdf/2021_Scienceetvie.pdf" target="_blank"> Le Mobiliscope, la plateforme qui cartographie la couleur sociale des quartiers au fil de la journée</a>, par Pierre-Yves Lerayer, avril 2021.</li>

				<li>Journal <i>Socialter</i>. <a href = "/pdf/2020_socialter.pdf" target="_blank">Le temps, clé des villes</a>, par Christelle Granja, Numéro 42, octobre-novembre 2020.</li>

				<li>Journal <i>Le Monde</i>. <a href = "/pdf/2020_lemonde.pdf" target="_blank">Élections municipales&nbsp;: les travers de la ‘démocratie du sommeil’</a>, par Lætitia Van Eeckhout, 16 juin 2020. </li>

				<li>Revue <i>Les Autres Possibles</i>. <a href = "https://lesautrespossibles.fr/numero_du_mois/23-ville/" target="_blank">Comment les femmes occupent les espaces de la ville ?</a> par Alice Mounissamy, octobre 2019. </li>

				<li>Revue <i>DécryptaGéo</i>. <a href = "http://decryptageo.fr/le-mobiliscope-setend-sur-22-villes-francaises/" target="_blank">Le Mobiliscope s’étend sur 22 villes françaises</a>, par Françoise De Blomac, avril 2019.</li>

				<li>Journal <i>Les Echos</i>. <a href = "https://www.lesechos.fr/pme-regions/innovateurs/un-outil-pour-mieux-cibler-laction-sociale-dans-les-villes-1007729" target="_blank"> Un outil pour mieux cibler l'action sociale dans les villes</a>, par Jacques Henno, 9 avril 2019.</li>

				</ul>
				</p>

		</section>

		<section>
			<h4>Varia</h4>

			<p>
			<ul>

			<li>Entretien <a href = "https://www.ined.fr/fr/tout-savoir-population/memos-demo/paroles-chercheurs/julie-vallee-et-guillaume-le-roux/" target="_blank">avec l'équipe du Mobiliscope : Julie Vallée, Aurélie Douet et Guillaume Le Roux</a>. Ined, avril 2022.</li>

			<li>Rapport de l’<i>Observatoire de la Mixité Sociale (OMIS)</i>. <a href = "/pdf/2021_omis.pdf" target="_blank">La région parisienne est plus mixte socialement le jour que la nuit, entretien avec la géographe Julie Vallée</a> par Jérôme Porier, juin 2021.</li>

			<li>La <i>Lettre de l’Institut des Sciences Humaines et Sociales du CNRS</i>. <a href = "http://www.inshs.cnrs.fr/sites/institut_inshs/files/download-file/lettre_infoinshs51hd-min.pdf#page=25"   target="_blank">Les temporalités quotidiennes de la ville</a>, janvier 2018.</li>

			</ul>

			</p>
		</section>


	<section>
	<h3>Publications scientifiques</h3>
	</section>

	<section>
		<h4>Articles</h4>

		<p>
			<ul>

			<li>Vallée J, Lenormand M (2024). <a href = "https://dx.doi.org/10.1177/23998083231174025" target="_blank">Intersectional approach of everyday geography</a>, <i>Environment and Planning B: Urban Analytics and City Science</i>, 51(2), pp.346-365. Preprint <a href = "https://arxiv.org/abs/2106.15492v2" target="_blank">arXiv</a> & <a href = "https://hal.science/hal-04109508" target="_blank">HAL</a></li>

			<li>Vallée J, Douet A, Le Roux G, Commenges H (2022). <a href = "https://theconversation.com/le-mobiliscope-un-outil-libre-sur-les-rythmes-quotidiens-des-territoires-192204" target="_blank"> Le Mobiliscope&nbsp;: un outil libre sur les rythmes quotidiens des territoires</a>, <i>The Conversation</i>.</li>

			<li>Vallée J (2022). <a href = "https://shs.hal.science/halshs-03801117/document" target="_blank">Une lecture critique des politiques territorialement sélectives et de leur géographie</a>. In <i>Les inégalités dans l’espace géographique</i> (sous la direction de C. Cottineau et J. Vallée), pp.221-249, ISTE éditions, Encyclopédie des Sciences, ISBN 9781789480887.</li>

			<li>Douet A, Vallée J (2021). <a href = "/pdf/2021_ONPV.pdf"   target="_blank">L'(im)mobilité quotidienne des femmes et des hommes</a>, Fiche de synthèse pour le <i><a href = "http://www.onpv.fr/uploads/media_items/onpv-rapport-2020.original.pdf/" target="_blank">Rapport 2020 de l'Observatoire National de la Politique de la ville</a>,</i> pp. 150-151.</li>

			<li>Vallée J (2020). <a href = "https://asmn.univ-nantes.fr/index.php?id=465"   target="_blank">Les femmes et les hommes dans la ville&nbsp;: la parité au quotidien</a>, <i>Atlas social de la métropole nantaise,</i> sous la direction de F. Madoré et J. Rivière.</li>

			<li>Lecomte C, Vallée J, Le Roux G, Commenges H (2018). <a href = "http://mappemonde.mgm.fr/123geov3/"   target="_blank">Le Mobiliscope, un outil de géovisualisation des rythmes quotidiens des métropoles</a>, <i>Mappemonde</i> n°123.</li>

			<li>Le Roux G, Vallée J, Commenges H (2017). <a href = "/pdf/2017_SegregationAroundTheClock.pdf" target="_blank">Social segregation around the clock in the Paris region (France),</a> <i> Journal of Transport Geography</i>, Vol 59, pp. 134-145.</li>

			<li>Vallée J (2017). <a href = "/pdf/2017_TheDaycourseOfPlace.pdf" target="_blank" > The daycourse of place,</a><i> Social Science & Medicine</i>, Dec. 2017, Vol 194, pp 177-181.</li>

			<li>Vallée J (2017). <a href = "/pdf/2017_TargetAreas.pdf" target="_blank">Challenges in targeting areas for public action. Target areas at the right place and at the right time,</a> <i> Journal of Epidemiology and Community Health</i>, Oct. 2017, Vol 71 n° 10, pp 945-946.</li>

			</ul>
		</p>

	</section>

	<section>
		<h4>Conférences et séminaires internationaux - en anglais</h4>

		<p>
			<ul>

			<li>International roundtable on Computational social science. <i>Institute for Analytical Sociology</i> (IAS), Norrköping (Sweden), May 2024.</li>

			<li>European Colloquium on Theoretical and Quantitative Geography - ECTQG, Braga (Portugal), September 2023.

			<li>TU Delft / Faculty of Architecture and The Built Environment, Delft (Netherlands), March 2023.</li>

			<li><i>Barcelona Time Use Initiative for a Healthy Society</i>, Barcelona (Spain), October 2022.</li>

			<li>Conference on Complex Systems (CCS) - Workshop UrbanSys, Lyon (France), October 2021. <i><a href ="https://hal.inrae.fr/hal-03406971/document" target="_blank">Slides</a></i></li>

			</ul>

		</p>

		</section>

		<section>
		<h4>Conférences et séminaires - en français</h4>

		<p>
		<ul>

		<li>Webinaire <i>Cartes blanches</i> de l'Action de Recherche (carto)graphies et (géo)visualisations de données du GDR MAGIS. 17 avril 2023. <i><a href ="https://github.com/magisAR9/webinaires/blob/main/cartogeoviz5-douet.md" target="_blank">Résumé et vidéo</a></i></li>

		<li>Séminaire du projet <i>MAMA</i> (du Monde d'Avant au Monde d'Après). Aubervilliers, 6 février 2023.</li>

		<li>Séminaire « Mobilités et comportements individuels » (Chaire « Mobilité territoriale » ENPC-IDFM / Lab Recherche Environnement Vinci-Paris Tech). Paris, 6 juillet 2022.</li>

		<li>Journée d'étude <i>Accès et utilisation des données en libre accès en géographie de la population, de la santé et de la mobilité</i>. Paris, 2 décembre 2021. <i><a href ="https://geopdata.sciencesconf.org/369005/document" target="_blank">Résumé</a></i></li>

		<li>Colloque <i>Cartomob</i>. 14-16 juin 2021.<i><a href = "https://prismes.univ-toulouse.fr/player.php?code=k11S4Sy6&width=100%&height=100%" target="_blank">Vidéo</a></i></li>

		<li>Colloque <i>Population, Temps, Territoires</i>, CIST. 18-20 novembre 2020. <i><a href = "https://api.nakala.fr/data/10.34847/nkl.aaeciscz/cd0fc7b6975eab8d2a98cec2c12b9aeb80968034" target="_blank">Vidéo</a></i></li>

		<li>Rencontres <i>ThéoQuant 2019</i>, Besançon, 6-8 février 2019.</li>

		<li>Colloque <i>Santé&nbsp;: équité ou égalité ? Définir, mesurer, agir</i>, IFERISS. Toulouse, 23-25 mai 2018.</li>

		<li>Colloque <i>Représenter les territoires</i>, CIST. Rouen, 22-23 mars 2018.</li>

		<li>Rencontres de Statistique Appliquée, <i>Dataviz&nbsp;: idée, méthode et perception</i></span>, Ined. Paris, 5 décembre 2017.</li>

		<li>Conférence <i>SAGEO - Spatial Analysis and Geomatics</i>. Rouen, 6-9 novembre 2017. <i><a href = "https://webtv.univ-rouen.fr/videos/07-11-2017-142856-partie-12/" target="_blank">Vidéo</a></i></li> 

		</ul>
		</p>
	</section>


	<section>
	<h3>Autres présentations</h2>
	</section>

	<section>
	<h4>À destination des acteurs de l'aménagement et des collectivités locales</h4>

	<p>
		<ul>

		<li><i>Journées d'échanges sur la mobilité urbaine - Cerema </i>, Nanterre, 17-18 octobre 2022. <i><a href = "http://www.cerema.fr/system/files/documents/2022/12/2_3_vallee_douet_cnrs.pdf" target="_blank"> Diapositives</a></i></li>

		<li><i> Regards croisés sur les pôles qui font centralité, la mobilité des habitants et l’accessibilité aux services</i>, Regards sur les territoires - ANCT, Paris, 20 octobre 2021. <i><a href = "https://media-anct.manager.tv/?publication=ca5c2d5bd770b6b3fc41b7b930b0cd8efb708b66&type=player&chaine=anct-tv" target="_blank">Vidéo</a></i></li>

		<li>Rencontres régionales <i> Éduquer aux mobilités</i>, Graine Occitanie, Narbonne, 21 octobre 2021.</li>

		<li><i>Salon Innovativ SHS</i>, CNRS, Lille, 15-16 mai 2019.</li>

		<li><i>Journées d'échanges sur la mobilité urbaine - Cerema </i>, Paris la Défense, 13-14 mai 2019. <i><a href = "https://www.cerema.fr/system/files/documents/2019/08/3_vallee.pdf" target="_blank">Diapositives</a></i></li>

		<li><i>Séminaire du Réseau national des acteurs des démarches temporelles</i>, Tempo Territorial, Paris, 19 mars 2019.</li>

		<li><i>Séminaire de l’Observation Urbaine (SOU)</i>. La ville connectée&nbsp;: observer les territoires à l'ère du numérique, CGET, Paris, 23 novembre 2017.</li>

		</ul>

	</p>
	</section>


	<section>
	<h4>À destination du grand public</h4>

	<p>
		<ul>

		<li><i>Meetup Dataviz Toulouse</i>, 19 septembre 2019. <i><a href = https://vimeo.com/361475032 target="_blank">Vidéo</a></i></li>

		<li><i>Café Géo Albi</i>, 14 novembre 2018.</li>

		<li>Exposition <i>Trajectoires, l'exposition qui interroge nos mobilités</i>. Exposition itinérante en Île-de-France, 2018-2020.</li>

		<li><i>Fête de la science 2018 </i>. La mobilité quotidienne favorise-t-elle le mélange des populations en Ile-de-France ? Nanterre, 6 octobre 2018.</li>

		<li><i>Nuit de la Géographie 2018</i>. Quizz autour du Mobiliscope, Paris (La Bellevilloise), 6 avril 2018.</li>

		<li><i>Festival International de Géographie (FIG) de St Dié des Vosges 2017 </i> - Session géovisualisation et cartographies dynamiques, 29 sept.-1er oct. 2017.</li>

		</ul>

	</p>
	</section>

	<br>
	<br>
	<br>

</div>

