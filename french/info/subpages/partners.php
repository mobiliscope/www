<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Partenaires</h2>
	</section>

	<section>
	<p>
		 
		Le Mobiliscope est développé au sein de deux laboratoires publics de recherche du <b>CNRS</b> : les laboratoires <b>Géographie-cités</b> à Paris (depuis 2017) et <b>LISST</b> à Toulouse (depuis 2024).

		<a href = "https://geographie-cites.cnrs.fr/" target="_blank" >
		<figure class="inline">
		  <img src="/dist/assets/geocites.png" alt="geocites" width="280"/>
		</figure>
		</a>

		<a href = "https://www.cnrs.fr/fr" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/cnrs.png" alt="cnrs" width="90"/>
		</figure>
		</a>

		<a href = "https://lisst.univ-tlse2.fr/" target="_blank" >
		<figure class="inline">
		  <img src="/dist/assets/logo-lisst.png" alt="lisst" width="215"/>
		</figure>
		</a>


		</br>
	</p>

	<p>
		Les principaux partenaires du Mobiliscope sont l'<b>Ined</b> et l’<b>ANCT</b>. 

		<a href = "https://www.ined.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/ined.png" alt="ined" width="170"/>
		</figure>
		</a>

		<a href = "https://agence-cohesion-territoires.gouv.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/anct.png" alt="anct" width="265"/>
		</figure>
		</a>



	</p>

	<p>
		Le Mobiliscope bénéficie du soutien d'organismes publics en termes scientifiques, financiers, informatiques ou d'accès aux données.


		<a href = "http://www.progedo-adisp.fr/" target="_blank">
		<figure class="inline">
		 <img src="/dist/assets/progedo.png" alt="progedo" width="130"/>
		</figure>
		</a>

		<a href = "https://www.cerema.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/cerema.png" alt="cerema" width="255"/>
		</figure>
		</a>

		<a href = "https://www.huma-num.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/huma-num.png" alt="huma-num" width="95"/>
		</figure>
		</a>

		<a href = "https://www.transports.gouv.qc.ca/" target="_blank">
		<figure class="inline">
			<img src="/dist/assets/mintransportsqc.png" alt="mintransportsqc" width="170"/>
		</figure>
		</a>

		<a href = "http://labex-dynamite.com/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/labex.png" alt="labex" width="80"/>
		</figure>
		</a>

		<a href = "https://www.spherelab.org/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/labsphere.png" alt="spherelab" width="145"/>
		</figure>
		</a>

	</p>
	</section>



	<p>
	<button class="style-button mb50"><a href="/fr/info/about/publications">Découvrez les articles et communications en lien avec l'outil</a></button>
	</p>

</div>
