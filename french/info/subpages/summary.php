<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
	<h2>En quelques mots...</h2>
	</section>

	</br>
	<section>
		<p>
		Le Mobiliscope est un outil de géovisualisation pour explorer la population présente et la mixité sociale dans les villes au cours des 24 heures de la journée. Il permet d'explorer librement <b>58 villes et leur région</b> (49 en France, 6 au Québec, Canada et 3 en Amérique latine). En France ce sont ainsi <b>10 000 communes</b> qui sont couvertes par l'outil (60% de la population).</br>
		</p>

		<p>
		Derrière cet outil, il y a une <a href="/fr/info/about/team">équipe</a> de chercheur·e·s et d'ingénieur·e·s ainsi que plusieurs <a href="/fr/info/about/partners">partenaires</a> institutionnels.</br>
		</p>

		<p>
		Divers <a href="/fr/info/about/publications">textes</a> scientifiques ou grand public ont aussi été publiés sur les rythmes quotidiens de villes et de leur mixité sociale.
		</p>

		<p>
		Pour citer la <b>version actuelle</b> du Mobiliscope&nbsp;:</b>
			<ul>Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C, Villard E (2024). Mobiliscope, un outil libre pour explorer les villes et leur mixité sociale au cours des 24 heures de la journée [v4.3]. www.mobiliscope.cnrs.fr. doi: 10.5281/zenodo.11111161
			</ul>
		</p>


	</section>


		<section>
		<h3>Méthodes</h3>

			<p>
			Afin de localiser la population présente dans les villes et leur péripherie à chaque heure de la journée, des <a href="/fr/info/methods/data">données sur les déplacements quotidiens</a> issues de grandes enquêtes sont utilisées</a>. Les populations présentes dans les villes au cours des 24 heures d'une journée type de semaine (lundi-vendredi) sont ensuite caractérisées en fonction d'<a href="/fr/info/methods/indicators">indicateurs</a> démographiques (sexe et âge) et sociaux (niveau d'éducation, catégorie socioprofessionnelle, et parfois revenus) mais aussi en fonction des secteurs de résidence, du type d'activité réalisée et du mode de transport utilisé.
			</p>

			<p>
			Pour chacune des villes intégrées au Mobiliscope, une <a href="/fr/info/methods/geovizualisation">géovisualisation</a> interactive (composée d'une carte et de deux graphiques qui évoluent au fil des heures) permet d'explorer les rythmes quotidiens des territoires.
			</p>

			<p>
			Le Mobiliscope est en constante <a href="/fr/info/methods/evolution">évolution</a>. De nouvelles villes et fonctionnalités sont régulièrement ajoutées.
			</p>

			 <button class="style-button mb50"><a href="/fr/geoviz/rennes/">Découvrir la géovisualisation pour <b>Rennes</b> et sa région</a></button>
			</section>

		<section>
		<h3>Science ouverte</h3>

			<p>
			Le développement du Mobiliscope s’inscrit dans les <b>principes du FAIR</b>&nbsp;: nous faisons notre maximum pour que le code et les données affichées dans l'outil soient «&nbsp;<b>F</b>aciles à trouver, <b>A</b>ccessibles, <b>I</b>nteropérables et <b>R</b>éutilisables&nbsp;». 
			</p>

			<p>
			Les <a href="/fr/info/open/license">codes</a> pour traiter les données initiales et pour créer l'interface de géovisualisation sont librement accessibles, ainsi qu'un <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">guide Administrateur·rice</a> qui documente le fonctionnement de l'outil. Les données affichées dans l'outil sont en <a href="/fr/info/open/opendata">open-data</a>. Des <a href="/fr/info/open/reuses">articles reproductibles</a> en sont issus. 
			</p>
			</br>
			</br>
		</section>

		<section>
		<h3>Des applications variées</h3>

			<p>
			Une grande variété de cas d'usages existe et différents publics peuvent se saisir de l'outil. Il peut ainsi être utilisé comme un <a href="/fr/info/multitask-tool/planning-tool">appui pour l'aménagement du territoire</a>, comme un <a href="/fr/info/multitask-tool/scientific-tool">outil pour la recherche scientifique</a>, ou comme une <a href="/fr/info/multitask-tool/pedagogical-tool">ressource pour l'enseignement</a>. 

			<p>
			Pour celles et ceux qui s'intéressent aux rythmes quotidiens dans les territoires des programmes <i>Actions Coeur de Ville (ACV)</i> et <i>Petites Villes de Demain (PVD)</i> et dans les <i>Quartiers Prioritaires en Politique de la Ville (QPV)</i>, un nouvel <b>outil</b> permet de renvoyer directement à la géovisualisation des territoires français concernés par ces zonages. 
			</p>

			 <button class="style-button mb50"><a href="/fr/info/toolbox/zoning">Découvrir l'outil <b>Filtrer selon les zonages institutionnels <small>(France)</small></b></a></button>
		</section>

		<section>

			<center>
				<figure class="inline" >
					<img src="/dist/assets/prix.jpg" alt="prix" width="350"/>
					<figcaption>Trophée du prix <b><a href="https://www.enseignementsup-recherche.gouv.fr/fr/prix-science-ouverte-des-donnees-de-la-recherche-86179" target="_blank">Science ouverte des données de la recherche</a></b></br><i>Attribué au au projet Mobiliscope en 2022 par le ministère français de l’Enseignement Supérieur et de la Recherche</i>
					</figcaption>
				</figure>
			</center>

				
		</section>

	  </br>
	  </br>	
	  </br>
	  </br>	


</div>
