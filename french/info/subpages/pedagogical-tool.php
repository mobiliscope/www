<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Une ressource pour l'enseignement</h2>
	</br>

	<p>
    Le Mobiliscope constitue une ressource pédagogique pour étudier la mobilité quotidienne dans les villes et les divisions soico-spatiales associées. 
	</p>
	</section>


	<section>
	<h3>À l'école élémentaire</h3>
		<p>
		Le Mobiliscope peut constituer un support ludique pour faire explorer aux élèves les différents types de déplacements (pendulaires, ponctuels et réguliers) et les fonctions urbaines&nbsp;: deux thématiques présentes dans le <b>programme de géographie de CM2</b>.
		</p>

		<p>
		Le Mobiliscope a été présenté à plusieurs reprises à des classes de CM2 à Paris et à Toulouse. A partir de tablettes ou d'ordinateurs, le Mobiliscope a permis aux élèves de répondre à un petit quizz centré sur leur quartier.
		</p>

		<h4>
		<figure class="inline">
		<img src="/dist/assets/ecole2.jpg" alt="ecole2" width="220"/>
		<img src="/dist/assets/ecole3.jpg" alt="ecole1" width="220"/>
		<img src="/dist/assets/ecole4.jpg" alt="ecole2" width="220"/>
		</figure>
		</h4>

		<p>
			Extrait du quizz proposé aux élèves de CM2 d'une école située dans le 20ème arrondissement à Paris&nbsp;:
			<ul>
				<li>À quelle heure y-a-t-il le plus de personnes présentes dans le 20ème ? </li>
				<li>À quelle heure y-a-t-il le moins de personnes présentes dans le 20ème ?</li>
				<li>Que font (quelle activité) 29% de la population présente dans le 20ème à 3h de l’après-midi ?</li>
				<li>Quel est le secteur limitrophe du 20ème qui est le plus peuplé à 11h du matin ?</li>
				<li>Quel est le secteur limitrophe du 20ème avec le plus d’ouvriers présents à 4h de l’après-midi ?</li>
				<li>Quel est le groupe d’âge avec le plus de personnes présents dans le 20ème à 7h du soir ?</li>
				<li>Quel est le nombre de personnes présentes dans le 20ème avec des revenus très élevés à 11h du matin ?</li>
				<li>À quelle heure la proportion de femmes est-elle la plus élevée dans le 20ème ?</li>
			</ul>
    		</p>

		</section>


	<section>
	<h3>Au lycée</h3>

		<p>
		Le Mobiliscope peut être utilisé <b>en classe de première</b> pour aborder les mobilités, la ségrégation socio-spatiale, le polycentrisme ou encore les inégalités.
		</p>

		<p>
		Une fiche d'activité a ainsi été rédigée par <a href = "https://histoire.ac-versailles.fr/spip.php?article1718" target="_blank">des professeurs d'histoire-géographie de lycée de l'académie de Versailles </a> pour l'étude des mobilités en Île-de-France <b>en classe de première&nbsp;:</b>
		</br>
		<i>
			Avec quelques explications et démonstrations, une forme de mise en activité consiste à proposer une série d’hypothèses qu’il faudra valider ou invalider, en justifiant sa réponse avec la couche d’information géographique et le classement des données pertinentes, puis avec un rapide commentaire explicitant les conclusions tirées de la visualisation de ces données. Les thèmes de la mobilité, de la ségrégation socio-spatiale, du polycentrisme ou encore des inégalités peuvent ainsi aisément, et concrètement, être abordés.».
			<ul>
				<li>Hypothèse n°1&nbsp;: les mobilités en IDF sont avant tout liées aux déplacements professionnels. </li>
				<li>Hypothèse n°2&nbsp;: la région IDF est « polycentrique »&nbsp;: plusieurs grands pôles d'activités cohabitent.</li>
				<li>Hypothèse n°3&nbsp;: la majorité des habitants de Grande Couronne se déplacent vers la petite couronne et Paris pour travailler.</li>
				<li>Hypothèse n°4&nbsp;: la majorité des habitants de Paris se déplacent en banlieue pour travailler.</li>
				<li>Hypothèse n°5&nbsp;: Paris et sa proche banlieue concentrent les activités de commandement.</li>
				<li>Hypothèse n°6&nbsp;: les populations défavorisées sont moins mobiles que les populations appartenant aux catégories sociales favorisées.</li>
				<li>Hypothèse n°7&nbsp;: les emplois en Seine-Saint-Denis n'ont pas les mêmes caractéristiques que les emplois dans les Hauts-de-Seine.</li>
			</ul>
		</i>
    	</p>

	</section>


	<section>
	<h3>A l'université</h3>

		<p>
			Le Mobiliscope est utilisé dans le cadre de cours à l'université. En voici quelques exemples :
			<ul>
				<li> A l'université de <b>Rouen</b>, le Mobiliscope a été présenté par Paul Gourdon aux étudiants de licence de géographie dans le cours «&nbsp;Géographie et numérique&nbsp;» afin d'illustrer les nouvelles données disponibles pour appréhender les thématiques de division sociale de l’espace et de mobilité quotidienne (<i>spatial mismatch</i> etc.). Il a ensuite servi de support à un travail des étudiants en petits groupes autour d'une question de recherche de leur choix. 
				</li>

				<li> A l'université de <b>Montpellier</b>, le Mobiliscope est intégré au module de formation en ligne sur les <a href = "https://www.univ-montp3.fr/fr/formation-lp/nexus/les-humanit%C3%A9s-num%C3%A9riques" target="_blank">Humanités Numériques</a>, et plus spécifiquement dans la brique <i>Espaces Digitaux</i> (chapitre <i>Représenter l'espace</i>, Alvéole <i>Esthétique et représentation des cartes</i> - Adrien Lammoglia & Marion Le Texier). Il est notamment utilisé pour illustrer des questions de sémiologie graphique. Un petit quizz permet aussi aux étudiants de tester leur compréhension de l'outil (Cf. <a href = "/pdf/UnivMontpellier.pdf" target="_blank">extrait du cours</a>).
				</li>
 
			</ul>

		</p>
		
	</section>


	<section>
    <h3>Partagez vos experiences pédagogiques</h3>
	
	<p>	
		N'hésitez pas à nous envoyer un message pour partager vos expériences pédagogiques avec le Mobiliscope. <br>
		Nous sommes très intéressés par vos retours. Nous pouvons aussi vous accompagner dans la mise en place d'actions spécifiques autour de votre quartier, votre ville... 
	</p>
	
    </section>

	 <br>
	 <br>
	 <br>

</div>
