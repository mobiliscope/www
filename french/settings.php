<?php
include($_SERVER['DOCUMENT_ROOT'].'/data-settings/city.php');
//phpinfo();
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include($_SERVER['DOCUMENT_ROOT'].'/minio-settings.php');

$language = "fr";
$page = '';
$subpage = '';
$pageMeta = '';
$sectionName = '';
$pagePath = '';
$pageCrumbs = "<a href='/".$language."'>Accueil</a>";

$curCity = (!empty($_GET['city']) && in_array($_GET['city'], array_keys($city)))? $_GET['city']: 'albi';


if( $bucket == 'mobiliscope' ) {
    $path =  $path . '/' . $curCity;
} else
    $path =  $hash;

if(!empty($_GET['hash']) ){
    $filters = getObject($s3Client, $bucket, $path,'filters.json');
    $filters_array = json_decode($filters, true);
    $curCity = $filters_array['request']['study'];
}

 // List of pages and their titles
    $pages = [
        'about'=>'A propos',
        'methods'=>'Méthodes',
        'open'=>'Science ouverte',
        'multitask-tool'=>'Champs d\'applications',
        'toolbox'=>'Boite à outils',
        // 'carbon'=>'Carbonscope',

    ];

    $subpages = [
        'about'=>[
            'summary'=>'En quelques mots...',
            'team'=>'Équipe',
            'partners'=>'Partenaires',
			'publications'=>'Publications & co',

        ],
        'methods'=>[
            'data'=>'Transformer les déplacements',
            'indicators'=>'Caractériser la population présente',
            'geovizualisation'=>'Géovisualisation (carto)graphique',
            'evolution'=>'Evolutions et actualisations',

        ],
        'open'=>[
            'license'=>'Code ouvert et documenté', 
            'opendata'=>'Données libres et réutilisables',  
            'reuses'=>'Articles reproductibles',
            'cite'=>'Comment citer le Mobiliscope?',

       ],
        'multitask-tool'=>[
            'scientific-tool'=>'Recherche',
            'planning-tool'=>'Aménagement',
            'pedagogical-tool'=>'Enseignement',
            
        ],
        'toolbox'=>[          
            'zoning'=>'Filtrer selon les zonages institutionnels <small>(France)</small>',
    //         'educ'=>'Découvrir les supports ludo pédagogiques',
     //        'mobiliquest'=>'Faire des requêtes avec le <strong>Mobiliquest</strong>',
        
     //    ],
     //     'carbon'=>[            
     //        'x-pourquoi'=>'Pourquoi ?',
     //        'x-comment'=>'Comment ?',
     //        'x-go'=>'GO',
        ]
    ];



    // this description will be used by Google as the text under the title of the page
    // TITLE it should be max 70 char
    // META it should be max 230 char

      $pageSEO = [
      	//'about'=>['title'=>'Le Mobiliscope pour explorer la population des villes le jour et la nuit !','meta'=>'Les villes changent... Selon les heures et la mobilité quotidienne des populations, ce sont les quartiers de villes qui changent au fil des heures, mais aussi la mixité sociale et la ségrégation urbaine.'],
        'summary'=>['title'=>'En quelques mots...','meta'=>'La mobilité quotidienne des populations occasionne des évolutions dans la compostion sociale des quartiers et dans la ségrégation des villes au cours des 24 heures de la journée. '],
		'team'=>['title'=>'L\'équipe du Mobiliscope','meta'=>'Un outil de géovisualisation sous la responsabilité de Julie Vallée et développé par Aurélie Douet et Constance Lecomte. Avec la participation de Guillaume Le Roux, Hadrien Commenges et Elisa Villard'],
        'partners'=>['title'=>'Les partenaires du Mobiliscope','meta'=>'Développé dans le laboratoire Géographie-cités, le Mobiliscope est soutenu par le CNRS, l\'INED, l\'ANCT, le Cerema, le Labex DynamiTe...'],
        'publications'=>['title'=>'Publications autour du Mobiliscope','meta'=>'En lien avec la géographie sociale, la géographie urbaine, la ‘time-geography’, la géomatique, la géovisualisation et la géographie de la santé.'],
        
        //'methods'=>['title'=>'Les méthodes du Mobiliscope','meta'=>'Une interface interactive pour explorer la coprésence des riches et des pauvres, des femmes et des hommes, des actifs et des retraités etc. au cours des 24 heures de la journée'],
        'data'=>['title'=>'Transformation des déplacements','meta'=>'Avec les enquêtes publiques sur les deplcements quotidiens, le Mobiliscope enrichit les études urbaines traditionnellement basées sur les recensements de populations et les navettes domicile-travail.'],
        'indicators'=>['title'=>'Caractérisation de la population présente','meta'=>'Des cartes heure par heure des villes selon le profil démographique (sexe et âge) et social (niveau d\'éducation, catégorie socioprofessionnelle CSP) des individus, leur mode de transport (marche, vélo, transport en commun, voiture) etc.'],
        'geovizualisation'=>['title'=>'Géovisualisation (carto)graphique','meta'=>'L’interface a été développée avec la librairie d3.js. Elle propose des cartes animées (choroplèthes, en cercles proportionnels, en oursins) et des indices de ségrégation (Duncan) et d’autocorrélation spatiale (Moran) calculés heure par heure.'],
        'evolution'=>['title'=>'Evolutions et actualisations','meta'=>'Un outil en constante évolution depuis sa première mise en ligne en 2017. Des nouvelles villes et fonctionnalités sont régulièrement ajoutées.'],

        
        //'news'=>['title'=>'Valorisation du Mobiliscope','meta'=>'Promouvoir une vision temporelle des villes auprès du grand public, des acteurs de l’aménagement, de la communauté scientifique et de la communauté pédagogique.'],
        //'events'=>['title'=>'Les événements du Mobiliscope','meta'=>'Fête de la science; Nuit de la Géographie ; Festival International de Géographie (FIG) ; Salon Innovativ SHS du CNRS ; ThéoQuant ; SAGEO ; Les mardis de Tempo territorial'],
        

        //'open'=>
        'license'=>['title' => 'Mobiliscope - Code ouvert et documenté','meta'=>'Des scripts open-source accessibles sur Gitlab & développés avec des technologies libres et open source (javascript, libraire D3.js)'],
        'opendata'=>['title' => 'Les données open-data du Mobiliscope','meta'=>'Des données librement accessibles en open-data, téléchargeables sur la plate-forme ou via un dépot zenodo'],
        'reuses'=>['title' => 'Mobiliscope - Articles reproductibles','meta'=>'Les travaux de recherche issus du Mobiliscope qui sont entièrement reproductibles'],
        'cite'=>['title' => 'Comment citer le Mobiliscope?','meta'=>'Le modèle de citation du Mobiliscope avec le doi associé'],

        //'multitask-tool'=>['title'=>'Le Mobiliscope – un outil multifonction','meta'=>'Un outil spatio-temporel sur la ville pour  les acteurs de l’aménagement, la communauté scientifique, les enseignants, les élèves et le grand public.'],
        'scientific-tool'=>['title'=>'Mobiliscope - Un outil de recherche','meta'=>'Pour explorer les effets de lieu au quotidien. Pour sortir d\'une approche statique de la justice spatiale.'],
        'pedagogical-tool'=>['title'=>'Mobiliscope - Un outil pédagogique','meta'=>'A disposition des enseignants d\'histoire-géo pour faire découvrir la ville et ses divisions socio-spatiales à l\'école primaire, au collège, au lycée et à l\'université.'],
        'planning-tool'=>['title'=>'Mobiliscope - Un outil d\'aménagement','meta'=>'Comment agir "au bon endroit et au bon moment" ? Une réflexion à mener en lien avec les politiques temporelles (bureaux des temps), les pulsations urbaines, l’approche chronotopique etc.'],


        //'toolbox'=>
        'zoning'=>['title'=>'Filtrer selon les zonages institutionnels','meta'=>'Connaitre les territoires français du Mobiliscope intégrés aux zonages de l\'action publique '],
    ];




    if($section == "info"){
        $page = (!empty($_GET['page']) && in_array($_GET['page'], array_keys($pages) ) ) ? $_GET['page']: 'about';
        $subpage = (!empty($_GET['subpage']) && in_array($_GET['subpage'], array_keys($subpages[$page]) ) ) ? $_GET['subpage']: '';
        $sectionName = "Info";
        $pagePath = "/".$section."/".$page."/".$subpage;
        $pageName = $subpages[$page][$subpage];
        $pageCrumbs .= "<i class='fas fa-chevron-right'></i>
                <span class=\"cur-bc-page\">".$pages[$page]."</span>
                <i class='fas fa-chevron-right'></i>
                <h1 class=\"cur-bc-subpage\">".$subpages[$page][$subpage]."</h1>";
    }

    if($section == "geoviz"){
        $page = $curCity;
        $sectionName = "Maps";
        $pagePath = "/".$section."/".$page;
        $pageName = !empty($pages[$page]) ? $pages[$page] : $city[$page]['longName'][$language];
        $pageCrumbs = "";
    }

    if($section == 'home'){
        $pagePath = "";
        $pageName = ' La ville à toute heure';
        $pageCrumbs .= "";
        $pageCrumbs = "<i class='fas fa-chevron-right'></i><a href='/".$language."'>Accueil</a>";
    }

    if($section == 'geoviz')
        $pageCrumbs = "";


    $pageSEODefaut = [
        'title'=>'Mobiliscope - ' .  $pageName . '',
        'meta'=> $pageName ." - le Mobiliscope, un outil cartographique interactif pour explorer la population présente, la composition sociale des quartiers et la ségrégation des villes au cours des 24 heures de la journée!",
    ];


    $titleSEO = ( !empty($subpage) && !empty($pageSEO[$subpage]) ) ? $pageSEO[$subpage]['title'] : ( !empty($pageSEO[$page]) ? $pageSEO[$page]['title'] : $pageSEODefaut['title'] ) ;
    $metaSEO  = ( !empty($subpage) && !empty($pageSEO[$subpage]) ) ? $pageSEO[$subpage]['meta']  : ( !empty($pageSEO[$page]) ? $pageSEO[$page]['meta'] :  $pageSEODefaut['meta'] );
    $pageTitle = !empty( $titleSEO ) ?  $titleSEO : $pageSEODefaut['title'];
    $pageMeta = !empty( $metaSEO ) ? $metaSEO : $pageSEODefaut['meta'];

    $curPage =  [
        'pagePath' => "/".$language.$pagePath,
        'pagePathNoLang' => $pagePath,
        'pageName' => $pageName,
        'pageCrumbs' => $pageCrumbs,
        'pageTitle' => $pageTitle,
        'pageMeta' => $pageMeta,
    ];


    //var_dump($curPage);
?>
