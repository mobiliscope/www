<?php

$section = 'home';

include ('./settings.php');

?>
<!DOCTYPE html>
<html lang="<?php echo $language; ?>">
<meta charset="utf-8">

<head>

  <title><?php echo $curPage['pageTitle']; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php echo $curPage['pageMeta']; ?>" />
  <meta name="author" content="Julie Vallée, Constance Lecomte & Aurélie Douet">

  <script src="/dist/index.js"></script>
  <link rel="icon" href="/dist/assets/favicon.png">

  <meta property="og:locale" content="fr_FR" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $curPage['pageTitle']; ?>" />
  <meta property="og:description" content="<?php echo $curPage['pageMeta']; ?><?php echo $curPage['pageMeta']; ?>" />
  <meta property="og:url" content="https://mobiliscope.cnrs.fr" />
  <meta property="og:site_name" content="Mobiliscope" />
  <meta property="og:image" content="https://mobiliscope.cnrs.fr/dist/assets/mobiliscope-fb.png" />
  <meta property="og:image:secure_url" content="https://mobiliscope.cnrs.fr/dist/assets/mobiliscope-fb.png" />
  <meta property="og:image:width" content="1200" />
  <meta property="og:image:height" content="630" />
  <meta property="og:image:alt" content="<?php echo $curPage['pageTitle']; ?>" />
  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:title" content="<?php echo $curPage['pageTitle']; ?>" />
  <meta name="twitter:description" content="<?php echo $curPage['pageMeta']; ?>" />
  <meta name="twitter:image" content="https://mobiliscope.cnrs.fr/dist/assets/mobiliscope-tw.png" />

  <link rel='stylesheet'  href='/dist/index.bundle.css' type='text/css' media='all' />

  <link rel="preload" as="image" href="/dist/assets/slider.jpg" imagesrcset="/dist/assets/slider-small.jpg 570w, /dist/assets/slider.jpg 3672w" imagesizes="(max-width: 768px) 100vw, 768px" media="all" />

  <link rel="preload" as="image" href="/dist/assets/cnrs.png" media="all" />

  <link rel="preload" as="image" href="/dist/assets/logo-title.svg" imagesrcset="/dist/assets/logo-mobile.svg 570w, /dist/assets/logo-title.svg 3672w" imagesizes="(max-width: 768px) 100vw, 768px" media="all" />


  <script type="text/javascript">
  <?php
    include('../data-settings/translation.php');
    $_t = $translation['frontTranslation'];
    echo "var translation = " . json_encode($_t) . ";\n";
  ?>
  </script>

<!-- Google Search Console -->
<meta name="google-site-verification" content="znFg1mx3EyhKWIUAsC4uUEL0_kPdnTx9jr_5x1Y3ka0" />
<!-- End Google Search Console -->

<!-- Google Search Console -->
<meta name="google-site-verification" content="znFg1mx3EyhKWIUAsC4uUEL0_kPdnTx9jr_5x1Y3ka0" />
<!-- End Google Search Console -->

<!-- Matomo -->
  <script type="text/javascript">
    var _paq = window._paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://analyseweb.huma-num.fr/";
      _paq.push(['setTrackerUrl', u+'matomo.php']);
      _paq.push(['setSiteId', '325']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
<!-- End Matomo Code -->
</head>

<body>

  <div id="container">

  <?php include ('./topbar.php'); ?>

    <div class="row sectionHeader section" >
      <div class="text-block">
        <div class="inner">
          <img src="/dist/assets/cnrs.png" alt="cnrs" width="70"/>
          <div class="logo"><h1>LA VILLE À TOUTE HEURE</h1>
            <div >
              <?php if($section == 'geoviz'){ ?>
                <span class="top-city-name" id="cityName"><i class="fas fa-map-marker-alt"></i>&nbsp;<?php echo $curPage['pageName']?></span>
              <?php } ?>
              <span class = "top-search-container">
                <input placeholder="Rechercher un nom de ville, de commune..." type="search" id="search-box" class = "typeahead"  autocomplete="off" />
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="home container">

      <div class="sectionTitle section" >

        <div class="row left-home-img">
          <h2 class="section-title first-title">LES POPULATIONS PRÉSENTES ET LEURS MOBILITÉS </br> AU COURS DE LA JOURNÉE</h2>
          <div class="col-sm-12 col-md-6"></div>

          <div class="col-sm-12 col-md-6 ">
            <section class="right-side">
              <h4>
              Les individus sont mobiles et leurs déplacements quotidiens modifient la géographie des territoires.</br>
                </br>

                Le Mobiliscope est un outil de géovisualisation qui donne à voir les <b>variations de la population présente</b> dans les territoires au cours des 24 heures d’une journée typique de semaine. Il propose des <b>cartes</b> et des <b>graphiques interactifs</b> pour explorer heure par heure la <b>mixité sociale</b> des territoires ainsi que leur <b>attractivité</b> en fonction du profil démographique et social des populations présentes, de leurs activités et des modes de transport utilisés.</br></br>

                C'est un outil <b>libre</b> et <b>gratuit</b> qui s’inscrit dans une démarche de science ouverte.</br>

              </h4>

              <div class="row hidden-sm">
                <button class="style-button mb50"><a href="/fr/info/about/summary">En savoir plus</a></button>
              </div>
            </section>
          </div>
        </div>

        <div class="row hidden-md hidden-lg left-home-img-mobile">
          <button class="style-button mt50"><a href="/fr/info/about/summary">En savoir plus</a></button>
        </div>

      </div>
    <div class="row sectionTitle section hidden-sm" >
      <div class="section-container">
        <div>
          <h2 class="section-title">UN OUTIL DE GÉOVISUALISATION ISSU DE GRANDES ENQUÊTES PUBLIQUES</h2>
          <div class="img-section-lg"><img src="/dist/assets/bandeau-violet-v4.png" /></div>
          <p>
            Issues de grandes enquêtes publiques, les données sur les déplacements quotidiens ont été mises en forme et analysées par un laboratoire public de recherche.</br>
            De ce travail est né le Mobiliscope qui permet d'explorer la répartition spatiale des populations présentes au cours de la journée dans un grand nombre de territoires en France, au Canada et en Amérique latine.</br></br>
          </p>
        </div>
        <button class="section-link">  <a href="/fr/info/methods/data">Découvrir les données initiales et les traitements effectués</a></button>
      </div>
    </div>

    <div class="row sectionTitle section hidden-lg hidden-md " >
      <div class="section-container">
        <div>
          <h2 class="section-title">UN OUTIL DE GÉOVISUALISATION ISSU DE GRANDES ENQUÊTES PUBLIQUES</h2>
          <div class="img-section"><span><img src="/dist/assets/footprint-v4.png" /></span></div>
          <div class="img-section"><span><img src="/dist/assets/people-v4.png" /></span></div>
          <div class="img-section"><span><img src="/dist/assets/location-v4.png" /></span></div>
          <p>
            Issues de grandes enquêtes publiques, les données sur les déplacements quotidiens ont été mises en forme et analysées par un laboratoire public de recherche.</br>
            De ce travail est né le Mobiliscope qui permet d'explorer la répartition spatiale des populations au cours de la journée dans un grand nombre de territoires en France, au Canada et en Amérique latine</br></br>.
        </p>
        </div>
        <button class="section-link">  <a href="/fr/info/methods/data">Découvrir les données initiales et les traitements effectués</a></button>
      </div>
    </div>

<!--     <div class="row sectionTitle section">
      <div class="section-container whitebg fullwidth-section">
        <h2 class="section-title">COMMENT ÇA MARCHE?</h2>
        <div class="video-container">
          <div class="responsive-video-container">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/PUwmA3Q0_OE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div> -->



    <div class = "row section sectionTitle HomeMap">
    	<h2 class="section-title">Choisissez la ville à explorer</h2>
      <div>
        <div id="loader">
          <div class="spinner tertiary"></div>
        </div>
      </div>

      <div class="col-sm-12 col-md-6"  style="padding:0;">
        <div id="map-container1" class="sectionMap"></div>
        <h2 class="map-title">France</h2>
      </div>


      <div class="col-sm-12 col-md-6" style="padding:0;">


        <div id="map-container2" class="sectionMap"></div>
        <h2 class="map-title">Canada</h2>

      </div>

      <div class="col-sm-12 col-md-6" style="padding:0; margin: 0 auto;">


        <div id="map-container3" class="sectionMap"></div>
        <h2 class="map-title"> Amérique latine</h2>

      </div>
    </div>

    <div class="section sectionTitle">
      <div class="section-container greybg fullwidth-section">
        <div class="card-container">
          <h2 class="section-title">LE MOBILISCOPE VU PAR NOS UTILISATEURS.RICES</h2>

          <?php
          $section = 'home';
          include ('./bloc-temoignages.php');
          ?>

        </div>
      </div>
    </div>

    <div class="row">
      <div class = "sectionCitiesList ">
        <div class="section-container whitebg fullwidth-section">
          <h2 class="section-title">Les villes du Mobiliscope</h2>
          <h5 class="section-subtitle">France</h5>
          <div class = "row">
            <?php
            $len = sizeof($frenchcities);
            $numpercol = (int) ($len / 4) + 1;


            for($i = 0; $i < 4; $i++){
              echo "<div class='col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-5 col-md-5 col-lg-offset-0 col-lg-3'><section>";
              echo "<ul class=\"cityList\">";
              for($j = 0; $j < $numpercol; $j++){
                $theCity = !empty(array_values($frenchcities)[$numpercol*$i+$j]) ? array_values($frenchcities)[$numpercol*$i+$j] : '';
                $frenchcitiesSlugs = array_keys($frenchcities);
                if(!empty($theCity))
                echo "<li><i class='arrow right'></i><a href='/" . $language . '/geoviz/' . $frenchcitiesSlugs[$numpercol*$i+$j] . "'>" . array_values($frenchcities)[$numpercol*$i+$j] . "</a></li>  ";
              }

              echo "</ul><br>";
              echo "</section></div>";
            }
            ?>

          </div>

          <div class="sectionSpacerSmallMobile"></div>

          <h5 class="section-subtitle">Canada</h5>
          <div class = "row">
            <?php
            $len = sizeof($cancities);
            $numpercol = (int) ($len / 4) + 1;


            for($i = 0; $i < 3; $i++){
              echo "<div class='col-sm-offset-1 col-sm-10 col-md-offset-0 col-md-4'><section>";
              echo "<ul class=\"cityList\">";
              for($j = 0; $j < $numpercol; $j++){
                $theCity = !empty(array_values($cancities)[$numpercol*$i+$j]) ? array_values($cancities)[$numpercol*$i+$j] : '';
                $cancitiesSlugs = array_keys($cancities);
                if(!empty($theCity))
                echo "<li><i class='arrow right'></i><a href='/" . $language . '/geoviz/' . $cancitiesSlugs[$numpercol*$i+$j] . "'>" . array_values($cancities)[$numpercol*$i+$j] . "</a></li>  ";
              }

              echo "</ul><br>";
              echo "</section></div>";
            }
            ?>

          </div>

          <div class="sectionSpacerSmallMobile"></div>

          <h5 class="section-subtitle"> Amérique latine</h5>
          <div class = "row">
            <?php
            $len = sizeof($ascities);
            $numpercol = (int) ($len / 4) + 1;


            for($i = 0; $i < 3; $i++){
              echo "<div class='col-sm-offset-1 col-sm-10 col-md-offset-0 col-md-4'><section>";
              echo "<ul class=\"cityList\">";
              for($j = 0; $j < $numpercol; $j++){
                $theCity = !empty(array_values($ascities)[$numpercol*$i+$j]) ? array_values($ascities)[$numpercol*$i+$j] : '';
                $ascitiesSlugs = array_keys($ascities);
                if(!empty($theCity))
                echo "<li><i class='arrow right'></i><a href='/" . $language . '/geoviz/' . $ascitiesSlugs[$numpercol*$i+$j] . "'>" . array_values($ascities)[$numpercol*$i+$j] . "</a></li>  ";
              }

              echo "</ul><br>";
              echo "</section></div>";
            }
            ?>

          </div>

        </div>
      </div>
    </div>

  </div>



  <?php include('./footer.php'); ?>




  <script type="text/javascript" src="/dist/scripts/typeahead.bundle.min.js"></script>
  <script type="text/javascript" src="/dist/scripts/slideshow.js"></script>
  <script type="text/javascript" src="/dist/scripts/menu.js"></script>
  <script type="text/javascript" src="/dist/scripts/map-printer.js"></script>

</body>

</html>
