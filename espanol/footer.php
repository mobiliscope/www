<div class="footer">
	<div class="row sectionFooter">

	    <div class="col-sm-12 col-md-8 col-md-offset-2">

			<div class="row">

				<div class="col-sm-12 col-md-2 hidden-md hidden-lg">
							<figure class="inline">
									<img src="/dist/assets/cnrs.png" alt="cnrs" width="50"/>
							</figure>
						</div>

	            <div class="col-sm-6 col-md-5">
						<h3>CONTACTO</h3>
									<a class="mail-link" href="mailto:mobiliscope@cnrs.fr">mobiliscope@cnrs.fr</a></br></br>
	            </div>

	    		<div class="col-sm-12 col-md-2 hidden-sm">
	            	<figure class="inline">
	              		<img src="/dist/assets/cnrs.png" alt="cnrs" width="50"/>
	            	</figure>
	          	</div>

	    		<div class="col-sm-6  col-md-5">
						 <h3>CIENCIA ABIERTA</h3>
					<a href="https://gitlab.huma-num.fr/mobiliscope" target="_blank"><i class="fab fa-gitlab"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="https://zenodo.org/records/11111161" target="_blank"><i class="fab fa-zenodo"></i></a>&nbsp;&nbsp;&nbsp;

	    		</br>
	            </br>
	          	</div>

	      	</div>

	    	<div class="footer-menu-item">
	        	<span><a href="/es/info/about/team">EQUIPO</a></span>|
	        	<span><a href="/es/info/about/partners">COLABORADORES</a>|
	        	<span><a href="/es/info/open/cite">¿COMO CITAR?</a>
	    	</div>

      	</div>
	</div>

		<!-- <div id="copyright"> Mobiliscope - <?php echo date('Y'); ?> <a href="/es/info/open/evolution">(v4.0)</a></div> -->
		<div id="copyright"> Mobiliscope <a href="/es/info/methods/evolution">[v4.3]</a></div>


</div>
