﻿<?php
include($_SERVER['DOCUMENT_ROOT'].'/data-settings/city.php');
//phpinfo();
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include($_SERVER['DOCUMENT_ROOT'].'/minio-settings.php');

$language = "es";
$page = '';
$subpage = '';
$pageMeta = '';
$sectionName = '';
$pagePath = '';
$pageCrumbs = "<a href='/".$language."'>Inicio</a>";


$curCity = (!empty($_GET['city']) && in_array($_GET['city'], array_keys($city)))? $_GET['city']: 'albi';


if( $bucket == 'mobiliscope' ) {
    $path =  $path . '/' . $curCity;
} else
    $path =  $hash;

if(!empty($_GET['hash']) ){
    $filters = getObject($s3Client, $bucket, $path,'filters.json');
    $filters_array = json_decode($filters, true);
    $curCity = $filters_array['request']['study'];
}
 // List of pages and their titles
    $pages = [
        'about'=>'Acerca de',
        'methods'=>'Métodos',
        'open' => "Ciencia abierta",
        'multitask-tool'=>'Aplicaciones',
        'toolbox'=>'Toolbox',
    ];

    $subpages = [
        'about'=>[
            'summary'=>'En resumen',
            'team'=>'Equipo',
            'partners'=>'Instituciones colaboradoras',
			'publications'=>'Publicaciones & co',

        ],
        'methods'=>[
            'data'=>'Transformar los viajes',
            'indicators'=>'Caracterizar la población presente',
            'geovizualisation'=>'Geovisualización',
            'evolution'=>'Cambios y actualizaciones',

        ],
        'open'=>[
            'license'=>'Código abierto y documentado',
            'opendata'=>'Datos abiertos y reutilizables',  
            'reuses'=>'Publicaciones reproducibles',
            'cite'=>'¿Cómo citar Mobiliscope?',

        ],
        'multitask-tool'=>[
            'scientific-tool'=>'Ciencia & Investigación',
            'planning-tool'=>'Planificación',
            'pedagogical-tool'=>'Educación',

        ],
        'toolbox'=>[          
            'zoning'=>'Filtrar por zonas institucionales <small>(Francia)</small>',
        ]
    ];

    // this description will be used by Google as the text under the title of the page
    // TITLE it should be max 70 char
    // META it should be max 230 char

      $pageSEO = [
      	//'about'=>['title'=>'Le Mobiliscope pour explorer la population des villes le jour et la nuit !','meta'=>'Les villes changent... Selon les heures et la mobilité quotidienne des populations, ce sont les quartiers de villes qui changent au fil des heures, mais aussi la mixité sociale et la ségrégation urbaine.'],
        'summary'=>['title'=>'Mobiliscope - En resumen','meta'=>'La movilidad diaria de la población provoca cambios en la composición social de los barrios y en la segregación de las ciudades durante las 24 horas del día. '],
		'team'=>['title'=>'Mobiliscope – Equipo','meta'=>'Una herramienta de geovisualización bajo la responsabilidad de Julie Vallée y desarrollada por Aurélie Douet y Constance Lecomte. Con la participación de Guillaume Le Roux, Hadrien Commenges y Elisa Villard'],
        'partners'=>['title'=>'Mobiliscope – Instituciones colaboradoras','meta'=>'Desarrollado en el laboratorio Géographie-cités, Mobiliscope cuenta con el apoyo de CNRS, ANCT, INED, Cerema y Labex DynamiTe.'],
        'publications'=>['title'=>'Mobiliscope - Publicaciones & co','meta'=>'En relación con la geografía social, la geografía urbana, la ‘time-geography’, la geomática, la geovisualización y la geografía de la salud.'],
        
        //'methods'=>['title'=>'Les méthodes du Mobiliscope','meta'=>'Une interface interactive pour explorer la coprésence des riches et des pauvres, des femmes et des hommes, des actifs et des retraités etc. au cours des 24 heures de la journée'],
        'data'=>['title'=>'Mobiliscope - Transformar los viajes','meta'=>'Con las encuestas de viajes de los hogares (EMD, EDVM, EGT, EDGT, EMC²) del CEREMA (ex CERTU), el Mobiliscope enriquece los estudios urbanos tradicionalmente basados en los censos de población y los viajes domicilio-trabajo.'],
        'indicators'=>['title'=>'Mobiliscope – Caracterizar la población presente','meta'=>'Mapas hora a hora de las ciudades según el perfil demográfico (sexo y edad) y social (nivel de estudios, categoría socioprofesional) de las personas, su modo de transporte (a pie, en bicicleta, en transporte público, en coche), etc.'],
        'geovizualisation'=>['title'=>'Mobiliscope - Geovisualización','meta'=>'La interfaz se ha desarrollado con la biblioteca d3.js. Propone mapas animados (coropletas, círculos proporcionales, erizos) e índices de segregación (Duncan) y autocorrelación espacial (Moran) calculados hora a hora.'],
        'evolution'=>['title'=>'Mobiliscope - Cambios y actualizaciones','meta'=>'Una herramienta que ha estado en constante evolución desde que se lanzó por primera vez en 2017. Se añaden regularmente nuevas ciudades y funciones.'],

        //'open'=>
        'license'=>['title' => 'Mobiliscope - Código abierto y documentado','meta'=>'Scripts de código abierto accesibles en Gitlab y desarrollados con tecnologías libres y de código abierto (javascript, biblioteca D3.js)'],
        'opendata'=>['title' => 'Mobiliscope - Datos abiertos y reutilizables','meta'=>'Datos abiertos de libre acceso, descargables desde la plataforma o a través de un repositorio de zenodo.'],
        'reuses'=>['title' => 'Mobiliscope - Publicaciones reproducibles','meta'=>'Resultados de investigación totalmente reproducibles de Mobiliscope'],
        'cite'=>['title' => '¿Cómo citar Mobiliscope?','meta'=>'El modelo de cita Mobiliscope con el doi asociado'],

        
        //'multitask-tool'=>['title'=>'Le Mobiliscope – un outil multifonction','meta'=>'Un outil spatio-temporel sur la ville pour  les acteurs de l’aménagement, la communauté scientifique, les enseignants, les élèves et le grand public.'],
        'scientific-tool'=>['title'=>'Mobiliscope - para la Ciencia y la Investigación','meta'=>'Explorar los efectos del lugar en la vida cotidiana. Abandonar el enfoque estático de la justicia espacial.'],
        'planning-tool'=>['title'=>'Mobiliscope - para la Planificación ','meta'=>'¿Cómo actuar "en el lugar y el momento adecuados"? Una reflexión a realizar en relación con las políticas temporales (oficinas del tiempo), las pulsaciones urbanas, el enfoque cronotópico, etc.'],
        'pedagogical-tool'=>['title'=>'Mobiliscope - para la Educación','meta'=>'A disposición de los docentes de historia y geografía para ayudarles a descubrir la ciudad y sus divisiones socioespaciales en las escuelas primarias, secundarias y universitarias.'],


        //'toolbox'=>
        'zoning'=>['title'=>'Filtrar por zonas institucionales','meta'=>'Conocer los territorios franceses del Mobiliscope integrado en la zonificación de la acción pública'],
   

    ];



    if($section == "info"){
        $page = (!empty($_GET['page']) && in_array($_GET['page'], array_keys($pages) ) ) ? $_GET['page']: 'about';
        $subpage = (!empty($_GET['subpage']) && in_array($_GET['subpage'], array_keys($subpages[$page]) ) ) ? $_GET['subpage']: '';
        $sectionName = "Info";
        $pagePath = "/".$section."/".$page."/".$subpage;
        $pageName = $subpages[$page][$subpage];
        $pageCrumbs .= "<i class='fas fa-chevron-right'></i>
                <span class=\"cur-bc-page\">".$pages[$page]."</span>
                <i class='fas fa-chevron-right'></i>
                <h1 class=\"cur-bc-subpage\">".$subpages[$page][$subpage]."</h1>";
    }

    if($section == "geoviz"){
        $page = $curCity;
        $sectionName = "Maps";
        $pagePath = "/".$section."/".$page;
        $pageName = !empty($pages[$page]) ? $pages[$page] : $city[$page]['longName'][$language];
        $pageCrumbs = "";
    }



    if($section == 'home'){
        $pagePath = "";
        $pageName = ' La ciudad a todas horas';
        $pageCrumbs .= "";
        $pageCrumbs = "<i class='fas fa-chevron-right'></i><a href='/".$language."'>Accueil</a>";
    }

    if($section == 'geoviz')
        $pageCrumbs = "";


    $pageSEODefaut = [
        'title'=>'Mobiliscope - ' .  $pageName . '',
        'meta'=> $pageName ." - le Mobiliscope, un outil cartographique interactif pour explorer la population présente, la composition sociale des quartiers et la ségrégation des villes au cours des 24 heures de la journée!",
    ];


    $titleSEO = ( !empty($subpage) && !empty($pageSEO[$subpage]) ) ? $pageSEO[$subpage]['title'] : ( !empty($pageSEO[$page]) ? $pageSEO[$page]['title'] : $pageSEODefaut['title'] ) ;
    $metaSEO  = ( !empty($subpage) && !empty($pageSEO[$subpage]) ) ? $pageSEO[$subpage]['meta']  : ( !empty($pageSEO[$page]) ? $pageSEO[$page]['meta'] :  $pageSEODefaut['meta'] );
    $pageTitle = !empty( $titleSEO ) ?  $titleSEO : $pageSEODefaut['title'];
    $pageMeta = !empty( $metaSEO ) ? $metaSEO : $pageSEODefaut['meta'];

    $curPage =  [
        'pagePath' => "/".$language.$pagePath,
        'pagePathNoLang' => $pagePath,
        'pageName' => $pageName,
        'pageCrumbs' => $pageCrumbs,
        'pageTitle' => $pageTitle,
        'pageMeta' => $pageMeta,
    ];


    //var_dump($curPage);
?>
