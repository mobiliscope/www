<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">

	<section>
		<h2> Filtrar por zonas institucionales (Francia)</h2>
        <center>
            <h4>Conozca las zonas francesas Mobiliscope que forman parte de programas de acción pública:&nbsp;:</br>
            <i>Actions Coeur de Ville (ACV)</i>, <i>Petites Villes de Demain (PVD)</i> et <i>Quartiers Prioritaires en Politique de la Ville (QPV)</i>.</h4>
        </center>
	</section>

    </br>

    <section>
        <h3>Action Coeur de Ville (ACV)</b></h3>
        
        <p> 
            Mobiliscope cubre <b>88</b> de los 235 <b>municipios</b> del programa <i><a href = "https://agence-cohesion-territoires.gouv.fr/action-coeur-de-ville-42" target="_blank">Action Coeur de Ville</a></i> - <small>lista de noviembre de 2021</small>.
        </p>

        <p>
            Estos 88 municipios ACV se enumeran en la siguiente tabla.
            <br>
            <small>
            <ul>
            <li>La tabla puede ordenarse, filtrarse y descargarse en formato csv.</li>
            <li>Al hacer clic en el nombre de un municipio, se le redirigirá a la geovisualización asociada.</li>
            <li>La categoría <i>Tipo de correspondencia</i> indica si el municipio está dividido en varios sectores Mobiliscope («Municipio multisectorial»), corresponde a un único sector («Municipio monosectorial») o está incluido en un sector más amplio formado por varios municipios («Municipio infrasectorial»). Algunos «Casos particular» están relacionados con fusiones de municipios o zonas comunales discontinuas.</li>
            </ul>
            </small>
        </p>
        
            <br>

            <?php include('table/acv_table.html'); ?> 
        </p>
    
    <p class="ss-style-dots" ></p>

    </section>


	<section>
		<h3>Petites Villes de Demain (PVD)</h3>

        <p> 
            Mobiliscope cubre <b>502</b> de los 1643 <b>municipios</b> del programa  <i><a href = "https://agence-cohesion-territoires.gouv.fr/petites-villes-de-demain-45" target="_blank">Petites Villes de Demain</a></i> - <small>lista de noviembre de 2021</small>.
        </p>

        <p>
            Estos 502 municipios PVD se enumeran en la siguiente tabla.
            <br>
            <small>
            <ul>
            <li>La tabla puede ordenarse, filtrarse y descargarse en formato csv.</li>
            <li>Al hacer clic en el nombre de un municipio, se le redirigirá a la geovisualización asociada.</li>
            <li>La categoría <i>Tipo de correspondencia</i> indica si el municipio está dividido en varios sectores Mobiliscope («Municipio multisectorial»), corresponde a un único sector («Municipio monosectorial») o está incluido en un sector más amplio formado por varios municipios («Municipio infrasectorial»). Algunos «Casos particular» están relacionados con fusiones de municipios o zonas comunales discontinuas.</li>
            </ul>
            </small>
        </p>
            
            <br>

            <?php include('table/pvd_table.html'); ?>
        </p>

    <p class="ss-style-dots" ></p>

    </section>

	<section>
		<h3>Barrios Prioritarios - France - France</h3>

         <p>
            <b>686</b> de los 2572 <b>sectores</b> del Mobiliscope incluyen Barrios Urbanos Prioritarios ('Quartier Prioritaire en Politique de la Ville - QPV'): 871 de los 1514 Barrios Prioritarios - France existentes en Francia están cubiertos por el Mobiliscope.
        </p>

        <p>
            Estas 686 sectores con 'Barrios Prioritarios' se enumeran en la siguiente tabla.
            <br>
            <small>
            <ul>
            <li>La tabla puede ordenarse, filtrarse y descargarse en formato csv.</li>
            <li>Haciendo clic en el nombre del sector, se le redirigirá a la geovisualización asociada.</li>
            <li>La categoría <i>Nivel de cobertura</i> corresponde a la proporción de la superficie del sector Mobiliscope que está cubierta por uno o más 'Barrios Prioritarios'.</li>
            <li>Los 'Barrios Prioritarios' tenidos en cuenta proceden de la <a href = "https://sig.ville.gouv.fr/page/198/les-quartiers-prioritaires-de-la-politique-de-la-ville-2015" target="_blank">geografía prioritaria de<b>2015</b></a> (y no de la nueva geografía prioritaria <a href = "https://sig.ville.gouv.fr/page/224/les-quartiers-prioritaires-de-la-politique-de-la-ville-2024" target="_blank">actualizada en <b>2024</b></a>).</li>
            </ul>
            </small>

            <br>

            <?php include('table/qpv_table.html'); ?> 
        </p>

        <p> 
        Mobiliscope dispone de otras funciones para explorar los ritmos cotidianos de los 'Barrios Prioritarios'. Puede :
        <ul>
            <li>visualizar en el mapa los límites de los 'Barrios Prioritarios' (2015) <img src="/dist/assets/layers2.png" width="25px" height= "25px"/></li>
            <li>medir (en cifras y %) la población presente según su lugar de residencia dentro o fuera de un 'Barrio Prioritario'</li>
        </ul>
        </p>

    <p class="ss-style-dots" ></p>

    </section>

    <section>
        <h3>Metodología y datos abiertos</h3>

        <p>
        <b>49 ciudades/regiones</b> francesas son accesibles en la versión actual de Mobiliscope. Estas 49 ciudades/regiones francesas están divididas en <b>2.572 sectores</b> que abarcan un total de más de 10.000 municipios donde vive el 60% de la población del país.</br>
        <i>Nota: 20 sectores (y 79 municipios) abarcan una zona compartida por varias ciudades/regiones.</i>
        </p>

        <h4>Etapa 1.</h4> 

        <p>
        Existen cuatro tipos de correspondencia entre municipios -> sectores:
        <ul>
            <li>Un municipio se divide en varios sectores («Municipio multisectorial»). Suele ser el caso de las ciudades (grandes).</li>
            <li>Un municipio corresponde a un único sector («Municipio monosectorial»).</li>
            <li>Un municipio está incluido en un sector más amplio formado por varios municipios («municipio infrasectorial»). Este tipo de situación se da sobre todo en las zonas periféricas y rurales.</li>
            <li>Los escasos «Casos particular» (2%) están relacionados, por ejemplo, con fusiones de municipios o zonas comunales discontinuas.</li>
        </ul>
        </p>

        <p>
            Los <b>archivos csv</b> de correspondencia Mobiliscope <i>commune->sector</i> (y también <i>sector->comuna</i>) pueden descargarse <a href="/data-settings/commune_secteur.zip" title="commune_secteur" download="commune_secteur.zip"><b>aquí</b></a>, junto con la <b>documentación</b> (descripción del método y diccionario de variables). 
        </p>

        <h4>Etapa 2.</h4> 

        <p>
            Se hace una referencia cruzada con los municipios de los programas <i>Action Coeur de Ville (ACV)</i> y <i>Petites Villes de Demain (PVD)</i> (así como con la zonificación de las Zonas Urbanas (ZAU), la zonificación de las Áreas de Atracción Urbana (AAV) y las cuadrículas de densidad municipal).
        </p>

        <p>
            Los <b>archivos csv</b> que enumeran los <b>municipios</b> (y también los <b>sectores</b> Mobiliscope) en los diferentes esquemas de zonificación pueden descargarse <a href="/data-settings/commune_secteur_zonages.zip" title="commune_secteur_zonages" download="commune_secteur_zonages.zip"><b>aquí</b></a>, junto con la <b>documentación</b> (descripción de los conjuntos de datos utilizados para los esquemas de zonificación, descripción del método y diccionario de variables). 
        </p>

    </br>
    </br>
    </br>
    </br>

    </section>

</div>


<script type="text/javascript">
$(document).ready(function () {

	// DataTable PVD
	$('#pvdtable thead tr')
        .clone(true)
        .addClass('filterspvd')
        .appendTo('#pvdtable thead');

    $('#pvdtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'pvd_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,1])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filterspvd th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    console.log(title);
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filterspvd th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([2,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filterspvd th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#pvdtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });

    

    // DataTable ACV
    $('#acvtable thead tr')
        .clone(true)
        .addClass('filtersacv')
        .appendTo('#acvtable thead');

    $('#acvtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'acv_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,1])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersacv th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filtersacv th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([2,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersacv th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#acvtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });


	// DataTable QPV
    $('#qpvtable thead tr')
        .clone(true)
        .addClass('filtersqpv')
        .appendTo('#qpvtable thead');

    $('#qpvtable').DataTable({
        dom: 'lfrtipB',
        buttons: [
            {
                extend: 'csv',
                title: 'qpv_in_mobiliscope'
            }
        ],
        orderCellsTop: true,
        order: [[3, 'desc']],
        fixedHeader: {
            header: true,
            headerOffset: 62,
            footer: false
        },
        initComplete: function () {
            var api = this.api();

            // Apply the search
            api
                .columns([0,2])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersqpv th').eq($(api.column(colIdx).header()).index());
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    $(cell).html('<input type="text" placeholder="Search ' + title + '" />');
                    var column = this;
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filtersqpv th').eq($(api.column(colIdx).header()).index())
                    )
                        .on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });

            // Apply the filter
            api
                .columns([1,3])
                .every(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filtersqpv th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title0 = $(cell).text();
                    var title = title0.split(" ")[0];
                    var column = this; 
                    var select = $('<select><option value="">All ' + title + '</option></select>')
                        .appendTo( $("#qpvtable thead tr:eq(1) th").eq(column.index()).empty() )
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
        
    });
	
});

</script>
