<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
	<h2>Publicaciones reproducibles</h2>
	</br>

	<p>
		El Mobiliscope ha dado lugar a varios artículos científicos (véase la <a href="/es/info/about/publications">lista</a>). Hacer que los datos de Mobiliscope estén disponibles como datos abiertos también ofrece la oportunidad de publicar artículos científicos totalmente reproducibles. 
	</p>
	</section>

	<section>
		<h3>Intersectional approach of everyday geography</h3>
	</section>

		<h4>Abstract</h4>
		<section>
			<p>
			In a paper published in the journal <i>Environment and Planning B: Urban Analytics and City Science</i> (preprint sur <a href = "https://arxiv.org/abs/2106.15492v2" target="_blank">arXiv</a> et <a href = "https://hal.science/hal-04109508" target="_blank">HAL</a></li>), Julie Vallée and Maxime Lenormand use Mobiliscope data to explore hour-by-hour variations in spatial distribution of gender, age and social class within cities.

			<p>
			Hour-by-hour variations in spatial distribution of gender, age and social class within cities remain poorly explored and combined in the segregation literature mainly centred on home places from a single social dimension. Taking advantage of 49 mobility surveys compiled together (385,000 respondents and 1,711,000 trips) and covering 60% of France’s population, Julie Vallée and Maxime Lenormand use Mobiliscope data to study variations in hourly populations of 2572 French districts after disaggregating population across gender, age and education level. They first isolate five district hourly profiles (two ‘daytime attractive’, two ‘nighttime attractive’ and one more ‘stable’) with very unequal distributions according to urban gradient but also to social groups.
			</p>

			<figure class="inline">
		  	<img src="/dist/assets/fig-epb.png" alt="Intersectional approach of everyday geography" width="750"/>
			</figure>

			<p>
			Taking as reference the dominant groups (men, middle-age and high educated people) known as concentrating hegemonic power and capital, the authors then explore the intersectional forms of these everyday geographies. They analyze specifically whether district hourly profiles of 'dominant' groups diverge from those of the others groups. It is especially in the areas exhibiting strong increase or strong decrease of ambient population during the day that district hourly profiles not only combine the largest dissimilarities all together across gender, age and education level but are also widely more synchronous between dominant groups than between non-dominant groups (women, elderly and low-educated people). These intersectional patterns shed new light on areas where peers are synchronously located over the 24-hour period and thus potentially in better position to interact and to defend their common interests.
			</p>

		</section>

		<section>
		<h4>To go further</h4>
			<p>
			<ul>
				<li>Download <b>Mobiliscope data </b> - v4.1 (Extract of French Data)&nbsp;: <a href = "http://www.doi.org/10.5281/zenodo.7738571" target="_blank">zenodo repository</a> (licence ODbL)</li>
			
				<li>Access <b>R codes</b>&nbsp;: <a href = "https://gitlab.huma-num.fr/mobiliscope/intersectionality" target="_blank">gitlab repository</a> (licence GPLv3)</li>

				<li>View they <b>results</b>&nbsp;: <a href = "https://shiny.umr-tetis.fr/Intersectionality" target="_blank">shiny platform</a></li>
			</ul>
			</p>
		</section>
		
		
		<section>
		<h4>Reference</h4>

			<p>
			Vallée J, Lenormand M (2024). <a href = "https://dx.doi.org/10.1177/23998083231174025" target="_blank">Intersectional approach of everyday geography</a>, <i>Environment and Planning B: Urban Analytics and City Science</i>, 51(2), 347-365. doi:10.1177/23998083231174025. Preprint <a href = "https://arxiv.org/abs/2106.15492v2" target="_blank">arXiv</a> & <a href = "https://hal.science/hal-04109508" target="_blank">HAL</a>
			</p>
		</section>


	</br>
	<section>
		<i><h3>Próximamente se publicarán otros documentos reproducibles!</h3></i>

	</section>


	 </br>
	 </br>	
	 </br>
	 </br>	


</div>
