<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Para la educación</h2>
	</section>

	<p>
    Estudiantes de distintas edades pueden utilizar  para descubrir las ciudades yMobiliscope la movilidad diaria de sus habitantes.
	</p>

	<ul>
		<li>En las <b>escuelas primarias</b>, el Mobiliscope ha demostrado ser un medio divertido para que los alumnos exploren los distintos tipos de desplazamientos (pendulares, puntuales y regulares) y la morfología urbana. Para hacer la experiencia más lúdica, se pidió a los alumnos que respondieran a un pequeño cuestionario utilizando tabletas en las que pueden explorar sus ciudades y sus barrios.</li>

		<li>En <b>escuelas secundarias</b> y <b>universidades</b>, el Mobiliscope también se utiliza en relación con estudios urbanos (movilidad, segregación, mecanismos de desigualdad social...) o métodos cartográficos/geovisualización.</li>

		</ul>
	</p>


	<h4>
	<figure class="inline">
	<img src="/dist/assets/ecole2.jpg" alt="ecole2" width="220"/>
	<img src="/dist/assets/ecole3.jpg" alt="ecole1" width="220"/>
	<img src="/dist/assets/ecole4.jpg" alt="ecole2" width="220"/>
	</figure>
	</h4>

	</section>

	<section>
     <h3>¡Comparta con nosotros sus experiencias pedagógicas!</h3>
    <p>
		No dude en enviarnos un mensaje para compartir sus experiencias con Mobiliscope en la escuela primaria, en el instituto o en la universidad. <br> Estamos muy interesados en sus comentarios.
		Además podemos acompañarle en la realización de acciones pedagógicas específicas sobre su barrio, su ciudad...
    </p>
	</section>

	<br>
	 <br>
	 <br>

</div>
