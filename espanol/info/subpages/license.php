<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Código abierto y documentado</h2>
	
	</br>
	<p>
		En el espíritu de la ciencia abierta y los principios FAIR, ponemos a disposición nuestros programas, y con una documentación.
	</p>
	</section>

	<section>
	<h3>Data-preprocessing</h3>

		<p>
		Los datos agregados y ponderados (que no permiten la identificación de las personas encuestadas) se almacenan localmente en forma de archivos geojson para los datos espaciales y csv para los datos de atributo sin geometría. Estos datos han sido generados automáticamente con antelación gracias a dos scripts R:
		<ul>
			<li> el primero transforma los datos de desplazamiento procedentes de las encuestas en datos de presencias;</li> 
			<li>el segundo produce el conjunto de archivos necesarios para la representación (carto)gráfica para cada uno de los indicadores.</li>
		</ul>
		</p>

		<p>
		Los scripts para el preprocesamiento de los datos de Mobiliscope se encuentran en el repositorio <a href = "https://gitlab.huma-num.fr/mobiliscope/data-process" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>data-process</b></a>.
		</p>

		<p>
		Los datos producidos se almacenan en el servicio de almacenamiento <i>MinIO</i>, en forma de archivos geojson para datos espaciales y archivos csv para datos de atributos sin geometría. Gran parte de estos datos son de <a href="/es/info/open/opendata">libre acceso</a> y reutilizables. 
		</p>
				
	</section>
	
	<section>
	<h3>Software</h3>

		<p>
			Mobiliscope se desarrolla en HTML, CSS (framework <a href = "https://minicss.org/" target = "_blank"> mini.css</a>), javascript y PHP. La geovisualización se ha codificado en javascript y se basa, para la interactividad de los mapas y gráficos, en las librerías <a  href = "https://d3js.org/" target="_blank" >D3.js</a> - desarrolladas por Mike Bostock - y <a href="http://leafletjs.com" title="A JS library for interactive maps" target="_blank">Leaflet</a>. Para la superposición de las capas OSM y los mapas diseñados con D3.js se utiliza el plugin <a href="https://github.com/teralytics/Leaflet.D3SvgOverlay" target="_blank">Leaflet.D3SvgOverlay</a> bajo licencia MIT.
		</p>

		<p>
			El código fuente de las páginas web de Mobiliscope está disponible en el repositorio <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b></a>. A partir de este código fuente se despliega Mobiliscope en el servidor de Huma-Num. 
		</p>
	</section>

	<section>
	<h3>Modelo de licencia y citación</h3>
		<p>
 		Este código está disponible bajo la <b>licencia libre <a href = "https://spdx.org/licenses/AGPL-3.0-or-later.html" target="_blank">AGPL</a></b>. Esta licencia AGPL autoriza cualquier redistribución o modificación del código siempre que:
 		<ul>
   			<li>se mantenga bajo la licencia AGPL</li>

    		<li>se cite Mobiliscope<br>
    		➔ Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C,  Villard E (2024). Mobiliscope, an open platform to explore cities and social mix around the clock [v4.3]. www.mobiliscope.cnrs.fr doi:10.5281/zenodo.11111161.</li>
		</ul>	
		</p>


	<section>
	<h3>Documentación</h3>

		<p>
			Se ha redactado una nueva <b>Guía del Administrador</b> (en francés). En ella se describen los códigos utilizados, se resume el funcionamiento de los servicios y se explica cómo mantener y actualizar la herramienta.</br>
			La guía está disponible en <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">https://mobiliscope.quarto.pub/guide_admin/guide_admin.html</a>. 
		</p>

	</section>

	<section>
	<h3>Archivar versiones antiguas de código</h3>
	
		<p>
		Antes de la v4.3, el código R de procesamiento de datos y el código de software estaban archivados en <a href = "https://github.com/Geographie-cites/mobiliscope" target="_blank">github.com/Geographie-cites/mobiliscope</a>. Este archivo ya no se utiliza después de la v4.3. 
	</p>

 	</section>

	</br>
	</br>
	</br>
	</br>

</div>





	

