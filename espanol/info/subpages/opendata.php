<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
	<h2>Datos abiertos y reutilizables</h2>

	</br>
	<p>
		El desarrollo de Mobiliscope se inscribe en un enfoque de ciencia abierta: hacemos lo máximo para que los datos construidos por el equipo y visualizados en la herramienta respeten los principios FAIR: «<b>F</b>áciles de encontrar, <b>A</b>ccesibles, <b>I</b>nteroperables y <b>R</b>eutilizables».</br>
	</p>


	<section>
	<h3>Datos</h3>

		<p>
			Los <b>datos</b> (formato .csv y .geojson) se pueden <b>descargar</b> en la plataforma Mobiliscope (así como en el diccionario de datos). Con estos archivos, también hemos adjuntado un documento pdf que resume los términos de la licencia ODbL y las fuentes a indicar. 
	
		</p>
		</section>

		<section>
		<h4>Descargar ciudad por ciudad mediante la herramiental</h4>
		
		<p>
			Para descargar los datos, haga <b>clic en el botón</b> <img src="/dist/assets/download.svg" width="20px" height= "20px"/>
			<ul>
				<li> o bien arriba del mapa central para obtener los valores agregados por sector según el indicador y el modo de representación seleccionado.</li>
				<li> o bien junto al gráfico de abajo para obtener los valores de los índices de segregación para el conjunto de la región según el indicador seleccionado y el índice de segregación elegido.</li>
			</ul>
		</ul>
		</p>

		</section>

		<section>
		<h4>Descargar todo el conjunto de datos</h4>

		<p>
			Una carpeta que contiene <b>todos los datos</b> descargables en la herramienta también es <b>accesible en <a href="https://doi.org/10.5281/zenodo.11111161" target="_blank">Zenodo</a></b>.
		</p>

		<p>
			A partir de los datos de Zenodo (y del código del software del repositorio <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b>)</a>), los usuarios pueden ejecutar Mobiliscope localmente en su ordenador: todos los datos serán visibles excepto los relativos a los flujos entre los sectores del hogar y la residencia.
			</br>
			<i>Para más información sobre esta instalación local, consulte la  <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html#sec-instalocal" target = "_blank">sección correspondiente</a> de la Guía del administrador.</i>
		</p>

		<p>
		 También se archivan los datos de las versiones anteriores de la herramienta:
		 <ul>
			<li>Datos de la versión <a href="https://zenodo.org/records/7822016" target="_blank">v4.2 [All data]</a> (licence ODbL)</li>
			<li> Datos de la versión <a href="https://zenodo.org/records/7738571" target="_blank">v4.1 [Extract of French Data]</a> (licence ODbL)</li> 
			<li>Datos de la versión <a href="https://zenodo.org/records/4900655" target="_blank">v4.0 [Extract of French Data]</a> (licence ODbL)</li> 
		</p>



		</section>

		<section>
		<h4>Otros datos disponibles</h4>

		<p>
		Para Francia, también se pueden descargar otros conjuntos de datos utilizados en la herramienta <i><a href="/es/info/toolbox/zoning">Filtrar por zonas institucionales (Francia)</a></i>.
		</p>


	</section>
	<section>
	<h3>Modelo de licencia y citación</h3>
		
		<p>
		Mobiliscope datos están disponibles bajo <b>licencia libre <a href = "https://spdx.org/licenses/ODbL-1.0.html" target="_blank">ODbL</a></b>. De este modo, pueden reutilizarse y modificarse libremente siempre:
		<ul>
			<li>que se mantengan bajo la licencia ODbL</li>
			
			<li>que citen Mobiliscope</br>
			 ➔ Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C, Villard E (2024). Mobiliscope, an open platform to explore cities and social mix around the clock [v4.3]. www.mobiliscope.cnrs.fr doi:10.5281/zenodo.11111161</li>

			<li>que se mencionen las fuentes originales de los datos (las encuestas sobre desplazamientos)</br>
    		➔ que se indiquen las fuentes en el plano de la ciudad correspondiente y en las condiciones de la licencia de reutilización.</li>
		</ul>
		
		</br>


	</p>

	</section>

	 </br>
	 </br>	
	 </br>
	 </br>	


</div>
