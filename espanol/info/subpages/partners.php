<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Instituciones colaboradoras</h2>
	</section>

	<section>
	<p>

		Mobiliscope se está desarrollando en dos laboratorios públicos franceses de investigación asociados al <b>CNRS</b>: el laboratorio <b>Géographie-cités</b> de París (desde 2017) y el laboratorio<b>LISST</b> de Toulouse (desde 2024).

		<a href = "https://geographie-cites.cnrs.fr/" target="_blank" >
		<figure class="inline">
		  <img src="/dist/assets/geocites.png" alt="geocites" width="280"/>
		</figure>
		</a>

		<a href = "https://www.cnrs.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/cnrs.png" alt="cnrs" width="90"/>
		</figure>
		</a>

		<a href = "https://lisst.univ-tlse2.fr/" target="_blank" >
		<figure class="inline">
		  <img src="/dist/assets/logo-lisst.png" alt="lisst" width="215"/>
		</figure>
		</a>

		</br>
	</p>

	<p>
		Los otros dos colaboradores principales de Mobiliscope son el <b>Ined</b> y la <b>ANCT</b>.</br>

		<a href = "https://www.ined.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/ined.png" alt="ined" width="170"/>
		</figure>
		</a>

		<a href = "https://agence-cohesion-territoires.gouv.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/anct.png" alt="anct" width="265"/>
		</figure>
		</a>


	</p>

	<p>
		Mobiliscope se beneficia del apoyo de otros socios públicos institucionales en términos de acceso a datos, infraestructuras informáticas y financiación.


		<a href = "http://www.progedo-adisp.fr/" target="_blank">
		<figure class="inline">
		 <img src="/dist/assets/progedo.png" alt="progedo" width="130"/>
		</figure>
		</a>


		<a href = "https://www.cerema.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/cerema.png" alt="cerema" width="255"/>
		</figure>
		</a>

		<a href = "https://www.huma-num.fr/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/huma-num.png" alt="huma-num" width="95"/>
		</figure>
		</a>

		<a href = "https://www.transports.gouv.qc.ca/" target="_blank">
		<figure class="inline">
			<img src="/dist/assets/mintransportsqc.png" alt="mintransportsqc" width="170"/>
		</figure>
		</a>

		<a href = "http://labex-dynamite.com/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/labex.png" alt="labex" width="80"/>
		</figure>
		</a>

		<a href = "https://www.spherelab.org/" target="_blank">
		<figure class="inline">
		  <img src="/dist/assets/labsphere.png" alt="spherelab" width="145"/>
		</figure>
		</a>

	</p>
	</section>




	<p>
	<button class="style-button mb50"><a href="/es/info/about/publications">Descubra los artículos y comunicaciones relacionados con la herramienta</a></button>
	</p>

</div>
