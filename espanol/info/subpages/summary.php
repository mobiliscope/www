<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
	<section>
	<h2>En resumen</h2>
	</section>
	
	</br>
	<section>
		<p>
		Mobiliscope es una herramienta de geovisualización para explorar la población presente y la mixidad social en las ciudades a lo largo de las 24 horas del día.
		</p>

		<p>
		Permite explorar libremente <b>58 regiones urbanas</b> (49 en Francia, 6 en Quebec, Canadá, y 3 en América Latina). En Francia <b>10.000 municipios</b> están así cubiertos por la herramienta (el 60% de la población).</br>
		</p>

		<p>
		Tras esta herramienta, hay un <a href="/es/info/about/team">equipo</a> de geógrafos, geomatemáticos y varios <a href="/es/info/about/partners">colaboradores</a> institucionales.</br>
		</p>

		<p>
		Asimismo, el equipo ha descrito la herramienta y analizado las dinámicas temporales, espaciales y sociales asociadas en diferentes <a href="/es/info/about/publications">articulos y communicaciones</a>.
		</p>

		<p>
		Para citar la <b>versión actual</b> de Mobiliscope&nbsp;:</b>
			<ul>
			Vallée J, Douet A, Le Roux G, Commenges H, Lecomte C, Villard E (2024). Mobiliscope, an open platform to explore cities and social mix around the clock [v4.3]. www.mobiliscope.cnrs.fr. doi:10.5281/zenodo.11111161
			</ul>
		</p>


		</section>

		<section>
		<h3>Cómo funciona la herramienta</h3>

			<p>
			Los <a href="/es/info/methods/data">datos</a> provienen de encuestas de viajes individuales a gran escala (968.000 personas y casi 2,9 millones de viajes) realizadas entre 2009 y 2019.
			</p>

			<p>
			Las poblaciones presentes en las ciudades durante las 24 horas de un día laborable típico (de lunes a viernes) se caracterizan según <a href="/es/info/methods/indicators">indicadores demográficos (sexo y edad) y sociales (nivel educativo, categoría socioprofesional y, a veces, ingresos)</a>, pero también según las zonas de residencia, el tipo de actividad desarrollada y el modo de transporte utilizado.
			</p>

			<p>
			El mapa central y los dos gráficos que se animan en el tiempo son los elementos clave de la propuesta de <a href="/es/info/methods/geovizualisation">geovisualización</a>.
			</p>

			<p>
			Mobiliscope está en constante <a href="/es/info/methods/evolution">evolución</a>. Se añaden regularmente nuevas ciudades y funciones.
			</p>

			 <button class="style-button mb50"><a href="/es/geoviz/bogota/">Descubra la herramienta para <b>Bogotá</b> y su región</a></button>
			</p>

		
			</section>

		<section>
		<h3>Ciencia abierta</h3>

			<p>
			El desarrollo de Mobiliscope se inscribe en los <b>principios FAIR</b>: hacemos todo lo posible para garantizar que los datos construidos por el equipo y mostrados en la herramienta sean «Fáciles de encontrar, Accesibles, Interoperables y Reutilizables».</br>
			</p>

			<p>
			El código fuente para el procesamiento de los datos iniciales y la creación de la interfaz de geovisualización son <a href="/es/info/open/license">abiertos</a>. Una <a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">guía del administrador</a> (en francés) también documenta los códigos utilizados y el funcionamiento de la aplicación web. Los datos mostrados en la herramienta son <a href="/es/info/open/opendata">open-data</a> y pueden reutilizarse en <a href="/es/info/open/reuses">artículos reproducibles</a>.
			</p>
			</br>
      		</br>
   		</section>

		<section>
		<h3>Una herramienta de análisis, planificación y enseñanza</h3>

		<p>
			Existe una gran variedad de usos y diferentes públicos pueden utilizar la herramienta. Por lo tanto, se puede utilizar como una <a href="/es/info/multitask-tool/planning-tool">herramienta de planificación</a> por los agentes públicos y las autoridades locales, una <a href="/es/info/multitask-tool/scientific-tool">herramienta de investigación</a> para los investigadores, una <a href="/es/info/multitask-tool/pedagogical-tool">herramienta pedagógica</a> para los docentes.
		</p>

		<p>
		Para los interesados en los ritmos diurnos de las zonas cubiertas por los programas de acción pública, una nueva herramienta ofrece enlaces directos con los territorios franceses afectados por estas zonificaciones.  
		</p>

		<button class="style-button mb50"><a href="/es/info/toolbox/zoning">Descubra el <b>Filtro por zonificaciones institucionales <small>(Francia)</small></b></a></button>
    
    </section>
		
		
	<section>
    <center>
        <figure class="inline" >
          <img src="/dist/assets/prix.jpg" alt="prix" width="350"/>
          <figcaption>Trofeo del premio <b>Open Science of Research Data</b>. </br><i>Otorgado en 2022 por el Ministerio francés de Enseñanza Superior e Investigación.</i></figcaption>
        </figure>
      </center>
   
	</section>

    </br>
    </br> 
    </br>
    </br> 


</div>


