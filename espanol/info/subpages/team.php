<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Equipo</h2>
	</section>

	<section>
	<h3>Miembros </h3>


    <p>
		<b><a href = "https://lisst.univ-tlse2.fr/accueil/julie-vallee#/" target="_blank" >Julie Vallée</a></b> es directora de investigación en geografía en el Centro Nacional de Investigación Científica francés (CNRS). Es miembro del laboratorio LISST (Toulouse) y está asociada al laboratorio Géographie-cités (París). Julie es la responsable científica de Mobiliscope.</br> 
		Sus trabajos versan sobre la movilidad cotidiana de la población, los fenómenos de segregación y los efectos de lugar. Con Mobiliscope, Julie explora y trata de elucidar los ritmos cotidianos de los espacios y las desigualdades sociales que conllevan.</br>
		
	</p>

	<p>
		<b><a  target="_blank" href = "http://crh.ehess.fr/index.php?9040">Aurélie Douet</a></b> es geomatemática y cartógrafa.</br>
		Estuvo a cargo del desarrollo de Mobiliscope durante más de 4 años. En particular, ha sido responsable de la ampliación de la herramienta (inicialmente centrada en la región de Île-de-France) a otras ciudades francesas, de la integración de ciudades latinoamericanas y, más recientemente, de la reorganización de la arquitectura del software y de los protocolos de tratamiento de datos. Desde diciembre de 2023, Aurélie es ingeniera de investigación en el Centre de Recherches Historiques (EHESS/CNRS) y sigue con interés la evolución del proyecto. 
	</p>
 

	<p>
		<b><a href = "https://www.ined.fr/fr/recherche/chercheurs/Le+Roux+Guillaume" target="_blank">Guillaume Le Roux</a></b> es investigador en el Instituto Nacional de Estudios Demográficos francés (INED) en la unidad Vivienda, Desigualdades espaciales y Trayectorias (LIST).</br> 
		Sus investigaciones se centran en las relaciones entre movilidad social, movilidad espacial y recomposición de los territorios bajo influencia urbana. Guillaume contribuyó en 2016 a la exploración crítica de la base de datos de la Encuesta Global Transporte (EGT) de Île-de-France con el objetivo de producir un análisis de la segregación social de la región parisina en las diferentes horas del día. Desde 2021 se dedica a la integración y el análisis de los ritmos cotidianos de las ciudades de América Latina.
	</p>

	<p>
		<b><a target="_blank" href = "https://geographie-cites.cnrs.fr/membres/hadrien-commenges/">Hadrien Commenges</a></b> es docente investigador en geografía en la Universidad París 1 Panthéon-Sorbonne y miembro del laboratorio Géographie-cités. </br>
		Sus líneas de investigación se inscriben en un enfoque cuantitativo de la geografía urbana, en particular en el campo de la movilidad urbana y de los transportes.
	</p>


	</section>

	<section>
	<h3>Anteriores miembras</h3>

    <p>
		<b>Constance Lecomte</b> desarrolló en 2017 las dos primeras versiones de Mobiliscope centradas en Île-de-France.</br>
		Después siguió muy implicada en el proyecto cuando trabajó en el Observatoire des Territoires de la Agence nationale de la cohésion des territoires (ANCT).
	</p>

	<p>
		<b>Elisa Villard</b> se unió al equipo durante varios meses en 2019. Se encargó de incluir ciudades canadienses en Mobiliscope (como continuación de sus prácticas de posgrado de cinco meses en Montreal).
	</p>

	</section>

	<section>
	<h3>Estudiantes</h3>

	<p>
	Varios estudiantes han realizado sus prácticas y tesinas en relación con Mobiliscope.

		<ul>

			<li><b>Robin Demé</b> (Master 2 Démographie - Expertise en Sciences de la Population - Université Paris 1 Panthéon-Sorbonne). 2023.</li>

			<li><b>Baptiste Larrouy</b> (especialización “Ciudades digitales” de la École Centrale de Nantes). 2021.</li>
			
			<li><b>Élisa Villard</b> (especialización “Ciudades digitales” de la École Centrale de Nantes). 2019.</li>

			<li><b>Simon Oertlin</b> (Máster Geoprisme - Universidad París 1 Panthéon-Sorbonne). 2019. </li>

			<li><b>Maximilien Riot</b> (Máster GeoPRAD - Universidad de Niza). 2019.</li>
			
		</ul>
	</p>
	</section>

	<section>
    <h3>Otros contribuciones</h3>

    <p>
		<ul>

			<li>Renaud Bessières (<a target="_blank" href="https://qodop.com/" title="qodop">qodop</a>) : desarrollo web</li> 
			<li>Mathieu Leclaire (Géographie-cités/ISC-PIF) : reorganización informática (alojamiento en HumaNum, almacenamiento de datos en MinIO, repositorios gitlab, etc.)</li>
			<li>Barbara Christian y Julie Pelata (Cerema) : ayuda en las encuestas origen-destino francesas (EMC²)</li>
			<li>Cédric Poirier : mejora de la interfaz</li>
			<li>Saber Marrouchi y Yonathan Parmentier (Géographie-cités) : apoyo informático</li>
			<li>Florent Demoraes (Université de Rennes/ANR Modural) : comentarios críticos sobre el uso de encuestas latinoamericanas y ayuda con el mapa base de Bogotá</li>
			<li>Marc Sainte-Croix y Blandine Maison (Agence d’urbanisme, région nîmoise et alésienne) : participación en la integración de la región de Nîmes</li>
			<li>Sébastien Haule (Géographie-cités) : apoyo a la comunicación</li>
			<li>Robin Cura y Mattia Bunel (Géographie-cités) : asesoramiento en geovisualización</li>

		</ul>
	</p>
	</section>

	<section>
     <h3>¡Únase a nosotros!</h3>
    <p>
		Contáctenos si desea unirse al equipo (por sus actividades o estudios).
    </p>
	</section>

	<p>
	<button class="style-button mb50"><a href="/es/info/about/partners">Descubra nuestros colaboradores</a></button>
	</p>


</div>
