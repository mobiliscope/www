<div class = "corps" data-title="<?php echo $curPage['pageTitle']; ?>">
    <section>
	<h2>Cambios y actualizaciones</h2>
	</section>

	</br>

<section>
 	<h3>Novedades de la última versión [v4.3]</h3>
 	
        <p>
        <i>Junio de 2024</i>
	
 			<ul> 

 			<li>Creación de una <b><a href="/es/info/toolbox/zoning" target="_blank">herramienta de filtrado</a></b> para listar los territorios franceses de Mobiliscope incluidos en los programas franceses de acción pública.</li>

 			<li>En la geovisualización de las ciudades francesas, ahora es posible <b>visualizar mapas de fondo de zonificación de estadísticas públicas</b> (Zonificación de Áreas Urbanas 2010; Áreas de Atracción de Ciudades 2020) y de acción pública (programas Action Coeur de Ville y Petites Villes de Demain), además de los perímetros de los Barrios Urbanos Prioritarios.</li>

 			<li>Servicios de alojamiento en la Plataforma Digital de Humanidades <b>HumaNum</b></li>

 			<li>Utilización de un <b>servicio de almacenamiento de datos</b> de alto rendimiento (MinIO).</li>

    		<li>Reorganización del código de <b>preprocesamiento</b> de datos y del código de <b>software</b> en repositorios públicos dedicados de <b><a href = "https://gitlab.huma-num.fr/mobiliscope" target="_blank">gitlab</a></b>.</li>

    		<li>Redacción y publicación de una nueva <b><a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html" target="_blank">Guía del Administrador</b></a> (en francés).</li>

    		<li>Los usuarios pueden <b>ejecutar Mobiliscope localmente</b> en sus propias máquinas clonando el código del software desde el repositorio <a href = "https://gitlab.huma-num.fr/mobiliscope/www" target = "_blank">gitlab.huma-num.fr/mobiliscope/<b>www</b></a>, descargando los datos a través del repositorio <b><a href="https://doi.org/10.5281/zenodo.11111161" target="_blank">Zenodo</a></b> y siguiendo las instrucciones de la <b><a href = "https://mobiliscope.quarto.pub/guide_admin/guide_admin.html#sec-instalocal" target="_blank">Guía del Administrador</b></a>.
    		</li>

 			</ul>
          </p>

  	</section>

  	<section>
	<h3>Actualizaciones</h3>

		<section>
 		<h4>Actualización con datos recientes</h4>

 		<p>
			Uno de los retos del equipo de Mobiliscope es utilizar los datos más recientes para actualizar las geovisualizaciones de las poblaciones que viven en las ciudades y sus alrededores.
		</p>

		<p>
			La geovisualización de la región de Valenciennes (Francia) se ha actualizado con los datos de la encuesta EMC² de 2019: <a href="/es/geoviz/valenciennes/" target='_blank'>Valenciennes y su región (<b>2019</b>)</a>.
		</br>
			Sin embargo, la geovisualización construida inicialmente con datos de 2011 sigue siendo accesible: <a href="/es/geoviz/valenciennes2011/" target='_blank'>Valenciennes y su región (<b>2011</b>)</a>. 
		</p>

		<p>
			La geovisualización de la <b>región de París</b> (Francia) propuesta por defecto en Mobiliscope utiliza datos de la Encuesta Mundial de Transporte (EGT) de 2010: <a href="/es/geoviz/idf/" target='_blank'>París y su región (<b>2010</b>)</a>.
			</br>
			Se han analizado datos más recientes de la Encuesta Mundial de Transporte 2018-20 y se han utilizado como base para otra geovisualización: <a href="/es/geoviz/idf2020/" target='_blank'>París y su región (<b>2020</b>)</a>. Sin embargo, la escasa muestra de personas encuestadas por sector en la EGT 2018-20 limita mucho la calidad de las estimaciones de las poblaciones presentes por sector y por tiempo (véanse los <a href="https://gitlab.huma-num.fr/mobiliscope/data-process/blob/main/traitements_annexes/val_stat_egt2020/val_stat_egt2020.pdf" target='_blank'>análisis complementarios</a>) - en francé ). Por lo tanto, para París y su región, se recomienda encarecidamente seguir utilizando la geovisualización construida a partir de los datos de <a href="/es/geoviz/idf/" target='_blank'><b>2010</b></a>.
		</p>

		<p>
			También se están estudiando otras actualizaciones para ciudades con datos más recientes sobre desplazamientos de población: <b>Toulouse</b> (2023 <i>vs.</i> 2013), <b>Nice</b> (2023 <i>vs.</i> 2009), <b>Angers</b> (2022 <i>vs.</i> 2012), <b>Caen</b> (2022 <i>vs.</i> 2011), <b>Bordeaux</b> (2021 <i>vs.</i> 2009), <b>Grenoble</b> (2020 <i>vs.</i> 2010), <b>Marseille</b> (2020 <i>vs.</i> 2009), <b>Montréal</b> (2018 <i>vs.</i> 2013), <b>Bogotá</b> (2023 <i>vs.</i> 2019)...


 
		</p>

		</section>

		<section>
 		<h4>Nuevas ciudades añadidas</h4>
	
		<p>
		En Francia, podrían añadirse a la herramienta nuevas ciudades con encuestas sobre desplazamientos diarios, como <b>Toulon</b> (2022) y <b>Chambéry</b> (2022)..
		</p>

		<p>
		También podrían añadirse ciudades de otros países si se dispone de encuestas precisas y representativas de los desplazamientos diarios de la población:
			<ul>
				<li><b>Stockholm</b> (<i>Stockholm Travel Survey, 2015</i>) en Suecia.</li>
				<li><b>Amsterdam</b>, <b>Rotterdam</b>, <b>Utrecht</b>, <b>La Haye</b> (<i>Dutch National travel survey - Onderweg in Nederland ODiN</i>) en los Países Bajos.</li>
				<li><b>Manchester</b> (<i>Greater Manchester Travel Diary Survey</i>) et <b>Londres</b> (<i>London Travel Demand Survey</i>) en Gran Bretaña.</li>
			</ul>
		</p>

		</br>

		<p>
		<b>Si está interesado en ayudar a actualizar los datos o añadir nuevas ciudades a Mobiliscope, póngase en contacto con nosotros.</br>
		<a href="mailto:mobiliscope@cnrs.fr">mobiliscope[at]cnrs.fr</a></b>.
		</p>

		</section>




	<section>
	<h3>Histórico de las versiones</h3>
	<p>
		Mobiliscope es una herramienta en constante evolución desde la primera versión en línea de 2017.
	</p>
	</section>

		<section>
		<h4><b>v1</b> & <b>v2</b> [2017]</h4> 

		<p>
			Las dos primeras versiones estaban centradas en la <b>región de París</b> (Francia), con interfaz en <b>inglés</b>.

			<ul>
				<li><b>v1</b> <i> [mayo de 2017]</i></br>
				Esta primera versión permite explorar la población presente a lo largo de las 24 horas del día según tres indicadores (población total, nivel educativo y categoría socioprofesional del hogar) y con dos modos de representación de la población presente (en stock y en porcentaje). Un mapa central muestra la población presente en los diferentes sectores de la región. Un primer gráfico permite visualizar los índices de segregación (Duncan y Moran) para el conjunto de la región. Un segundo gráfico permite visualizar la población presente en el sector seleccionado en el mapa. La población estudiada tiene 16 años y más.</li>
				
				</br> 
				
				<li><b>v2</b><i> [septiembre de 2017]</i></br> 
				Se incorporan seis nuevos indicadores que permiten caracterizar a la población presente en el transcurso del día (sexo, edad, ingresos, ocupación, lugar de residencia y actividad realizada). Se propone un nuevo modo de representación cartográfica (mapas de flujo) de la población no residente.</li>

			</ul>
		</p>
		
		</section>


		<section>
		<h4><b>v3</b> [2019-2020]</h4> 

		<p>
			La tercera versión integra <b>nuevas ciudades</b> francesas, pero también canadienses, para extender el perímetro geográfico de la herramienta. Se ofrece una <b>interfaz bilingüe</b> (francés/inglés).

			<ul>
				<li><b>v3.1.</b> <i>[marzo de 2019]</i></br> 
				Se incorporan <b>21 ciudades francesas</b>. La edad de la población estudiada se fija ahora en 16 años y más. Se añade un nuevo indicador sobre el último modo de transporte. </li>

				</br>

				<li><b>v3.2.</b> <i>[diciembre de 2019]</i></br> 
				Se añade la ciudad de <b>Nîmes</b>, con la colaboración de la Agencia de urbanismo, región de Nimes y Alès.</li>

				</br>

				<li><b>v3.3.</b> <i>[abril de 2020]</i></br> 
				Se añaden <b>6 ciudades de Quebec</b>. La visualización es la misma que para las ciudades francesas. Los indicadores que caracterizan a la población presente en el transcurso del día son algo menos numerosos (sexo, edad, ingresos, ocupación, actividad realizada y último modo de transporte utilizado). El umbral de edad es ligeramente inferior (15 años o más)</li>
			
			</ul>
		</p>

		</section>

		
		<section>
		<h4><b>v4</b> [2021-...]</h4>
			
		<p>

			En la cuarta versión de la herramienta se lleva a cabo una importante <b>renovación de la interfaz gráfica</b>.</br>
			Se facilita el acceso a los datos: los usuarios pueden <b>descargar los datos</b> ponderados y agregados por hora y por sector, tal y como se muestran en la herramienta.</br>
 			
		<ul>
 			<li>
 				<b>v4.0</b><i> [abril de 2021]</i>

				<ul style="margin-top: 0px">
					<li>Incorporación de <b>26 nuevas ciudades/regiones francesas</b> como Rouen, Metz, Besançon, Brest, Le Havre, Poitiers y Tours, además de los territorios de Martinique y La Réunion.</li>
					
					<li><b>Open-data</b>. Disponibilidad de los datos ponderados y agregados por hora y por sector para las <b>ciudades francesas</b>.</li>
				
					<li>Integración de 2 nuevos indicadores: (1) residentes/no residentes del sector; (2) habitante (sí/no) en Barrio Prioritario en Políticas Urbanas - QPV (para Francia únicamente).</li>

					<li>Incorporación de un nuevo modo de representación para el indicador "Población total": densidad de población presente (pers. por km²).</li>

					<li>Pueden también visualizarse capas <i>OpenStreetMap</i> para ubicarse mejor en las ciudades.</li>

					<li>Cambio de los valores umbral para la visualización de la población no residente (mapas de flujo). Se ha eliminado el umbral de 12 de las versiones precedentes (umbral a partir del cual se difundía la información) aplicado sobre el stock bruto de no residentes y sobre las relaciones entre sector de presencia y sector de residencia. Ha sido reemplazado por un umbral de 6 encuestados brutos aplicado únicamente a los flujos entre los sectores de presencia y residencia.</li>

					<li>Otras modificaciones: Actualización de datos para Valenciennes y su región a partir de la encuesta EMC² 2019; Ampliación de la area geográfica de Bordeaux a través de la incorporación de 19 sectores periféricos de la encuesta de 2009; Correcciones de las estimaciones de la población presente en las ciudades de Quebec; Supresión de la visualización de los índices de segregación para los indicadores "actividades" y "modo de transporte".</li>

				</ul>

			</li>

			</br>

 			<li>
 				<b>v4.1</b><i> [abril de 2022]</i>

 				<ul style="margin-top: 0px">

					<li>Adición de <b>3 nuevas ciudades/regiones latinoamericanas</b>: Bogotá (Colombia), Santiago (Chile) y São Paulo (Brasil), con la adición de indicadores específicos y capas asociadas (por ejemplo, indicador sobre el trabajo informal, red TransMilenio en Bogotá)</li>

					<li><b>Open-data</b>. Suministro de datos ponderados y agregados por hora y por sector para las <b>ciudades canadienses y latinoamericanas</b>.</li>

					<li>Desarrollo de la interfaz en español para ofrecer una <b>interfaz trilingüe</b> (francés, inglés, español).</li>

					<li>Integración de un indicador sobre la composición de los hogares de los encuestados para las 58 ciudades/regiones de Mobiliscope.</li> 

					<li>Posible visualización de imágenes satelitales y/o aéreas para ayudar al usuario a orientarse en las ciudades.</li>

					<li>Reajuste del cálculo de las horas de presencia. Este reajuste conlleva un desplazamiento de una hora en los datos de asistencia en comparación con los datos mostrados en las versiones anteriores de la herramienta. la población presente observada en un sector a una hora <i>h</i> fue previamente observada a <i>h-1</i>.</li>
		 
					<li>Creación de la URL correspondiente a la geovisualización mostrada. Por ejemplo, la proporción de mujeres en Bogotá a las 8 de la mañana en la zona de La Candelaria es ahora accesible siguiendo este <a target="_blank"  href = "/es/geoviz/bogota?m1=2&m2=2&m3=1&m4=part&t=8&s=UTAM94">URL</a>. Posibilidad de compartir geovisualizaciones específicas a través del botón "Compartir vista" <img src="/dist/assets/share.svg" width="20px" height= "20px"/>. </li>

					<li>Mejora de la herramienta de búsqueda por nombre de municipio <img src="/dist/assets/search-black.svg" width="20px" height= "20px"/>: tras la redirección a la geovisualización correspondiente a la búsqueda, se selecciona en el mapa y en el gráfico superior el sector asociado al municipio buscado.</li>

					<li>Cambio en la definición del modo de transporte principal para las ciudades canadienses: en el caso de los viajes multimodales, el modo de transporte principal se define ahora en este orden de prioridad: 1) Transporte colectivo; 2) Vehículo motorizado individual y 3) Modo suave (a pie, en bicicleta, etc.). Este orden de prioridad es ahora el mismo que el orden adoptado para las ciudades francesas y latinoamericanas. </li>

			
				</ul>
			</li>
		</br>

			<li>
 				<b>v4.2</b><i> [abril de 2023]</i>
 				
 				<ul style="margin-top: 0px">
 				
 					<li>Amplia <b>reorganización de la arquitectura del software</b> (refactorización del código web) para garantizar la estabilidad, eficacia y durabilidad de la herramienta y facilitar futuros desarrollos.</li>

 					<li><b>Refactorización del código R</b> utilizado para el tratamiento de datos. Datos también se han <b>reorganizado y simplificado</b>.</li>

 					<li>Para los mapas de coropletas, modificación de los métodos de discretización realizados ahora "sobre la marcha" ciudad por ciudad gracias a la biblioteca <a href = "https://github.com/simogeo/geostats" target="_blank" >geostats.js</a>. En consecuencia, los nuevos límites de clase pueden diferir de los de la v4.1, en particular para los indicadores "Sexo", "Zona/Residencia" y "Actividad", cuyos límites de clase eran anteriormente similares para todas las ciudades.</li>
 							
 					<li>Introducción de normas más estrictas para la eliminación de dobles recuentos (es decir, personas contadas dos veces en la misma zona en un momento dado) con el fin de corregir algunas estimaciones de presencia. Estas correcciones afectan principalmente a las ciudades canadienses, y muy marginalmente a las ciudades francesas y latinoamericanas.</li>
 							
 					<li>Simplificación de los títulos de mapas y gráficos.</li>

 					<li>Creación de un <b><a href="https://doi.org/10.5281/zenodo.7822016" target="_blank">repositorio Zenodo</a></b> que reúna <b>todos los datos</b> propuestos en open-data en la herramienta.</li> 

 				</ul>
 				
			</li>
	 	</p>		
		</section>
		
		
</br>
</br>
</br>
</br>
</br>
</br>	
</div>
