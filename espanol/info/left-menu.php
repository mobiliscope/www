

   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'about' ? 'current-page' : ''; ?>">Acerca de</a>
      <div class="left-menu-niv2">
        <a href="/es/info/about/summary"><div class="item">En resumen</div></a>
        <a href="/es/info/about/team"><div class="item">Equipo</div></a>
        <a href="/es/info/about/partners"><div class="item">Instituciones colaboradoras</div></a>
        <a href="/es/info/about/publications"><div class="item">Publicaciones & co</div></a>
     </div>
   </div>

   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'methods' ? 'current-page' : ''; ?>">Métodos
     </a>
     <div class="left-menu-niv2">
        <a href="/es/info/methods/data"><div class="item">Transformar los viajes</div></a>
        <a href="/es/info/methods/indicators"><div class="item">Caracterizar la población presente</div></a>
        <a href="/es/info/methods/geovizualisation"><div class="item">Geovisualización</div></a>
        <a href="/es/info/methods/evolution"><div class="item">Cambios y actualizaciones</div></a>
    </div>
   </div>

    <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'open' ? 'current-page' : ''; ?>">Ciencia abierta
     </a>
     <div class="left-menu-niv2">
        <a href="/es/info/open/license"><div class="item">Código abierto y documentado</div></a>
        <a href="/es/info/open/opendata"><div class="item">Datos abiertos y reutilizables</div></a>
        <a href="/es/info/open/reuses"><div class="item">Publicaciones reproducibles</div></a>
        <a href="/es/info/open/cite"><div class="item">¿Cómo citar Mobiliscope?</div></a>
    </div>
   </div>


   <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'multitask-tool' ? 'current-page' : ''; ?>">Aplicaciones
     </a>
     <div class="left-menu-niv2">
        <a href="/es/info/multitask-tool/scientific-tool"><div class="item">Ciencia & Investigación</div></a>
        <a href="/es/info/multitask-tool/planning-tool"><div class="item">Planificación</div></a>
        <a href="/es/info/multitask-tool/pedagogical-tool"><div class="item">Educación</div></a>
    </div>
   </div>

        <div class="menu-item-container">
     <a href="#" class = "menu-item <?php echo $page == 'toolbox' ? 'current-page' : ''; ?>">Toolbox</a>
     <div class="left-menu-niv2">
      <a href="/es/info/toolbox/zoning"><div class="item">Filtrar por zonas institucionales <small>(Francia)</small></div></a>
    </div>
    </div>




