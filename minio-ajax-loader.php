<?php
/*
    Chargement asynchrone d'un fichier minio

    Exemple d'appel : /minio-ajax-loader.php?file=paramgeom.js

*/

include($_SERVER['DOCUMENT_ROOT'].'/minio-settings.php');

$file = (!empty($_GET['file'])) ? $_GET['file'] : '';

$path = (!empty($_GET['path'])) ? $_GET['path'] : $path;

echo getObject($s3Client, $bucket, $path, $file);


?>



